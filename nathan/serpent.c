#include <SDL2/SDL.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#define NB_P 2 // nombre partie par serpent

typedef struct
{ // facon plus simple de définir les couleurs d'une partie du corps
    int r;
    int v;
    int b;
} color;

typedef struct body_part
{ // chaque rectangle possède une coleur propre un angle par rapport à son prédecesseur et une taille

    struct body_part *prec;
    struct body_part *next;
    int angle;
    SDL_Rect Rect;

} body_part;

typedef struct
{
    body_part *head;
    body_part *tail;
    int x;
    int y;
} serpent;

color *Rand_color()
{ // renvoie une structure color d'une couleur aléatoire
    color *couleur = malloc(sizeof(color));

    couleur->b = (rand() % 256);
    couleur->r = (rand() % 256);
    couleur->v = (rand() % 256);

    return (couleur);
}

void chooseSize(int *size)
{
    size[0] = (rand() % 200) + 50;
    size[1] = (rand() % 100) + 50;
}
/*
int creer_corps(serpent *snake, int nb_parts){
    printf("nombre de parties = %d\n", nb_parts);

    if (nb_parts<=0){
        return 0;
    }

    else{

        color *couleur = Rand_color();
        body_part * new_bd_part = malloc(sizeof(body_part));
        if (new_bd_part == NULL){
            return(0);
        }
        else{

            new_bd_part[0].color = couleur;

            if (snake->head->angle)
            {
                snake->tail->next = new_bd_part;
                new_bd_part->prec = snake->tail;

                snake->tail = new_bd_part;

                snake->tail->x_size = 0.9 * snake->tail->prec->x_size; // taille de la nouvelle body_part égale à 90% de sa précédente
                snake->tail->y_size = 0.9 * snake->tail->prec->y_size;
                snake->tail->angle = sin(1 / (snake->tail->x_size * snake->tail->y_size)); // tentative d'ondulation mais à reprendre
            }

            else{

                snake->head = new_bd_part;
                snake->tail = new_bd_part;

                int size[1];
                chooseSize(size);
                snake->head->x_size = size[0];
                snake->head->y_size = size[1];
                snake->head->angle = sin(1 / (snake->head->x_size * snake->head->y_size));
            }
        }
        int err = creer_corps(snake, nb_parts - 1);
    }
    return 0;
}*/

int creer_corps(serpent *snake, int nb_parts)
{
    if (nb_parts <= 0)
    {
        return 0;
    }

    while (nb_parts > 0)
    {

        body_part *new_bd_part = malloc(sizeof(body_part));

        if (new_bd_part == NULL)
        {
            free(snake); // don't forget to free the snake as well in this case
            return 0;
        }

        if (snake->head == NULL)
        {
            snake->head = new_bd_part;

            int size[2];
            chooseSize(size);
            snake->head->Rect.h = size[0];
            snake->head->Rect.w = size[1];
            printf("taille fenetre initiale = %d,%d\n", size[0], size[1]);
            snake->head->angle = sin(1 / (snake->head->Rect.h *
                                          snake->head->Rect.x));
            snake->tail = snake->head;
        }
        else
        {
            new_bd_part->prec = snake->tail;
            
            snake->tail->next = new_bd_part;
            snake->tail = new_bd_part;

            snake->tail->Rect.h = 0.9 * snake->tail->prec->Rect.h;
            snake->tail->Rect.w = 0.9 * snake->tail->prec->Rect.w;
            snake->tail->angle = sin(1 / (snake->tail->Rect.h *
                                          snake->tail->Rect.w));
            snake->head = new_bd_part;
        }
        nb_parts--;
    }
    return 0;
}

void creer_serpent(serpent *snake, int x, int y, int nb_parts)
{

    snake = (serpent *)malloc(sizeof(serpent));
    snake->x = x;
    snake->y = y;
/*  body_part *head = malloc(sizeof(body_part));
    snake->head = head;
    body_part *tail = malloc(sizeof(body_part));
    snake->tail = tail;

    snake->head->angle = 0;
    snake->tail->angle = 0;*/

    creer_corps(snake, nb_parts);
}

void affiche_rect(body_part *part, SDL_Renderer *Rend)
{

    color *col = Rand_color();

    SDL_SetRenderDrawColor(Rend,
                           col->r, col->v, col->b, // mode Red, Green, Blue (tous dans 0..255)
                           255);
    /*
        if (part->prec){
            printf("yoyo\n");
            part->Rect.x = (cos(part->prec->angle) * part->prec->Rect.x) + part->Rect.x;
            part->Rect.y = part->Rect.y - (sin(part->prec->angle) * part->prec->Rect.y);
        }
    */
    part->Rect.x = 100;
    part->Rect.y = 100;
    SDL_RenderDrawRect(Rend, &(part->Rect));
    SDL_RenderPresent(Rend);
}

void display_serpent(SDL_Window * Win, serpent * serpent_L, SDL_Renderer * Rend)
{

    if (Win && serpent_L && Rend)
    {

        printf("hi\n");
        body_part * head_i = serpent_L->head;
        if (!head_i)
        {
            exit(0);
        }
        printf("hi\n");

        if (head_i)
        {
            affiche_rect(head_i, Rend); /*
              while (head_i->next != NULL){
                  printf("hi\n");

                  head_i = head_i->next;
                  //affiche_rect(head_i,Rend);
                  printf("fin while\n");
              }*/
        }
    }
}

int main(int argc, char **argv)
{ // argv contient le nombre de serpents en argument 1

    (void)argc;
    srand(time(NULL));
    int NB_SERPENTS = atoi(argv[1]);


    // printf("r = %d,v = %d,b = %d\n", couleur->r, couleur->v, couleur->b);
    // printf("x = %d, y = %d\n", size[0], size[1]);
    serpent *list_serpent;
/*
    for (int i = 0; i < NB_SERPENTS; i++)
    {

        creer_serpent(list_serpent[i], 100 * i, 100, NB_P); // comment définir les coordonnées des têtes des serpents à l'intant initial ?
    }
*/

    creer_serpent(list_serpent, 100, 100, NB_P); // comment définir les coordonnées des têtes des serpents à l'intant initial ?

    // definir et creer les fenetres,
    //  gerer l'affichage et la maj des coordonnées et angles

    // On récupère les dimensions de l'écran
    if (SDL_Init(SDL_INIT_VIDEO) != 0)
    {
        SDL_Log("Error on SDL INIT VIDEO - %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }
    SDL_DisplayMode mode;
    if (SDL_GetCurrentDisplayMode(0, &mode) != 0)
    {
        SDL_Log("Error on Getting current display - %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    // Affichage des dimensions format "largeur x hauteur"
    printf("Display size = %dx%d\n", mode.w, mode.h);

    SDL_Window *Window = NULL;
    Window = SDL_CreateWindow("serpent.c", mode.w / 8, mode.h / 8, 6 * mode.w / 8, 6 * mode.h / 8, 0);

    SDL_Renderer *Rend = SDL_CreateRenderer(Window, -1, 0);

    /*
    SDL_Rect rectangle;
    rectangle.h = 50;
    rectangle.w = 50;
    rectangle.x = 50;
    rectangle.y = 50;

    SDL_SetRenderDrawColor(Rend,
                           50, 0, 0, // mode Red, Green, Blue (tous dans 0..255)
                           255);

    SDL_RenderDrawRect(Rend, &rectangle);
    SDL_RenderPresent(Rend);*/

    display_serpent(Window, list_serpent, Rend);

    SDL_Delay(5000);

    SDL_DestroyWindow(Window);

    SDL_Quit();
}
