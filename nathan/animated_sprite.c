#include <SDL2/SDL.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <SDL2/SDL_image.h>

SDL_Texture *loadTexture(char *file_name, SDL_Window *win, SDL_Renderer *rend)
{

    SDL_Surface *img = NULL;
    SDL_Texture *tex = NULL;

    img = IMG_Load(file_name);

    if (img == NULL)
    {
        SDL_Log("Error on loading surface from img : %s - %s\n", file_name, SDL_GetError());
        exit(EXIT_FAILURE);
    }

    tex = SDL_CreateTextureFromSurface(rend, img);
    SDL_FreeSurface(img);

    if (tex == NULL)
    {
        SDL_Log("Error on loading texture from surface img : %s - %s\n", file_name, SDL_GetError());
        exit(EXIT_FAILURE);
    }

    return tex;
}

void getMode(SDL_DisplayMode *mode)
{
    if (SDL_GetCurrentDisplayMode(0, mode) != 0)
    {
        SDL_Log("Error on Getting current display - %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }
}

void playBackGround(SDL_Texture *tex, SDL_Window *win, SDL_Renderer *rend, int back_off)
{
    SDL_Rect
        source = {0},
        win_size = {0},
        det = {0};

    SDL_GetWindowSize(
        win, &win_size.w,
        &win_size.h);

    SDL_QueryTexture(tex, NULL, NULL, // Récupération des dimensions de l'image
                     &source.w, &source.h);

    int scale = 3;

    det.w = win_size.w;
    det.h = win_size.h;

    det.y = 0;
    det.x = back_off;

    source.x = 0;
    source.y = 0;

    SDL_RenderCopy(rend, tex, &source, &det); // Préparation de l'affichage
    det.x += win_size.w;
    SDL_RenderCopy(rend, tex, &source, &det); // Préparation de l'affichage
}

void printTexture(SDL_Texture *tex, SDL_Window *win, SDL_Renderer *rend, SDL_Rect det, int reverse)
{

    (void)reverse;
    SDL_Rect
        source = {0},
        win_size = {0};

    SDL_GetWindowSize(
        win, &win_size.w,
        &win_size.h);

    SDL_QueryTexture(tex, NULL, NULL,
                     &source.w, &source.h);

    int scale = 1;

    det.w = source.w * scale;
    det.h = source.h * scale;

    source.x = 0;
    source.y = 0;

    /*
        switch (reverse)
        {
        case 1:
            SDL_RenderCopyEx(rend, tex, &source, &det, 0, NULL, 1); // Préparation de l'affichage
            break;
        case 0:
            SDL_RenderCopyEx(rend, tex, &source, &det, 0, NULL, 0); // Préparation de l'affichage
            break;
        }
        SDL_RenderPresent(rend); // Affichage de la nouvelle image
        SDL_Delay(20);
    */

    /*
        for (int j = 0; j < nb_it_x; j++)
        {

            SDL_RenderClear(rend); // Effacer l'image précédente

            SDL_RenderCopy(rend, tex, &source, &det); // Préparation de l'affichage
            SDL_RenderPresent(rend);                  // Affichage de la nouvelle image
            SDL_Delay(20);
            source.x += 192;
        }
    */

    SDL_RenderCopy(rend, tex, &source, &det); // Préparation de l'affichage
}

void playSprite(SDL_Texture *tex, SDL_Window *win, SDL_Renderer *rend, SDL_Rect det, int num_anim, int sprite, int reverse)
{
    SDL_Rect
        source = {0},
        win_size = {0};

    SDL_GetWindowSize(
        win, &win_size.w,
        &win_size.h);

    source.h = 192;
    source.w = 192;
    source.x = 0;
    source.y = (num_anim - 1) * 192;

    int nb_it_x = 6;

    int scale = 1;

    det.w = source.w * scale;
    det.h = source.h * scale;

    source.x += 192 * sprite;

    switch (reverse)
    {
    case 1:
        SDL_RenderCopyEx(rend, tex, &source, &det, 0, NULL, 1); // Préparation de l'affichage
        break;
    case 0:
        SDL_RenderCopyEx(rend, tex, &source, &det, 0, NULL, 0); // Préparation de l'affichage
        break;
    }
    /*
        for (int j = 0; j < nb_it_x; j++)
        {

            SDL_RenderClear(rend); // Effacer l'image précédente

            SDL_RenderCopy(rend, tex, &source, &det); // Préparation de l'affichage
            SDL_RenderPresent(rend);                  // Affichage de la nouvelle image
            SDL_Delay(20);
            source.x += 192;
        }*/

    /*
        SDL_RenderClear(rend); // Effacer l'image précédente


        SDL_RenderCopy(rend, tex, &source, &dest); // Préparation de l'affichage
        SDL_RenderPresent(rend);                   // Affichage de la nouvelle image
        SDL_Delay(5000);

    */
}

int main(int argc, char **argv)
{

    (void)argc;
    (void)argv;
    // On récupère les dimensions de l'écran
    if (SDL_Init(SDL_INIT_VIDEO) != 0)
    {
        SDL_Log("Error on SDL INIT VIDEO - %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    SDL_DisplayMode mode;
    getMode(&mode);
    printf("mode->h = %d\n", mode.h);

    SDL_Window *Window = NULL;

    Window = SDL_CreateWindow("serpent.c", mode.w / 8, mode.h / 8, 6 * mode.w / 8, 6 * mode.h / 8, 0);

    SDL_Renderer *Rend = SDL_CreateRenderer(Window, -1, 0);
    //SDL_Renderer *Rend2 = SDL_CreateRenderer(Window, -1, 0);

    if (Rend == NULL)
    {
        SDL_Log("Error on creating Render - %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }


    SDL_DisplayMode winMode;

    SDL_GetWindowDisplayMode(Window, &winMode);

    printf("winMode.w = %d\n", winMode.w);
    printf("winMode.h = %d\n", winMode.h);


    SDL_Texture *tex;
    tex = loadTexture("images/Warrior_Purple.png", Window, Rend);

    SDL_Texture *castle;
    castle = loadTexture("images/Castle_Purple.png", Window, Rend);

    SDL_Texture *cloud1;
    cloud1 = loadTexture("images/3.png", Window, Rend);

    SDL_Texture *cloud2;
    cloud2 = loadTexture("images/2.png", Window, Rend);
 
    SDL_Texture *cloud3;
    cloud3 = loadTexture("images/4.png", Window, Rend);
   
    SDL_Texture *sky;
    sky = loadTexture("images/1.png", Window, Rend);

    SDL_Rect rectS; // rectangle source
    rectS.x = 0;
    rectS.y = 0;

    SDL_Rect Castle_Proj;
    Castle_Proj.x = winMode.w/2 -150;
    Castle_Proj.y = winMode.h - 270 ;

    int Background_offset[2] = {0,0};
    int vitesse_defilement = 5;

    // coefficients qui définissent le nombre d'instructions "avancer" avant de changer de sprite
    float sprite_i = 0;
    int sprite = 0;
    int reverse = 0;

    while (Window) // Tant que l'on veut que le jeu fonctionne
    {
        SDL_Event ev;
        while (SDL_PollEvent(&ev) && Window)
        {
            switch (ev.type)
            {
            case SDL_WINDOWEVENT:
                if (ev.window.event == SDL_WINDOWEVENT_CLOSE)
                {
                    SDL_Delay(1000);

                    SDL_DestroyWindow(Window);

                    SDL_Quit();
                    exit(0);
                }
                break;
                /*
                case SDL_KEYUP:
                    if (ev.key.keysym.sym == SDLK_SPACE) // C'est la touche Barre Espace
                    {
                        vect.y -= 20;
                    }

                    if (ev.key.keysym.sym == SDLK_ESCAPE)
                    {

                        SDL_Delay(1000);

                        SDL_DestroyWindow(Window);

                        SDL_Quit();
                    }
                    break;
                */

            case SDL_KEYDOWN:

                if (ev.key.keysym.sym == SDLK_LEFT)
                {
                    rectS.x -= 5;
                    sprite_i += 0.25;
                    sprite = sprite_i;
                    reverse = 1;
                }
                else if (ev.key.keysym.sym == SDLK_RIGHT)
                {
                    rectS.x += 5;
                    sprite_i += 0.25;
                    sprite = sprite_i;
                    reverse = 0;
                }
                else if (ev.key.keysym.sym == SDLK_UP)
                {
                    rectS.y -= 5;
                    sprite_i += 0.20;
                    sprite = sprite_i;
                }
                else if (ev.key.keysym.sym == SDLK_DOWN)
                {
                    rectS.y += 5;
                    sprite_i += 0.20;
                    sprite = sprite_i;
                }
                break;
            }
        }

        SDL_RenderClear(Rend);


        playBackGround(sky, Window, Rend, 0);
        playBackGround(cloud2, Window, Rend, 0);

        Background_offset[0] -= vitesse_defilement;
        Background_offset[1] -= 0.6* vitesse_defilement;

        Background_offset[0] %= winMode.w;
        Background_offset[1] %= winMode.w;

        playBackGround(cloud3, Window, Rend, Background_offset[1]);
        playBackGround(cloud1, Window, Rend, Background_offset[0]);

        printTexture(castle, Window, Rend, Castle_Proj, reverse);
        playSprite(tex, Window, Rend, rectS, 2, sprite % 6, reverse);
        SDL_RenderPresent(Rend); // Affichage de la nouvelle image

        SDL_Delay(20);
    }

    SDL_Delay(5000);

    SDL_DestroyWindow(Window);

    SDL_Quit();
}