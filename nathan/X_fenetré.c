#include <SDL2/SDL.h>
#include <stdio.h>
#include <math.h>

// compilation : gcc X_fenetré.c -o X -lm $(sdl2-config --cflags --libs) 

#define WIN_NB 20 //>1


#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

static int window_counter = 0;

void generateWindowName(char* buffer, size_t buffer_size) {
    snprintf(buffer, buffer_size, "Fenetre %d", window_counter);
    window_counter++;
}

int Win_y_sin(float w_step, int win_x, SDL_DisplayMode mode){
    int win_y = (((2*sin(w_step*win_x)/(-M_PI))+1)* mode.h/2);
    return(win_y);
}

int main(int argc, char** argv){
    //Pas besoin des paramètres

    (void) argc;
    (void) argv;

    //On récupère les dimensions de l'écran
    if (SDL_Init(SDL_INIT_VIDEO)!=0){
        SDL_Log("Error on SDL INIT VIDEO - %s\n" ,SDL_GetError());
        exit(EXIT_FAILURE);
    };
    SDL_DisplayMode mode;
    int displayIndex = 0;
    if (SDL_GetCurrentDisplayMode(0,&mode) !=0){
        SDL_Log("Error on Getting current display - %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }
    
    //Affichage des dimensions format "largeur x hauteur"
    printf("Display size = %dx%d\n",mode.w, mode.h);



    // on va créer une quantité fixe de fenetres toutes de même taille que l'on stockera dans un tableau
    SDL_Window * Windows[WIN_NB] = {NULL};



    const size_t buffer_size = 50;
    char window_name[buffer_size];


    //On va essayer ici de faire décrire aux fenetres une sinusoïde

    //le pas est définie selon la taille de l'écran pour une période du sinus par
    float w_step = (2*M_PI)/mode.w; 
    int mid_screen_y = mode.h/2;

    int win_w = mode.w/WIN_NB; 

    int win_x = 0;
    int win_y = mode.h/2;

    int win_x_prec=0;
    int win_y_prec = 0;

    Windows[0]=SDL_CreateWindow("nom", win_x, win_y, 10, win_w,0);

    for (int i=1; i<WIN_NB; i++){

        generateWindowName(window_name, buffer_size);

        win_y_prec = win_y;
        win_x_prec = win_x;
        win_x+=win_w;

        win_y = Win_y_sin(w_step, win_x, mode);
        printf("w_step = %f\n ", w_step);
        printf("la fenetre %d se situe à x = %d\n", i , win_x);
        printf("hauteur fenetre %d est à y = %d\n", i, win_y);
        //win_y_prec = Win_y_sin(w_step, win_x_prec, mode);

        Windows[i] = SDL_CreateWindow(window_name,win_x,win_y, win_w,abs(win_y-win_y_prec),0);
    }

    for (int i = 0; i<10; i++){
        for(int j =0; j<WIN_NB; j++) {
            win_y_prec = win_y;
            SDL_GetWindowPosition(Windows[j],  &win_x, &win_y);
            SDL_SetWindowPosition(Windows[j], win_x, Win_y_sin(w_step,win_x+(i*win_w),mode));
            SDL_SetWindowSize(Windows[j],win_w,abs(win_y-win_y_prec) );
        }
    }



    for (int i=0; i<WIN_NB; i++){
        SDL_DestroyWindow(Windows[i]);
    }
    
    //SDL_DestroyWindow(Windows[0]);
    SDL_Quit();

}