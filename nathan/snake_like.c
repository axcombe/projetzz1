#include <SDL2/SDL.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <time.h>








typedef struct vect2
{
    int x;
    int y;

} vect2;

void drawRectangle(SDL_Rect *rect, SDL_Renderer *Rend)
{
    SDL_SetRenderDrawColor(Rend,
                           255, 0, 0, // mode Red, Green, Blue (tous dans 0..255)
                           255);

    SDL_RenderFillRect(Rend, rect);
    SDL_RenderPresent(Rend);
}

void set_RectPos(vect2 *vect, int velocity, SDL_Rect *rect, int * fen_size)
{

    vect2 gravity;
    gravity.x = 0;
    gravity.y = -3;

    vect->x -= gravity.x;
    // printf("vecteur y =%d\n", vect->y);

    // On met le carré en gravité = 90% durant une partie des phases de montées

    if (vect->y < -10){
        vect->y -= 0.80*gravity.y;
    }
    else{
    vect->y -= gravity.y;
    }
    
    // printf("vecteur y =%d\n", vect->y);

    int new_x = vect->x * velocity;
    int new_y = vect->y * velocity;


    //gestion des collisions

    if (((rect->y + new_y) >= fen_size[1] || (rect->y + new_y) < 0))
    {
        vect->y = -vect->y * 0.85;
        vect->x = 0.95 * vect->x ;
        new_y = vect->y * velocity;
    }

    if (((rect->x + new_x) >= fen_size[0] || (rect->x + new_x) < 0))
    {
        vect->x = -vect->x * 0.85;
        new_x = vect->x * velocity;
    }

    if ((rect->y + new_y) > (fen_size[1] -  2*gravity.y))
    {
        new_y = 0;
        rect->y = 0;
    }


    // printf("vecteur y =%d\n", vect.y);
    // printf("taille en hauteur =%d\n", (5 * mode.h / 8));

    rect->x += new_x;
    rect->y += new_y;
}

int main(int argc, char **argv)
{
    (void)argc;
    (void)argv;

    // On récupère les dimensions de l'écran
    if (SDL_Init(SDL_INIT_VIDEO) != 0)
    {
        SDL_Log("Error on SDL INIT VIDEO - %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }
    SDL_DisplayMode mode;
    if (SDL_GetCurrentDisplayMode(0, &mode) != 0)
    {
        SDL_Log("Error on Getting current display - %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    SDL_Window *Window = NULL;
    int fen_size [2]= {6 *mode.w / 8, 6 *mode.h / 8};
 

    Window = SDL_CreateWindow("serpent.c", mode.w / 8, mode.h / 8, 6 * mode.w / 8, 6 * mode.h / 8, 0);

    SDL_Renderer *Rend = SDL_CreateRenderer(Window, -1, 0);

    if (Rend == NULL)
    {
        SDL_Log("Error on creating Render - %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    //definition du rectangle
    SDL_Rect rectangle;
    rectangle.h = 50;
    rectangle.w = 50;
    rectangle.x = 50;
    rectangle.y = 50;

    // velocité initiale du rectangle
    vect2 vect;
    vect.x = 20;
    vect.y = 10;

    int velocity = 1.4;

    SDL_SetRenderDrawColor(Rend, 255, 0, 0, 255);
    drawRectangle(&rectangle, Rend);

    while (1) // Tant que l'on veut que le jeu fonctionne
    {
        SDL_Event ev;
        while (SDL_PollEvent(&ev))
        {
            switch (ev.type)
            {
            case SDL_WINDOWEVENT:                             
                if (ev.window.event == SDL_WINDOWEVENT_CLOSE) 
                {
                    SDL_Delay(1000);

                    SDL_DestroyWindow(Window);

                    SDL_Quit();
                }
                break;
            case SDL_KEYUP:                             
                if (ev.key.keysym.sym == SDLK_SPACE) // C'est la touche Barre Espace
                {
                    vect.y -= 20;
                    
        
                }

                if (ev.key.keysym.sym == SDLK_ESCAPE){

                    SDL_Delay(1000);

                    SDL_DestroyWindow(Window);

                    SDL_Quit();
                }
                break;
            

            case SDL_KEYDOWN:

                if (ev.key.keysym.sym == SDLK_LEFT)
                {
                    vect.x-=12;
                }
                if (ev.key.keysym.sym == SDLK_RIGHT)
                {
                    vect.x += 12;
                }
            }
        }

        set_RectPos(&vect, velocity, &rectangle, fen_size);

        SDL_SetRenderDrawColor(Rend, 0, 0, 0, 255);
        SDL_RenderClear(Rend);
        SDL_RenderPresent(Rend);

        SDL_SetRenderDrawColor(Rend, 255, 0, 0, 255);
        drawRectangle(&rectangle, Rend);

        SDL_Delay(20);
    }
/*
    while (vect.y!=0 || vect.x!=0)
    {

        set_RectPos(&vect, velocity, &rectangle, fen_size);

        SDL_SetRenderDrawColor(Rend, 0, 0, 0, 255);
        SDL_RenderClear(Rend);
        SDL_RenderPresent(Rend);

        SDL_SetRenderDrawColor(Rend, 255, 0, 0, 255);
        drawRectangle(&rectangle, Rend);

        SDL_Delay(20);
    }*/

    SDL_Delay(1000);

    SDL_DestroyWindow(Window);

    SDL_Quit();
}