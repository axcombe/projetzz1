#include <SDL2/SDL.h>
#include <stdio.h>
#include <math.h>


// compil : gcc -o rendu rendu.c -lSDL2 -Wall -Wextra -Werror -lm


void dessinrect(SDL_Renderer *renderer,int rectX,int rectY){
    SDL_Rect rectangle;                                                
                                            
    rectangle.x = rectX-50;                                             
    rectangle.y = rectY-50;                                                  
    rectangle.w = 100;                                                
    rectangle.h = 100;
                                        
    SDL_RenderFillRect(renderer, &rectangle);
}

SDL_Color HSVtoRGB(float h, float s, float v) {
    float r, g, b;
    int i;
    float f, p, q, t;

    if (s == 0) {
        r = g = b = v;
    } else {
        h /= 60; // Conversion de degrés en secteurs
        i = floor(h);
        f = h - i;
        p = v * (1 - s);
        q = v * (1 - s * f);
        t = v * (1 - s * (1 - f));

        switch (i) {
            case 0: r = v; g = t; b = p; break;
            case 1: r = q; g = v; b = p; break;
            case 2: r = p; g = v; b = t; break;
            case 3: r = p; g = q; b = v; break;
            case 4: r = t; g = p; b = v; break;
            default: r = v; g = p; b = q; break;
        }
    }

    // Conversion de [0,1] à [0,255]
    SDL_Color color = { (Uint8)(r * 255), (Uint8)(g * 255), (Uint8)(b * 255), 255 };
    return color;
}

int main(int argc, char **argv) {
    (void)argc;
    (void)argv;

    SDL_Window * window = NULL;


    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        SDL_Log("Error : SDL initialisation - %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    SDL_DisplayMode displayMode;
    if (SDL_GetCurrentDisplayMode(0, &displayMode)!=0){
        fprintf(stderr,"Impossible de récupérer le Display Mode : %s\n", SDL_GetError());
        SDL_Quit();
        return 1;
    }

    int largeur_f = displayMode.w - 400;
    int hauteur_f = displayMode.h - 400;



    window = SDL_CreateWindow("Fenêtre", 200, 200, largeur_f, hauteur_f, SDL_WINDOW_RESIZABLE);

    if (window == NULL) {
        SDL_Log("Error : SDL window 1 creation - %s\n", SDL_GetError());
        SDL_Quit();     
        exit(EXIT_FAILURE);
    }

    SDL_Renderer *renderer = SDL_CreateRenderer(window,-1,SDL_RENDERER_ACCELERATED);
    if (!renderer) {
        printf("Echec de la creation du renderer : %s\n", SDL_GetError());
        SDL_Quit();
        exit(EXIT_FAILURE);
    }


    SDL_Event event;
    SDL_bool running = SDL_TRUE;

    int centreX = largeur_f/2;  // coordonnée du dernier clic
    int centreY = hauteur_f/2;  // souris (par defaut le centre de la fenetre)
    int rayon = 100;
    int angle=0;

    int rectX,rectY; // coordonnée du rect a dessiner


    SDL_Color color;

    while (running) {


        while (SDL_PollEvent(&event)) {
            switch (event.type){
            
                case SDL_KEYDOWN:
                    running = SDL_FALSE;
                    break;
                case SDL_MOUSEBUTTONDOWN:
                    if (event.button.button == SDL_BUTTON_LEFT) {
                        centreX = event.button.x;
                        centreY = event.button.y;
                    }
                    break;
                default:
                    break;
            }
        }

        SDL_SetRenderDrawColor(renderer,255,255,255,255);
        SDL_RenderClear(renderer);
    
        rectX = cos(angle*(M_PI/180))*rayon + centreX;
        rectY = sin(angle*(M_PI/180))*rayon + centreY;

        color = HSVtoRGB((float) angle, 1.0, 1.0);
        SDL_SetRenderDrawColor(renderer,color.r, color.g, color.b,255);

        dessinrect(renderer,rectX,rectY);

        SDL_RenderPresent(renderer);

        SDL_Delay(10);
        
        angle = (angle + 1) %360;

    }

    SDL_Delay(1000);                           

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window); 

    SDL_Quit();

    return 0;
}
