#include <stdlib.h>
#include <SDL2/SDL.h>
#include <stdio.h>
#include <math.h>

#define NB_FENETRE 30

// compil : gcc -o fenetre fenetre.c -lSDL2 -Wall -Wextra -Werror -lm

int main(int argc, char **argv) {
    (void)argc;
    (void)argv;

    int i,
    hauteur,    //  Hauteur de l'ecran
    largeur,    //  Largeur de l'ecran
    centreX,    //  La coordonnée X du centre de l'ecran
    centreY,    //  La coordonnée Y du centre de l'ecran
    rayon,      //  distance entre les fenetres et le centre de l'écran
    angle,      //  Angle de la fenetre actuelle (dans la boucle)
    decale_angle,   //  La valeur de l'angle qu'il y a entre chaque fenetre
    t_fenetreX, // Largeur des fenetres
    t_fenetreY, // Hauteur des fenetres
    fenetreX,   // Coordonnée X de la fenetre actuelle (dans la boucle)
    fenetreY,   // Coordonnée Y de la fenetre actuelle (dans la boucle)
    pupilleX,   // Coordonnée X de la pupille
    pupilleY;   // Coordonnée Y de la pupille

    float sinus, // Valeur du sin de l'angle
    cosinus;       // Valeur du cos de l'angle

    /* Initialisation de la SDL  + gestion de l'échec possible */
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        SDL_Log("Error : SDL initialisation - %s\n",SDL_GetError());
        exit(EXIT_FAILURE);
    }

    SDL_DisplayMode displayMode;
    if (SDL_GetCurrentDisplayMode(0, &displayMode)!=0){
        fprintf(stderr,"Impossible de récupérer le Display Mode : %s\n", SDL_GetError());
        SDL_Quit();
        return 1;
    }

    hauteur=displayMode.h;
    largeur=displayMode.w;
    centreX=largeur/2;
    centreY=hauteur/2;
    rayon = centreY - 150; //  Pour que le rayon soit 150 pixels plus petit que la moitié de l'écran en Y
    decale_angle=360/NB_FENETRE;
    angle=0;


    t_fenetreX=100;
    t_fenetreY=100;


    SDL_Window *windows[NB_FENETRE];         // Tableau de windows pour le contour de l'oeil
    int nbFenetre = NB_FENETRE;

    SDL_Window *pupille=NULL; //Pupille de l'oeil




    for (i=0;i<nbFenetre;++i){
        char titre_Fenetre[50];
        snprintf(titre_Fenetre, sizeof(titre_Fenetre), "fenetre %d", i + 1);
        sinus = sin(angle*(M_PI/180));
        cosinus = cos(angle*(M_PI/180));

        printf("Angle : %d\n",angle);
        printf("Sin %d : %f\n",i,sinus);
        printf("Cos %d : %f\n",i,cosinus);

        fenetreX = cosinus*rayon + centreX;
        fenetreY = sinus*rayon + centreY;

        printf("X %d : %d\n",i,fenetreX);
        printf("Y %d : %d\n",i,fenetreY);
        printf("\n");

        windows[i]=SDL_CreateWindow(titre_Fenetre,fenetreX-50,fenetreY-50,t_fenetreX,t_fenetreY,0);
        if (windows[i] == NULL) {
            SDL_Log("Error : SDL window %d creation - %s\n",i, 
            SDL_GetError());                 // échec de la création de la fenêtre
            SDL_Quit();                              // On referme la SDL       
            exit(EXIT_FAILURE);
        }

        angle = angle + decale_angle;
    }


    pupilleX=centreX-50;
    pupilleY=centreY-50;
    pupille = SDL_CreateWindow("pupille",pupilleX,pupilleY,t_fenetreX,t_fenetreY,0);
    if (pupille == NULL) {
        SDL_Log("Error : SDL window %d creation - %s\n",i, 
        SDL_GetError());
        SDL_Quit();     
        exit(EXIT_FAILURE);
    }

    int v=100; // variation de la largeur et de la hauteur
    char op='+'; // defini le sens de variation (+ ou -)


    SDL_Event event;
    SDL_bool running = SDL_TRUE;


    while (running) {


        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_KEYDOWN) {
                running = SDL_FALSE;
                break;
            }
        }
        
        SDL_SetWindowSize(pupille, v, v);
        SDL_SetWindowPosition(pupille, pupilleX, pupilleY);

        if(v==200){
            op='-';
        }
        else if (v==30){
            op='+';
        }

        if (op=='+'){
            v=v+2;
            pupilleX-=1;
            pupilleY-=1;
        }
        else{
            v=v-2;
            pupilleX+=1;
            pupilleY+=1;
        }

    }

    
    
    

    

    SDL_Delay(1000);                           // Pause exprimée  en ms

    SDL_DestroyWindow(pupille);
    /* et on referme tout ce qu'on a ouvert en ordre inverse de la création */
    for (i=nbFenetre-1;i>=0;i--){
        SDL_DestroyWindow(windows[i]);
    }

    
    

    SDL_Quit();                                // la SDL

    return 0;
}