#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <math.h>

// compil : gcc -o sprite sprite.c -lSDL2 -lSDL2_image -Wall -Wextra -Werror -lm

SDL_Texture* load_texture_from_image(char *file_image_name, SDL_Renderer *renderer) {
    SDL_Surface *my_image = IMG_Load(file_image_name);
    if (my_image == NULL) {
        printf("Echec de la creation de l'image : %s\n", SDL_GetError());
        SDL_Quit();
        exit(EXIT_FAILURE);
    }

    SDL_Texture* my_texture = SDL_CreateTextureFromSurface(renderer, my_image);
    SDL_FreeSurface(my_image);
    if (!my_texture) {
        printf("Echec de la creation de la texture : %s\n", SDL_GetError());
        SDL_Quit();
        exit(EXIT_FAILURE);
    }

    return my_texture;
}

SDL_Rect dessinrect(int rectX, int rectY, int tX, int tY) {
    SDL_Rect rect;
    rect.x = rectX;
    rect.y = rectY;
    rect.w = tX;
    rect.h = tY;

    return rect;
}

int main(int argc, char **argv) {
    (void)argc;
    (void)argv;

    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        SDL_Log("Error : SDL initialisation - %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    SDL_DisplayMode displayMode;
    if (SDL_GetCurrentDisplayMode(0, &displayMode) != 0) {
        fprintf(stderr, "Impossible de récupérer le Display Mode : %s\n", SDL_GetError());
        SDL_Quit();
        return 1;
    }

    int largeur_f = displayMode.w - 400;
    int hauteur_f = displayMode.h - 400;

    SDL_Window *window = SDL_CreateWindow("Fenêtre", 200, 200, largeur_f, hauteur_f, SDL_WINDOW_RESIZABLE);
    if (window == NULL) {
        SDL_Log("Error : SDL window creation - %s\n", SDL_GetError());
        SDL_Quit();
        exit(EXIT_FAILURE);
    }

    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (!renderer) {
        printf("Echec de la creation du renderer : %s\n", SDL_GetError());
        SDL_DestroyWindow(window);
        SDL_Quit();
        exit(EXIT_FAILURE);
    }

    SDL_Texture *texture = load_texture_from_image("sprites/sora.png", renderer);
    SDL_Texture *fond = load_texture_from_image("sprites/fond.jpg", renderer);

    SDL_Rect dest;
    SDL_Rect src;

    SDL_Event event;
    SDL_bool running = SDL_TRUE;

    int centreX = largeur_f / 2;
    int centreY = hauteur_f / 2;

    int dir = 0;
    int pos = 0;
    int i = 0;
    int decal = 0;

    while (running) {
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                running = SDL_FALSE;
            }
        }

        const Uint8 *state = SDL_GetKeyboardState(NULL);
        if (state[SDL_SCANCODE_UP]) {
            centreY -= 5;
            dir = 3;
        }
        if (state[SDL_SCANCODE_DOWN]) {
            centreY += 5;
            dir = 0;
        }
        if (state[SDL_SCANCODE_LEFT]) {
            centreX -= 5;
            dir = 1;
        }
        if (state[SDL_SCANCODE_RIGHT]) {
            centreX += 5;
            dir = 2;
        }

        if (state[SDL_SCANCODE_UP] || state[SDL_SCANCODE_DOWN] || state[SDL_SCANCODE_LEFT] || state[SDL_SCANCODE_RIGHT]) {
            i++;
            if (pos == 1) {
                pos = 0;
            } else if (pos == 0 && i == 6) {
                pos = 2;
                i = 0;
            } else if (pos == 2 && i == 6) {
                pos = 0;
                i = 0;
            }
        }
        else {
            pos = 1;
            i = 0;
        }

        if (centreX - 60 <= 0 || centreY - 77 <= 0) {
            if (centreX - 60 <= 0) {
                centreX = 61;
            }
            if (centreY - 77 <= 0) {
                centreY = 78;
            }
            pos = 1;
        } else if (centreX + 60 >= largeur_f || centreY + 77 >= hauteur_f) {
            if (centreX + 60 >= largeur_f) {
                centreX = largeur_f - 61;
            }
            if (centreY + 77 >= hauteur_f) {
                centreY = hauteur_f - 78;
            }
            pos = 1;
        }

        SDL_SetRenderDrawColor(renderer, 32, 32, 32, 255);
        SDL_RenderClear(renderer);
        dest = dessinrect(0, 0, largeur_f, hauteur_f);
        SDL_RenderCopy(renderer, fond, NULL, &dest);

        if (dir == 0) {
            if (pos == 0) {
                decal = 22;
            } else if (pos == 2) {
                decal = -22;
            }
        }
        dest = dessinrect(centreX - 60 + decal, centreY - 77, 120, 154);
        decal = 0;
        src = dessinrect(pos * 60, dir * 77, 60, 77);

        SDL_RenderCopy(renderer, texture, &src, &dest);
        SDL_RenderPresent(renderer);

        SDL_Delay(16);
    }

    SDL_DestroyTexture(texture);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);

    IMG_Quit();
    SDL_Quit();

    return 0;
}