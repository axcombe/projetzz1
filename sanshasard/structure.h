#ifndef STRUCTURE_H
#define STRUCTURE_H

enum Ribbon {
    NOTHING = 0,
    RED_RECT = 1,
};

enum Form {
    SQUARE = 0,
    CROSS = 1,
};

enum Color {
    BLACK = 0,
    WHITE = 1,
};

enum Empty {
    FULL = 0,
    EMPTY = 1,
};

typedef struct {
    int num;
    int ribbon; // 0 = nothing or 1 = red rectangle
    int form; // 0 = square or 1 = cross
    int color; // 0 = black or 1 = white
    int empty; // 0 = full or 1 = empty
} PieceLo;

typedef struct {
    PieceLo *piecelo;
} PieceSDL;

typedef struct {
    int num;
    int Mx;
    int My;
    int radius;
} Box;

typedef struct {
    int x;
    int y;
    int w;
    int h;
} BoxPiece;

typedef struct {
    int ** possible_moves;
    int nb_moves;

}movements;



typedef struct {
    PieceLo pieces[16]; 
    int piece/*ToCircle*/[16];// 0 to 15 is num of box, -1 is NONE
    int circle/*ToPiece*/[16];// 0 to 15 is num of piece , -1 is NONE
    int pieceselected;//
    int pieceplayed;//-1 NONE, 0 to 15  Piece played
    int num_coup;
    int modified;//0 pas de modif 1 y a modif
    movements move;
} Board_state;

typedef struct{
    Box box[16];
    BoxPiece * rect; 
}Zone_SDL;

#endif 
