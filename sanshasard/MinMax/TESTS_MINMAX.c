#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "../structure.h"

#include "../logique.c"

#include "algorithme_minmax.c"
#include "minmax.h"


int main()
{

    Board_state board; // objet du type de la structure board_state

    initGameLogique(&board);


    // printf("[%d,%d]\n",board.move.possible_moves[0][0], board.move.possible_moves[0][1]);
    // printf("[%d,%d]\n",board.move.possible_moves[1][0], board.move.possible_moves[1][1]);

    int connex = 0;
    int valu = 0;

    // TEST DE SITUATION GAGNANTE POUR LE JOUEUR
    /*
        board.circle[5] = 0;
        board.circle[0] = 1;
        board.circle[10] = 2;
        board.circle[15] = 3;

        connex = 0;
        valu = 0;
        connexity(&board,&connex);
        valu = valuate(&board, 1);

        printf("game over ? = %d\n", isGameOver(board));
        printf("connexité = %d\n", connex);
        printf("valuate = %d\n", valu);

    */

    // TEST DE SITUATION GAGNANTE POUR LE minMax
    /*
        connex = 0;
        valu = 0;
        connexity(&board,&connex);
        valu = valuate(&board, 0);

        printf("game over ? = %d\n", isGameOver(board));
        printf("connexité = %d\n", connex);
        printf("valuate = %d\n", valu);
    */

    // TEST DE SITUATION D'EGALITE SANS VICTOIRES Pour le tour du minMax
    /*
        board.circle[0] = 11;
        board.circle[1] = 15;
        board.circle[2] = 0;
        board.circle[3] = 5;
        board.circle[4] = 8;
        board.circle[5] = 1;
        board.circle[6] = 4;
        board.circle[7] = 2;
        board.circle[8] = 3;
        board.circle[9] = 9;
        board.circle[10] = 12;
        board.circle[11] = 6;
        board.circle[12] = 14;
        board.circle[13] = 10;
        board.circle[14] = 7;
        board.circle[15] = 13;

        board.num_coup=16;

        connex = 0;
        valu = 0;
        connexity(&board,&connex);
        valu = valuate(&board, 0);

        printf("game over ? = %d\n", isGameOver(board));
        printf("connexité = %d\n", connex);
        printf("valuate = %d\n", valu);
    */

    // TEST boardPlay sur tableau vide
    /*
        int coup[2];
        coup[0] = 11;
        coup[1] = 0;

        boardPlay(coup, &board);

        printf("sur le cercle 0 se trouve la piece : %d\n", board.circle[0]);

        coup[0] = 5;
        coup[1] = 6;

        boardPlay(coup, &board);

        printf("sur le cercle 6 se trouve la piece : %d\n", board.circle[6]);
        printf("la piece precedement ajoutée est : %d\n", board.pieceplayed);
    */

    // TEST boardPlay sur tableau rempli en cercle 0, piece 5
    /*
        int coup[2];
        coup[0] = 5;
        coup[1] = 0;

        boardPlay(coup, &board);


        printf("sur le cercle 6 se trouve la piece : %d\n", board.circle[6]);
        printf("la piece precedement ajoutée est : %d\n", board.pieceplayed);

        boardUndo(&board);

        printf("sur le cercle 6 se trouve la piece : %d\n", board.circle[6]);
        printf("la piece precedement ajoutée est : %d\n", board.pieceplayed);

        printf("what %d\n", board.move.possible_moves[0][0]);

    */

    // TEST AFFICHAGE DES COUPS POSSIBLES

    /*displayPossibleMoves(board);

    int coup [2] ;
    coup [0]=0;
    coup [1]=0;

    boardPlay(coup,&board);

    printf("emplacement de la piece 0 : %d\n", board.piece[0]);

    printf("le cercle 0 contient la piece : %d\n", board.circle[0]);

    displayPossibleMoves(board);

    boardUndo(&board);

    //addPossibleMoves(&board);

    displayPossibleMoves(board);


    //addPossibleMoves(&board);

    */


//TEST MINMAX PROFONDEUR 2 ET 3 (résultats différents)
/* 
    int coup [2] ;
    coup [0]=15;
    coup [1]=15;

    
    boardPlay(coup, &board);  //On remplit une case pour observer le comportement vis à vis de la case


    int * result = toPlayMinMax(3,&board,1);

    printf("result of minmax : [%d,%d] \n",result[0], result[1]);


*/

// TEST D'UNE SITUATION GAGNANTE (est censé renvoyer la piece 8 ou 12 ou 13 sur la case 12)



    int coup [2] ;
    coup [0]=14;
    coup [1]=0;

    boardPlay(coup, &board);

    coup[0]=9;
    coup[1]=4;

    boardPlay(coup, &board);

    coup[0]=10;
    coup[1]=8;

    boardPlay(coup, &board);
/*
    coup[0]=8;
    coup[1]=12;

    boardPlay(coup, &board);
*/
    printf("board case 0 cintient piece : %d\n", board.circle[8]);
    printf("board piece 8 est dans case : %d\n", board.piece[8]);


    int * result = toPlayMinMax(1,&board,1);

    printf("result of minmax : [%d,%d] \n",result[0], result[1]);

    printf("is game over : %d\n", isGameOver(board));

}
