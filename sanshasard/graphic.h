#ifndef GRAPHIC_H
#define GRAPHIC_H

#include "structure.h"

void initGameSDL(SDL_Window **,SDL_Renderer **,SDL_Texture **);
SDL_Texture* load_texture_from_image(char *, SDL_Renderer *);
void updateGameSDL(SDL_Window *,SDL_Renderer *,Board_state *, Zone_SDL *,SDL_Texture **, int,int*);
void animation(SDL_Renderer *, PieceLo *, int, Zone_SDL *,SDL_Texture *);


void getZone_SDLCenterSDL(SDL_Window *,Zone_SDL *);
void getBoxPieceCenterSDL(BoxPiece *, int);
void getBoxCenterSDL(Box *, int, int);

void drawCircleSDL(SDL_Renderer *, int, int, int);
void drawBoxSDL(SDL_Renderer *, int, int,int);
void drawPieceSDL(SDL_Renderer*,PieceLo,int,Zone_SDL*,int);
void drawStorageSDL(SDL_Renderer*, BoxPiece *,SDL_Texture*);

void endSDL(SDL_Window *,SDL_Renderer *);

//void initGameLogique (Board_state *);
void drawRectPiece (SDL_Renderer * renderer,int where,int numpiece,int color, int ribbon, int empty, Zone_SDL zone, int alpha);
void drawCrossPiece (SDL_Renderer * renderer,int where,int numpiece,int color, int ribbon, int empty, Zone_SDL zone, int alpha);


void affichage_fin(SDL_Window *window, SDL_Renderer *renderer, int joueur, int running);
#endif