#include <SDL2/SDL.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "eventhandle.h"
#include "logique.h"
#include "graphic.h"
#include "structure.h"
#include "minmax.h"
#include <SDL2/SDL_image.h>

#define DEPTH 1

int main()
{
    int multijoueur = 0; // variable pour savoir combien de joueur : 0 un seul joueur 1 deux joueurs
    int running = 1;     // variable d'état du jeu. Vaut 0 si le jeu est fini, 1 sinon
    int player = 0;      // variable d'information du jeu (joueur = 1 (l'algorithme de meilleur coup) ou joueur = 0 (le joueur physique)pour décrire les différents joueurs)

    int piece_select = 0;
    int player_intention = -1;
    int *bot_intention = NULL;
    int *bot_intention_free[8];
    int index_free = 0;

    int offset[1];
    offset[0] = 0;

    SDL_Window *window = NULL;
    SDL_Renderer *renderer = NULL;
    SDL_Texture *Textures[4];
    Textures[0] = NULL;
    Textures[1] = NULL;
    Textures[2] = NULL;
    Textures[3] = NULL;

    Board_state board; // objet du type de la structure board_state

    Zone_SDL zone;
    zone.rect = malloc(sizeof(BoxPiece));
    getZone_SDLCenterSDL(window, &zone); // j'ai changer pour avoir un Zone_SDL ici surement structure de données à modifier encore;

    initGameLogique(&board);

    initGameSDL(&window, &renderer, Textures);
    updateGameSDL(window, renderer, &board, &zone, Textures, player, offset);

    while (running == 1)
    { // tant que le jeu n'est pas fini

        getZone_SDLCenterSDL(window, &zone);
        if (multijoueur == 0)
            if (player == 0)
            { // Si c'est au joueur physique

                player_intention = pollEvent(window, piece_select, zone, &running);
            }

            else
            {
                // bot_intention = toPlayMinMax(DEPTH, &board, 1); // doit renvoyer un int[2]={numero_piece, numero_cercle}
                // bot_intention = toPlayMinMax(DEPTH, &board, 1); // doit renvoyer un int[2]={numero_piece, numero_cercle}
                bot_intention_free[index_free] = toPlayMinMax(DEPTH, &board, 1);
                bot_intention = bot_intention_free[index_free];
                index_free += 1;
            }
        else
        {
            player_intention = pollEvent(window, piece_select, zone, &running);
        }

        updateGameLogique(multijoueur, &player, &piece_select, player_intention, bot_intention, &board, &running);
        updateGameSDL(window, renderer, &board, &zone, Textures, player, offset);
        board.modified = 0;
    }
    if (running != 3)
    {
        updateGameSDL(window, renderer, &board, &zone, Textures, player, offset);
        affichage_fin(window, renderer, player, running);
        SDL_Delay(5000);
    }
    // endSDL(window,renderer);
    for (int i=0;i<4;++i){
        SDL_DestroyTexture(Textures[i]);
    }

    free(zone.rect);
    for (int i = 0; i < 256; i++)
    {
        free(board.move.possible_moves[i]);
    }
    free(board.move.possible_moves);
    for (int i = 0; i < index_free; ++i)
    {
        free(bot_intention_free[i]);
    }
}
