int COLOR ={{150,50,50},{0,0,0},{255,255,255}};


void drawRectPiece (SDL_Renderer * renderer,int where,int numpiece,int color, int ribbon, int empty, Zone_SDL zone, int alpha){
    SDL_SetRenderDrawColor(renderer, COLOR[color][0], COLOR[color][1], COLOR[color][2], alpha);
    if (where !=-1){
        int Mx = zone.box[where].Mx;
        int My = zone.box[where].My;
        int R = zone.box[where].radius;
        float w = sqrt(2)*R;
        float x = Mx - w/2.0;
        float y = My - w/2.0;
        SDL_Rect rect1 = {x/1,y/1,w/1,w/1};
        SDL_RenderFillRect(renderer,&rect1);
        if (ribbon){
            SDL_SetRenderDrawColor(renderer,255,0,0, alpha);
            float xr = x;
            float yr = y + w*4.0/9.0;
            float wr= w;
            float hr=w/9.0;
            SDL_Rect rectr = {xr/1,yr/1,wr/1,hr/1};
            SDL_RenderFillRect(renderer,&rectr);
        }
        if (empty){
            SDL_SetRenderDrawColor(renderer,COLOR[2][0], COLOR[2][1], COLOR[2][2], alpha);
            float we= 0.5*w;
            float xe = x + (w-we)/2;
            float ye = y + (w-we)/2; 
            SDL_Rect recte = {xe/1,ye/1,we/1,we/1};
            SDL_RenderFillRect(renderer,&recte);
        }
    }
    else{
        int i=numpiece/2;
        int j=numpiece%2;
        float w=0.8* zone.rect->w;
        float x=(zone.rect->x + j*zone.rect->w)+(zone.rect->w -w)/2.0;
        float y=(zone.rect->y + i*zone.rect->w)+(zone.rect->w -w)/2.0;

        SDL_Rect rect1 = {x/1,y/1,w/1,w/1};
        SDL_RenderFillRect(renderer,&rect1);
        if (ribbon){
            SDL_SetRenderDrawColor(renderer,255,0,0, alpha);
            float xr = x;
            float yr = y + w*4.0/9.0;
            float wr= w;
            float hr=w/9.0;
            SDL_Rect rectr = {xr/1,yr/1,wr/1,hr/1};
            SDL_RenderFillRect(renderer,&rectr);
        }
        if (empty){
            SDL_SetRenderDrawColor(renderer,COLOR[2][0], COLOR[2][1], COLOR[2][2], alpha);
            float we= 0.5*w;
            float xe = x + (w-we)/2;
            float ye = y + (w-we)/2; 
            SDL_Rect recte = {xe/1,ye/1,we/1,we/1};
            SDL_RenderFillRect(renderer,&recte);
        }
    }
}



        

void drawCrossPiece (SDL_Renderer * renderer,int where,int numpiece,int color, int ribbon, int empty, Zone_SDL zone, int alpha){
    SDL_SetRenderDrawColor(renderer, COLOR[color][0], COLOR[color][1], COLOR[color][2], alpha);
    if (where !=-1){
        int Mx = zone.box[where].Mx;
        int My = zone.box[where].My;
        int R = zone.box[where].radius;
        float w = sqrt(2)*R;
        float x = Mx - w/2.0;
        float y = My - w/2.0;
        float x1= x;
        float x2= x + w/3.0;
        float y1= y + w/3.0;
        float y2= y;
        float w1=w;
        float w2=w/3.0;
        float h1=w/3.0;
        float h2=w;

        printf("%f %f %f %f\n",x1,y1,w1,h1);
        SDL_Rect rect1 = {x1/1,y1/1,w1/1,h1/1};
        SDL_Rect rect2 = {x2/1,y2/1,w2/1,h2/1};
        SDL_RenderFillRect(renderer,&rect1);
        SDL_RenderFillRect(renderer,&rect2);
        if (ribbon){
            SDL_SetRenderDrawColor(renderer,255,0,0, alpha);
            float xr = x;
            float yr = y + w*4.0/9.0;
            float wr= w;
            float hr=w/9.0;
            SDL_Rect rectr = {xr/1,yr/1,wr/1,hr/1};
            SDL_RenderFillRect(renderer,&rectr);
        }
        if (empty){
            SDL_SetRenderDrawColor(renderer,COLOR[2][0], COLOR[2][1], COLOR[2][2], alpha);
            float we= 0.5*w;
            float xe = x + (w-we)/2;
            float ye = y + (w-we)/2; 
            float xe1= xe;
            float xe2= xe + we/3.0;
            float ye1= ye + we/3.0;
            float ye2= ye;
            float we1=we;
            float we2=we/3.0;
            float he1=we/3.0;
            float he2=we;

            SDL_Rect rect1 = {xe1/1,ye1/1,we1/1,he1/1};
            SDL_Rect rect2 = {xe2/1,ye2/1,we2/1,he2/1};
            SDL_RenderFillRect(renderer,&rect1);
            SDL_RenderFillRect(renderer,&rect2);
        }
    }
    else{
        int i=numpiece/2;
        int j=numpiece%2;
        float w=0.8* zone.rect->w;
        float x=(zone.rect->x + j*zone.rect->w)+(zone.rect->w -w)/2.0;
        float y=(zone.rect->y + i*zone.rect->w)+(zone.rect->w -w)/2.0;
        float x1= x;
        float x2= x + w/3.0;
        float y1= y + w/3.0;
        float y2= y;
        float w1=w;
        float w2=w/3.0;
        float h1=w/3.0;
        float h2=w;

        printf("%f %f %f %f\n",x1,y1,w1,h1);
        SDL_Rect rect1 = {x1/1,y1/1,w1/1,h1/1};
        SDL_Rect rect2 = {x2/1,y2/1,w2/1,h2/1};
        SDL_RenderFillRect(renderer,&rect1);
        SDL_RenderFillRect(renderer,&rect2);
        if (ribbon){
            SDL_SetRenderDrawColor(renderer,255,0,0, alpha);
            float xr = x;
            float yr = y + w*4.0/9.0;
            float wr= w;
            float hr=w/9.0;
            SDL_Rect rectr = {xr/1,yr/1,wr/1,hr/1};
            SDL_RenderFillRect(renderer,&rectr);
        }
        if (empty){
            SDL_SetRenderDrawColor(renderer,COLOR[2][0], COLOR[2][1], COLOR[2][2], alpha);
            float we= 0.5*w;
            float xe = x + (w-we)/2;
            float ye = y + (w-we)/2; 
            float x1= x;
            float x2= x + w/3.0;
            float y1= y + w/3.0;
            float y2= y;
            float w1=w;
            float w2=w/3.0;
            float h1=w/3.0;
            float h2=w;

            printf("%f %f %f %f\n",x1,y1,w1,h1);
            SDL_Rect rect1 = {x1/1,y1/1,w1/1,h1/1};
            SDL_Rect rect2 = {x2/1,y2/1,w2/1,h2/1};
            SDL_RenderFillRect(renderer,&rect1);
            SDL_RenderFillRect(renderer,&rect2);
        }
    }
}