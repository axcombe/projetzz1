#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include "graphic.h"

int COLOR[3][3] ={{250,250,250},{20,50,100},{0,0,0}};

void initGameSDL(SDL_Window **window, SDL_Renderer **renderer,SDL_Texture ** Textures) {
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
        endSDL(*window, *renderer);
    }
    
    SDL_DisplayMode dim;
    if (SDL_GetCurrentDisplayMode(0, &dim) != 0) {
        printf("SDL could not get display mode! SDL_Error: %s\n", SDL_GetError());
        endSDL(*window, *renderer);
    }

    float WINDOW_SCALE = 0.8;   // percentage of the window size relative to the screen

    int sizedevicex = dim.w;
    int sizedevicey = dim.h;

    int sizewindowx = sizedevicex * WINDOW_SCALE;
    int sizewindowy = sizedevicey * WINDOW_SCALE;

    *window = SDL_CreateWindow("Quarto Board", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, sizewindowx, sizewindowy, SDL_WINDOW_RESIZABLE);
    if (*window == NULL) {
        printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
        endSDL(*window, *renderer);
    }

    *renderer = SDL_CreateRenderer(*window, -1, SDL_RENDERER_ACCELERATED);
    if (*renderer == NULL) {
        printf("Renderer could not be created! SDL_Error: %s\n", SDL_GetError());
        endSDL(*window, *renderer);
    }
    SDL_SetRenderDrawBlendMode(*renderer,SDL_BLENDMODE_BLEND);

    Textures[0] = load_texture_from_image("sprite/wooden_base.png", *renderer);
    Textures[1]  = load_texture_from_image("sprite/Train.png",*renderer);
    Textures[2] = load_texture_from_image("sprite/background2.png",*renderer);
    Textures[3] = load_texture_from_image("sprite/mountain_parallaxe.jpg",*renderer);

    if (TTF_Init() == -1) {
        printf("TTF_Init: %s\n", TTF_GetError());
        endSDL(*window, *renderer);
    }
 
}

SDL_Texture* load_texture_from_image(char *file_image_name, SDL_Renderer *renderer) {
    SDL_Surface *my_image = IMG_Load(file_image_name);
    if (my_image == NULL) {
        printf("Echec de la creation de l'image : %s\n", SDL_GetError());
        SDL_Quit();
        exit(EXIT_FAILURE);
    }

    SDL_Texture* my_texture = SDL_CreateTextureFromSurface(renderer, my_image);
    SDL_FreeSurface(my_image);
    if (!my_texture) {
        printf("Echec de la creation de la texture : %s\n", SDL_GetError());
        SDL_Quit();
        exit(EXIT_FAILURE);
    }

    return my_texture;
}

void updateGameSDL(SDL_Window *window, SDL_Renderer *renderer, Board_state *board_state, Zone_SDL *zone, SDL_Texture ** Textures, int player,int *offset) {

    // background
    SDL_Rect rect_background={0,0,0,0};

    // background2
    SDL_Rect rect_background2={0,0,0,0};

    //parallaxe
    SDL_Rect rect_mountain={0,0,0,0};
    SDL_Rect src_mountain={0,0,0,0};




    // gestion surbrillance lors de la selection
    int surbrillance=board_state->pieceselected;
    SDL_Rect rect_surbrillance={0,0,0,0};

    // gestion animation lors du déplacement
    int modified = board_state->modified;
    int pieceplayed = board_state->pieceplayed;


    int sizewindowx;
    int sizewindowy;

    SDL_GetWindowSize(window, &sizewindowx, &sizewindowy);


    int centrex;
    int centrey;

    int board_radius;

    int i;

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);

    //Gestion de la parallaxe

    *offset -= 5;
    *offset %= sizewindowx; 




    rect_mountain.w = sizewindowx;
    rect_mountain.h = sizewindowy/2;


    rect_mountain.x = *offset;
    rect_mountain.y = 0;

    SDL_QueryTexture(Textures[3], NULL, NULL, // Récupération des dimensions de l'image
                &src_mountain.w, &src_mountain.h);


    src_mountain.x=0;
    src_mountain.h = src_mountain.h/3 ;
    src_mountain.y = 2*src_mountain.h;

    SDL_RenderCopy(renderer,Textures[3],&src_mountain,&rect_mountain);

    rect_mountain.x += sizewindowx;

    SDL_RenderCopy(renderer,Textures[3],&src_mountain,&rect_mountain);

    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);


    centrex = sizewindowx / 2;
    centrey = sizewindowy / 2;

    board_radius = 0.45 * sizewindowy;

    rect_background.x=0;
    rect_background.y=0;
    rect_background.w=sizewindowx;
    rect_background.h=sizewindowy;
    SDL_RenderCopy(renderer, Textures[1], NULL, &rect_background);

    rect_background2.x=centrex-board_radius;
    rect_background2.y=centrey-board_radius;
    rect_background2.w=2*board_radius;
    rect_background2.h=2*board_radius;
    SDL_RenderCopy(renderer, Textures[2], NULL, &rect_background2);


    drawBoxSDL(renderer, centrex, centrey, board_radius);    // large circle
    drawStorageSDL(renderer,zone->rect,Textures[0] );

    for (i = 0; i < 16; ++i) {

        if (i==surbrillance){
            SDL_SetRenderDrawColor(renderer,200,200,200,50);
            rect_surbrillance.x=(zone->rect->x)+(zone->rect->w)*(i%2);
            rect_surbrillance.y=(zone->rect->y)+(zone->rect->w)*(i/2);
            rect_surbrillance.w=zone->rect->w;
            rect_surbrillance.h=zone->rect->h;
            
            SDL_RenderFillRect(renderer, &rect_surbrillance);
        }
        if (!modified || i!=pieceplayed){
            drawPieceSDL(renderer, board_state->pieces[i],board_state->piece[i],zone,255);
        }
        SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
        drawBoxSDL(renderer, zone->box[i].Mx, zone->box[i].My, zone->box[i].radius); // draw a box at (boxx, boxy)

    }
    TTF_Font *font = TTF_OpenFont("font/font1.ttf", 52);
    if (!font) {
        fprintf(stderr, "Erreur de chargement de la police: %s\n", TTF_GetError());
        TTF_Quit();
        return;
    } 

    SDL_Color textColor = {0,0,0,255};
    SDL_Surface *textSurface = NULL;
    SDL_Texture *textTexture = NULL;
    SDL_Rect textRect;

    char joueurText[20];
    snprintf(joueurText, sizeof(joueurText),"Joueur %d",player);
    textSurface = TTF_RenderText_Blended(font, joueurText, textColor);
    textTexture = SDL_CreateTextureFromSurface(renderer,textSurface);

    int textwidth = 0.15 * sizewindowx;
    int textheight = 0.10 * sizewindowy;
    
    textRect.x = sizewindowx - textwidth - 0.05*sizewindowy;
    textRect.y = sizewindowy/2 - textheight/2;
    // printf("%d %d %d %d %d %d\n",sizewindowx,sizewindowy,textwidth,textheight,textRect.x, textRect.y);

    textRect.w = textwidth;
    textRect.h = textheight;
    // SDL_RenderFillRect(renderer, &textRect);
    SDL_RenderCopy(renderer, textTexture, NULL, &textRect);
    TTF_CloseFont(font);
    SDL_FreeSurface(textSurface);
    SDL_DestroyTexture(textTexture);


    // Charger une police de caractères
    TTF_Font *font = TTF_OpenFont("font/font1.ttf", 52);
    if (!font) {
        fprintf(stderr, "Erreur de chargement de la police: %s\n", TTF_GetError());
        TTF_Quit();
        return;
    }

    SDL_Color textColor = {0, 0, 0, 255}; // Couleur du texte (noir)
    SDL_Surface *textSurface = NULL;
    SDL_Texture *textTexture = NULL;
    SDL_Rect textRect;
    
    // Texte à afficher
    char joueurText[20];
    snprintf(joueurText, sizeof(joueurText), "Joueur %d", player);
    textSurface = TTF_RenderText_Blended(font, joueurText, textColor);
    textTexture = SDL_CreateTextureFromSurface(renderer, textSurface);
    
    // Dimensions du texte
    textRect.x = sizewindowx - (0.4 * sizewindowy) - 0.05*sizewindowy;
    textRect.y = sizewindowy/2 - (0.25 * sizewindowy)/2; 
    textRect.w = 0.4 * sizewindowy;
    textRect.h = 0.25 * sizewindowy;

    // SDL_Rect R={0,0,0,0};
    // R.x = sizewindowx - ((sizewindowx/2)-board_radius-0.1*sizewindowy) - 0.05*sizewindowy;
    // R.y = sizewindowy/2 - (0.4 * ((sizewindowx/2)-board_radius-0.1*sizewindowy))/2; 
    // R.w = (sizewindowx/2)-board_radius-0.1*sizewindowy;
    // R.h = 0.4 * ((sizewindowx/2)-board_radius-0.1*sizewindowy);
    // SDL_RenderFillRect(renderer, &R);
    
    SDL_RenderCopy(renderer, textTexture, NULL, &textRect);
    
    SDL_FreeSurface(textSurface);
    SDL_DestroyTexture(textTexture);

    if (modified){
        animation(renderer,&(board_state->pieces[pieceplayed]),board_state->piece[pieceplayed],zone,Textures[0] );
    }

    SDL_RenderPresent(renderer);
    SDL_Delay(16);

}

void animation(SDL_Renderer *renderer, PieceLo *piece, int where, Zone_SDL *zone,SDL_Texture *wooden_base){
    int alpha;
    SDL_Rect r={0,0,0,0};
    r.x=(zone->rect->x) + (piece->num)%2 * (zone->rect->w); 
    r.y=(zone->rect->y) + ((piece->num)/2)*(zone->rect->w);
    r.w=zone->rect->w;
    r.h=zone->rect->h;
    for (alpha=0;alpha<256;alpha=alpha+1){
        SDL_RenderCopy(renderer, wooden_base , NULL, &r);

        SDL_SetRenderDrawColor(renderer, 255, COLOR[2][1], COLOR[2][2], alpha);
        SDL_RenderDrawRect(renderer, &r);

        drawPieceSDL(renderer, *piece,where,zone,alpha);
        drawPieceSDL(renderer, *piece,-1,zone,255-alpha);

        SDL_RenderPresent(renderer);
        SDL_Delay(1);
    }
}

void getZone_SDLCenterSDL(SDL_Window * window,Zone_SDL * zone){
    int sizewindowx = 0;
    int sizewindowy = 0;
    SDL_GetWindowSize(window, &sizewindowx, &sizewindowy);

    getBoxPieceCenterSDL(zone->rect,sizewindowy);
    getBoxCenterSDL(zone->box,sizewindowx,sizewindowy);

    
}

void getBoxPieceCenterSDL(BoxPiece *rect, int sizewindowy){  // rempli un BoxPiece
    int offset = 0.05 * sizewindowy;
    int cote = (0.9 * sizewindowy)/8;


    rect->h = cote;
    rect->w = cote;
    rect->x = offset;
    rect->y = offset;


}

void getBoxCenterSDL(Box * box,int sizewindowx,int sizewindowy){    // rempli un tableau de Box, par leur coordonnées


    int offset; // offset between the 4 outer boxes and the board
    int board_radius; // radius of the board
    int radius; // radius of the boxes

    double board_radius_to_radius = 0.16;
    double board_radius_to_offset = 0.03;

    int centrex;
    int centrey;

    int boxx;   // X coordinate of the current box to draw
    int boxy;   // Y coordinate of the current box to draw

    int first_boxx; // X coordinate of the first box to draw in each loop
    int first_boxy; // Y coordinate of the first box to draw in each loop

    int box_offset; // offset between each box (in X and Y coordinates)

    int i, j;

    centrex = sizewindowx / 2;
    centrey = sizewindowy / 2;

    board_radius = 0.45 * sizewindowy;
    radius = board_radius * board_radius_to_radius;
    offset = board_radius * board_radius_to_offset;

    centrex = sizewindowx / 2;
    centrey = sizewindowy / 2;

    box_offset = (centrex - (centrex - board_radius + offset + radius)) / 3;

    first_boxx = centrex - board_radius + offset + radius;
    first_boxy = centrey;

    for (i = 0; i < 4; ++i) {
        for (j = 0; j < 4; ++j) {
            boxx = first_boxx + j * box_offset;
            boxy = first_boxy + j * box_offset;

            box[i*4 + j].Mx = boxx;
            box[i*4 + j].My = boxy;
            box[i*4 + j].num = i*4 + j;
            box[i*4 + j].radius = radius;
        }
        first_boxx = first_boxx + box_offset;
        first_boxy = first_boxy - box_offset;
    }
}
// Bresenham's circle algorithm
void drawCircleSDL(SDL_Renderer *renderer, int centrex, int centrey, int radius) {
    int x = radius;
    int y = 0;
    int tx = 1;
    int ty = 1;
    int err = tx - (radius << 1);

    while (x >= y) {
        // 8 octants
        SDL_RenderDrawPoint(renderer, centrex + x, centrey - y);
        SDL_RenderDrawPoint(renderer, centrex + x, centrey + y);
        SDL_RenderDrawPoint(renderer, centrex - x, centrey - y);
        SDL_RenderDrawPoint(renderer, centrex - x, centrey + y);
        SDL_RenderDrawPoint(renderer, centrex + y, centrey - x);
        SDL_RenderDrawPoint(renderer, centrex + y, centrey + x);
        SDL_RenderDrawPoint(renderer, centrex - y, centrey - x);
        SDL_RenderDrawPoint(renderer, centrex - y, centrey + x);

        if (err <= 0) {
            y++;
            err += ty;
            ty += 2;
        }

        if (err > 0) {
            x--;
            tx += 2;
            err += (tx - (radius << 1));
        }
    }
}
// draw a box
void drawBoxSDL(SDL_Renderer *renderer, int centrex, int centrey, int radius) {
    drawCircleSDL(renderer, centrex, centrey, radius);
}
// draw a piece
void drawPieceSDL(SDL_Renderer *renderer, PieceLo piece, int where, Zone_SDL *zone, int alpha){ // dessine une piece, et la dessine 
    if (piece.form == 0){
        drawRectPiece(renderer,where,piece.num,piece.color,piece.ribbon,piece.empty,*zone,alpha);
    }
    else {
        drawCrossPiece(renderer,where,piece.num,piece.color,piece.ribbon,piece.empty,*zone,alpha);
    }
}
// draw storages
void drawStorageSDL(SDL_Renderer *renderer,BoxPiece *storage,SDL_Texture *texture){
    int i,j;


    SDL_Rect rect={0,0,0,0};
    rect.h=storage->w;
    rect.w=storage->w;

    for (i=0;i<2;++i){
        rect.x=(storage->x) + i * (storage->w);
        for (j=0;j<8;++j){
            rect.y=(storage->y) + j * (storage->w);
            SDL_RenderCopy(renderer, texture, NULL, &rect);

            SDL_SetRenderDrawColor(renderer, 255, COLOR[2][1], COLOR[2][2], 255);
            SDL_RenderDrawRect(renderer, &rect);

        }

    }
    
}

// end all graphics
void endSDL(SDL_Window *window, SDL_Renderer *renderer) {
    if (renderer) {
        SDL_DestroyRenderer(renderer);
    }
    if (window) {
        SDL_DestroyWindow(window);
    }
   
    TTF_Quit();
    SDL_Quit();
    TTF_Quit();
}

/* void initGameLogique (Board_state *board_state) {
    int tableau[16][4] = {
        {0, 0, 0, 0},
        {0, 0, 0, 1},
        {0, 0, 1, 0},
        {0, 0, 1, 1},
        {0, 1, 0, 0},
        {0, 1, 0, 1},
        {0, 1, 1, 0},
        {0, 1, 1, 1},
        {1, 0, 0, 0},
        {1, 0, 0, 1},
        {1, 0, 1, 0},
        {1, 0, 1, 1},
        {1, 1, 0, 0},
        {1, 1, 0, 1},
        {1, 1, 1, 0},
        {1, 1, 1, 1},
    };

    for (int i = 0; i < 16; i++) {
        board_state->pieces[i].num = i;
        board_state->pieces[i].form = tableau[i][0];
        board_state->pieces[i].empty = tableau[i][1];
        board_state->pieces[i].ribbon = tableau[i][2];
        board_state->pieces[i].color = tableau[i][3];
        board_state->piece[i] = -1;
        board_state->circle[i] = -1;
    }
    board_state->pieceselected = -1;
    board_state->pieceplayed = -1;
    board_state->modified = 0;
    board_state->num_coup = 0;
} */

void drawRectPiece (SDL_Renderer * renderer,int where,int numpiece,int color, int ribbon, int empty, Zone_SDL zone, int alpha){
    SDL_SetRenderDrawColor(renderer, COLOR[color][0], COLOR[color][1], COLOR[color][2], alpha);
    if (where !=-1){
        int Mx = zone.box[where].Mx;
        int My = zone.box[where].My;
        int R = zone.box[where].radius;
        float w = sqrt(2)*R;
        float x = Mx - w/2.0;
        float y = My - w/2.0;
        SDL_Rect rect1 = {x/1,y/1,w/1,w/1};
        SDL_RenderFillRect(renderer,&rect1);
        if (ribbon){
            SDL_SetRenderDrawColor(renderer,255,0,0, alpha);
            float xr = x;
            float yr = y + w*4.0/9.0;
            float wr= w;
            float hr=w/9.0;
            SDL_Rect rectr = {xr/1,yr/1,wr/1,hr/1};
            SDL_RenderFillRect(renderer,&rectr);
        }
        if (empty){
            SDL_SetRenderDrawColor(renderer,COLOR[2][0], COLOR[2][1], COLOR[2][2], alpha);
            float we= 0.5*w;
            float xe = x + (w-we)/2;
            float ye = y + (w-we)/2; 
            SDL_Rect recte = {xe/1,ye/1,we/1,we/1};
            SDL_RenderFillRect(renderer,&recte);
        }
    }
    else{
        int i=numpiece/2;
        int j=numpiece%2;
        float w=0.8* zone.rect->w;
        float x=(zone.rect->x + j*zone.rect->w)+(zone.rect->w -w)/2.0;
        float y=(zone.rect->y + i*zone.rect->w)+(zone.rect->w -w)/2.0;

        SDL_Rect rect1 = {x/1,y/1,w/1,w/1};
        SDL_RenderFillRect(renderer,&rect1);
        if (ribbon){
            SDL_SetRenderDrawColor(renderer,255,0,0, alpha);
            float xr = x;
            float yr = y + w*4.0/9.0;
            float wr= w;
            float hr=w/9.0;
            SDL_Rect rectr = {xr/1,yr/1,wr/1,hr/1};
            SDL_RenderFillRect(renderer,&rectr);
        }
        if (empty){
            SDL_SetRenderDrawColor(renderer,COLOR[2][0], COLOR[2][1], COLOR[2][2], alpha);
            float we= 0.5*w;
            float xe = x + (w-we)/2;
            float ye = y + (w-we)/2; 
            SDL_Rect recte = {xe/1,ye/1,we/1,we/1};
            SDL_RenderFillRect(renderer,&recte);
        }
    }
}


void drawCrossPiece (SDL_Renderer * renderer,int where,int numpiece,int color, int ribbon, int empty, Zone_SDL zone, int alpha){
    SDL_SetRenderDrawColor(renderer, COLOR[color][0], COLOR[color][1], COLOR[color][2], alpha);
    if (where !=-1){
        int Mx = zone.box[where].Mx;
        int My = zone.box[where].My;
        int R = zone.box[where].radius;
        float w = sqrt(2)*R;
        float x = Mx - w/2.0;
        float y = My - w/2.0;
        float x1= x;
        float x2= x + w/3.0;
        float y1= y + w/3.0;
        float y2= y;
        float w1=w;
        float w2=w/3.0;
        float h1=w/3.0;
        float h2=w;

        SDL_Rect rect1 = {x1/1,y1/1,w1/1,h1/1};
        SDL_Rect rect2 = {x2/1,y2/1,w2/1,h2/1};
        SDL_RenderFillRect(renderer,&rect1);
        SDL_RenderFillRect(renderer,&rect2);
        if (ribbon){
            SDL_SetRenderDrawColor(renderer,255,0,0, alpha);
            float xr = x;
            float yr = y + w*4.0/9.0;
            float wr= w;
            float hr=w/9.0;
            SDL_Rect rectr = {xr/1,yr/1,wr/1,hr/1};
            SDL_RenderFillRect(renderer,&rectr);
        }
        if (empty){
            SDL_SetRenderDrawColor(renderer,COLOR[2][0], COLOR[2][1], COLOR[2][2], alpha);
            float we= 0.5*w;
            float xe = x + (w-we)/2;
            float ye = y + (w-we)/2; 
            float xe1= xe;
            float xe2= xe + we/3.0;
            float ye1= ye + we/3.0;
            float ye2= ye;
            float we1=we;
            float we2=we/3.0;
            float he1=we/3.0;
            float he2=we;

            SDL_Rect rect1 = {xe1/1,ye1/1,we1/1,he1/1};
            SDL_Rect rect2 = {xe2/1,ye2/1,we2/1,he2/1};
            SDL_RenderFillRect(renderer,&rect1);
            SDL_RenderFillRect(renderer,&rect2);
        }
    }
    else{
        int i=numpiece/2;
        int j=numpiece%2;
        float w=0.8* zone.rect->w;
        float x=(zone.rect->x + j*zone.rect->w)+(zone.rect->w -w)/2.0;
        float y=(zone.rect->y + i*zone.rect->w)+(zone.rect->w -w)/2.0;
        float x1= x;
        float x2= x + w/3.0;
        float y1= y + w/3.0;
        float y2= y;
        float w1=w;
        float w2=w/3.0;
        float h1=w/3.0;
        float h2=w;

        SDL_Rect rect1 = {x1/1,y1/1,w1/1,h1/1};
        SDL_Rect rect2 = {x2/1,y2/1,w2/1,h2/1};
        SDL_RenderFillRect(renderer,&rect1);
        SDL_RenderFillRect(renderer,&rect2);
        if (ribbon){
            SDL_SetRenderDrawColor(renderer,255,0,0, alpha);
            float xr = x;
            float yr = y + w*4.0/9.0;
            float wr= w;
            float hr=w/9.0;
            SDL_Rect rectr = {xr/1,yr/1,wr/1,hr/1};
            SDL_RenderFillRect(renderer,&rectr);
        }
        if (empty){
            SDL_SetRenderDrawColor(renderer,COLOR[2][0], COLOR[2][1], COLOR[2][2], alpha);
            float we= 0.5*w;
            float xe = x + (w-we)/2;
            float ye = y + (w-we)/2; 
            float xe1= xe;
            float xe2= xe + we/3.0;
            float ye1= ye + we/3.0;
            float ye2= ye;
            float we1=we;
            float we2=we/3.0;
            float he1=we/3.0;
            float he2=we;

            SDL_Rect rect1 = {xe1/1,ye1/1,we1/1,he1/1};
            SDL_Rect rect2 = {xe2/1,ye2/1,we2/1,he2/1};
            SDL_RenderFillRect(renderer,&rect1);
            SDL_RenderFillRect(renderer,&rect2);
        }
    }
}
/* 
int main() {
    SDL_Window *window = NULL;
    SDL_Renderer *renderer = NULL;
    SDL_Texture *wooden_base = NULL;
    initGameSDL(&window, &renderer,&wooden_base);
    SDL_Event e;
    int quit = 0;
    while (!quit) {
        while (SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                quit = 1;
            }
        }
        Zone_SDL zone;
        zone.rect = malloc(sizeof(BoxPiece));
        Board_state board;
        initGameLogique(&board);
        getZone_SDLCenterSDL(window,&zone);
        updateGameSDL(window, renderer, &board,&zone,wooden_base);
        // drawRectPiece(renderer, 12, 7, 1, 1, 0,zone,255);
        // drawCrossPiece(renderer, 7, 15, 0, 1, 1,zone,255);
        SDL_RenderPresent(renderer);

    }
    endSDL(window, renderer);
    return 0;
}
 */

 void affichage_fin(SDL_Window *window, SDL_Renderer *renderer, int joueur, int running) {
    // Dimensions de la fenêtre
    int window_width, window_height;
    SDL_GetWindowSize(window, &window_width, &window_height);
    
    // Couleur du rectangle central (blanc)
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    
    // Dimensions du rectangle central
    int rect_width = 300;
    int rect_height = 200;
    SDL_Rect central_rect = {(window_width - rect_width) / 2, (window_height - rect_height) / 2, rect_width, rect_height};
    SDL_RenderFillRect(renderer, &central_rect);
    
<<<<<<< HEAD
=======
    // // Initialiser SDL_ttf pour le texte
    // if (TTF_Init() == -1) {
    //     fprintf(stderr, "Erreur d'initialisation de TTF: %s\n", TTF_GetError());
    //     return;
    // }
>>>>>>> b1cd82695800bf9064d854b2940e6008f8f5a3e8
    
     // Charger une police de caractères
    TTF_Font *font = TTF_OpenFont("font/font1.ttf", 52);
    if (!font) {
        fprintf(stderr, "Erreur de chargement de la police: %s\n", TTF_GetError());
        TTF_Quit();
        return;
    } 
    
    SDL_Color textColor = {0, 0, 0, 255}; // Couleur du texte (noir)
    SDL_Surface *textSurface = NULL;
    SDL_Texture *textTexture = NULL;
    SDL_Rect textRect;
    
    // Texte à afficher
    if (running == 2) {
        textSurface = TTF_RenderText_Blended(font, "Egalite", textColor);
    } else {
        textSurface = TTF_RenderText_Blended(font, "Victoire", textColor);
    }
    textTexture = SDL_CreateTextureFromSurface(renderer, textSurface);
    
    // Dimensions du texte
    textRect.x = (window_width - textSurface->w) / 2;
    textRect.y = (window_height - textSurface->h) / 2 - 30; // Décaler légèrement vers le haut
    textRect.w = textSurface->w;
    textRect.h = textSurface->h;
    
    SDL_RenderCopy(renderer, textTexture, NULL, &textRect);
    
    SDL_FreeSurface(textSurface);
    SDL_DestroyTexture(textTexture);
    
    if (running != 2) {
        char joueurText[20];
        snprintf(joueurText, sizeof(joueurText), "Joueur %d", joueur);
        textSurface = TTF_RenderText_Blended(font, joueurText, textColor);
        textTexture = SDL_CreateTextureFromSurface(renderer, textSurface);
        
        textRect.x = (window_width - textSurface->w) / 2;
        textRect.y = (window_height - textSurface->h) / 2 + 30; // Décaler légèrement vers le bas
        textRect.w = textSurface->w;
        textRect.h = textSurface->h;
        
        SDL_RenderCopy(renderer, textTexture, NULL, &textRect);
        TTF_CloseFont(font);
        SDL_FreeSurface(textSurface);
        SDL_DestroyTexture(textTexture);
    }
    
    // Libérer les ressources
<<<<<<< HEAD
    TTF_CloseFont(font);
=======
   

>>>>>>> b1cd82695800bf9064d854b2940e6008f8f5a3e8
    
    // Mettre à jour l'affichage
    SDL_RenderPresent(renderer);
}
