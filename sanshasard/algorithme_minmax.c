
#include "minmax.h"

void displayPossibleMoves(Board_state board)
{

    printf("[");

    for (int i = 0; i < board.move.nb_moves; i++)
    {
        if (i < board.move.nb_moves - 1)
        {

            printf("[%d,%d],", board.move.possible_moves[i][0], board.move.possible_moves[i][1]);
        }
        else
        {
            printf("[%d,%d]]\n", board.move.possible_moves[i][0], board.move.possible_moves[i][1]);
        }
    }




}

void displayListPossibleMoves(int ** list, int size)
{

    printf("[");

    for (int i = 0; i < size; i++)
    {
        if (i < size- 1)
        {

            printf("[%d,%d],", list[i][0], list[i][1]);
        }
        else
        {
            printf("[%d,%d]]\n", list[i][0], list[i][1]);
        }
    }

}

void connexity(Board_state *board, int *connexity)
{

    int nb_on_a_line = 0;
    int nb_on_a_col = 0;

    int piece_on_circle = 0;
    int carac_sum[4] = {0, 0, 0, 0};

    for (int i = 0; i < 4; i++)
    {
        nb_on_a_line = 0;

        for (int j = 0; j < 4; j++)
        {

            piece_on_circle = board->circle[(i * 4) + j];

            if (piece_on_circle != -1)
            {
                nb_on_a_line += 1;
                carac_sum[0] += board->pieces[piece_on_circle].ribbon;
                carac_sum[1] += board->pieces[piece_on_circle].color;
                carac_sum[2] += board->pieces[piece_on_circle].empty;
                carac_sum[3] += board->pieces[piece_on_circle].form;
            }
        }
        for (int h = 0; h < 4; h++)
        {
            if (nb_on_a_line != 0 && (carac_sum[h] == nb_on_a_line || carac_sum[h] == 0))
            {
                *connexity += 1;
                carac_sum[h] = 0;
            }
        }

        nb_on_a_col = 0;

        for (int j = 0; j < 4; j++)
        {

            piece_on_circle = board->circle[i + (4 * j)];

            if (piece_on_circle != -1)
            {
                nb_on_a_col += 1;
                carac_sum[0] += board->pieces[piece_on_circle].ribbon;
                carac_sum[1] += board->pieces[piece_on_circle].color;
                carac_sum[2] += board->pieces[piece_on_circle].empty;
                carac_sum[3] += board->pieces[piece_on_circle].form;
            }
        }
        for (int h = 0; h < 4; h++)
        {
            if (nb_on_a_col != 0 && (carac_sum[h] == nb_on_a_col || carac_sum[h] == 0))
            {
                *connexity += 1;
                carac_sum[h] = 0;
            }
        }
    }

    int nb_on_diag1 = 0;
    for (int p = 0; p < 4; p++)
    {
        piece_on_circle = board->circle[5 * p];
        if (piece_on_circle != -1)
        {
            nb_on_diag1 += 1;

            carac_sum[0] += board->pieces[piece_on_circle].ribbon;
            carac_sum[1] += board->pieces[piece_on_circle].color;
            carac_sum[2] += board->pieces[piece_on_circle].empty;
            carac_sum[3] += board->pieces[piece_on_circle].form;
        }
    }

    for (int h = 0; h < 4; h++)
    {
        if (nb_on_diag1 != 0 && (carac_sum[h] == nb_on_diag1 || carac_sum[h] == 0))
        {
            *connexity += 1;
            carac_sum[h] = 0;
        }
    }

    int nb_on_diag2 = 0;
    for (int p = 0; p < 4; p++)
    {
        piece_on_circle = board->circle[3 * (p + 1)];
        if (piece_on_circle != -1)
        {
            nb_on_diag2 += 1;

            carac_sum[0] += board->pieces[piece_on_circle].ribbon;
            carac_sum[1] += board->pieces[piece_on_circle].color;
            carac_sum[2] += board->pieces[piece_on_circle].empty;
            carac_sum[3] += board->pieces[piece_on_circle].form;
        }
    }

    for (int h = 0; h < 4; h++)
    {
        if (nb_on_diag2 != 0 && (carac_sum[h] == nb_on_diag2 || carac_sum[h] == 0))
        {
            *connexity += 1;
            carac_sum[h] = 0;
        }
    }
}

int valuate(Board_state *board, int maximise)

{

    /*
    Si la situation est gagnante pour le joueur valuation = 200 si c'est une egalité 199 sinon 100
    Si la situation est gagnante pour le joueur adverse valuation = 0, egalité valuation = 1, sinon 100

    à cela on rajoute la connexité des pieces deja placées à laquelle on ajoute 100;



    */

    int valuate = 0;

    connexity(board, &valuate);

    if (maximise == 1)
    { // si le coup qui a fini le parcours est un coup de l'adversaire

        if (isGameOver(*board) == 2)
        { // l'adversaire est arrivé à une egalité
            valuate += 1;
        }
        else if (isGameOver(*board) != 0)
        { // l'adversaire n'a pas gagné avant de boucler
            valuate += 100;

        } // dans le cas ou le joueur adverse n'a pas fait égalité et n'a pas pas gagné, alors le joueur adverse a gagné
        // On a donc valuate +=0 car on prend le minimum des options du joueur adverse en priorité, donc celles ou il gagne
    }
    else
    { // si le coup qui a conclut le parcours est un coup du minmax

        if (isGameOver(*board) == 2)
        { // l'adversaire est arrivé à une egalité
            valuate += 199;
        }
        else if (isGameOver(*board) != 0)
        { // l'adversaire n'a pas gagné avant de boucler
            valuate += 100;
        }
        else
        {
            valuate += 200;

        } // dans le cas ou le joueur adverse n'a pas fait égalité et n'a pas pas gagné, alors le joueur adverse a gagné
        // On a donc valuate +=0 car on prend le minimum des options du joueur adverse en priorité, donc celles ou il gagne
    }

    return (valuate);
}


void ListMoves(int ** listmoves, Board_state board, int * nb_coups)
{

    int i = 0;
    int j = 0;
    // printf("nb_coups = %d\n", *nb_coups);


    for (i = 0; i < 16; i++)
    {
        for (j = 0; j < 16; j++)
        {
            if (board.piece[j] == -1 && board.circle[i] == -1)
            {
                // printf("nb_coups = %d\n", *nb_coups);
                listmoves[(*nb_coups)][0] = j;
                listmoves[(*nb_coups)][1] = i;
                *nb_coups += 1;
            }
        }
    }

}


/*

void supprPossibleMoves(Board_state *board) // Supprime des coups possibles les combinaisons qui correspondent au dernier coups joué
{
    int num_circle = board->piece[board->pieceplayed];

    int num_piece = board->pieceplayed;

    // printf("emplacement de la piece 3 : %d\n", num_circle);
    // printf("le cercle 0 contient la piece : %d \n", num_piece);

    int nb_moves = board->move.nb_moves;

    //printf("nb_moves %d",nb_moves);

    for (int i = 0; i < nb_moves; i++)
    {
        // printf("hi! %d\n", board.move.nb_moves);
        if (board->move.possible_moves[i][0] == num_piece || board->move.possible_moves[i][1] == num_circle)
        {
            while(board->move.possible_moves[board->move.nb_moves - 1][0] ==  num_piece || board->move.possible_moves[board->move.nb_moves - 1][1] == num_circle){

                board->move.possible_moves[board->move.nb_moves - 1][0] = -1;
                board->move.possible_moves[board->move.nb_moves - 1][1] = -1;
                board->move.nb_moves -= 1;

            }

            board->move.possible_moves[i][0] = board->move.possible_moves[board->move.nb_moves - 1][0];
            board->move.possible_moves[i][1] = board->move.possible_moves[board->move.nb_moves - 1][1];

            board->move.possible_moves[board->move.nb_moves - 1][0] = -1;
            board->move.possible_moves[board->move.nb_moves - 1][1] = -1;

            board->move.nb_moves -= 1;
        }

    }
    //printf("nb_moves sortie = %d\n",board->move.nb_moves);
}
*/
/*

void addPossibleMoves(Board_state *board)
{

    int circle_num = board->piece[board->pieceplayed];
    int num_piece = board->pieceplayed;

    int i = 0;

    //printf("insertion en queue sur l'element [%d,%d]\n", board->move.possible_moves[board->move.nb_moves - 1][0],board->move.possible_moves[board->move.nb_moves - 1][1] );





    for (i = 0; i < 16; i++)
    {
        if (board->piece[i] == -1)
        {
            board->move.possible_moves[board->move.nb_moves][0] = i;
            board->move.possible_moves[board->move.nb_moves][1] = circle_num;
            board->move.nb_moves += 1;
        }

        if (board->circle[i] == -1)
        {
            board->move.possible_moves[board->move.nb_moves][0] = num_piece;
            board->move.possible_moves[board->move.nb_moves][1] = i;
            board->move.nb_moves += 1;
        }
    }

    board->move.possible_moves[board->move.nb_moves][0] = num_piece;
    board->move.possible_moves[board->move.nb_moves][1] =  circle_num;

    board->move.nb_moves += 1;

}
*/

void boardPlay(int *datas, Board_state *board)
{ // joue le coup de data [piece,cercle] sur le board

    board->pieceplayed = datas[0];

    board->piece[datas[0]] = datas[1];
    board->circle[datas[1]] = datas[0];


    board->num_coup += 1;
}

void boardUndo(Board_state *board)
{

    int circle_played = board->piece[board->pieceplayed];
    board->piece[board->pieceplayed] = -1;
    board->circle[circle_played] = -1;

    board->num_coup -= 1;
}




int * minMax ( int depth,Board_state board, int  maximise){

    if ( isGameOver(board) != 1 || depth == 0){

        int * to_return = malloc(sizeof(int)*3);
        to_return[0]=valuate(&board, maximise);
        to_return[1]=board.pieceplayed;
        to_return[2]=board.piece[board.pieceplayed];

        return(to_return);
    }

    else{
        int valeur = 0;
        int piece_p = -1;
        int circle_p=-1;

        if (maximise == 1){
            int ** list_moves;
            int size_list =0;
            list_moves = malloc(sizeof(int*)*257);

            for (int i=0;i<256;i++ ){
                list_moves[i]=malloc(sizeof(int)*2);
            }

            ListMoves(list_moves,board, &size_list);
            //displayListPossibleMoves(list_moves, size_list);
            // printf(" taille liste coups = %d\n", size_list);

            valeur = -1000;
            
            for (int i=0; i<size_list; i++){
                board.move.possible_moves = list_moves;
                boardPlay(list_moves[i], &board);
                int *curr_valL = minMax(depth-1, board, 0);
                int curr_val=curr_valL[0];
                if (valeur < curr_val){
                    valeur = curr_val;
                    piece_p= board.pieceplayed;
                    circle_p = board.piece[board.pieceplayed];



                }

                free(curr_valL);
                boardUndo(&board);

            }
            for (int i=0;i<256;++i){
                free(list_moves[i]);
            }
            free(list_moves);
        }

        else{
            int ** list_moves;
            int size_list =0;
            list_moves = malloc(sizeof(int*)*256);

            for (int i=0;i<256;i++ ){
                list_moves[i]=malloc(sizeof(int)*2);
            }

            ListMoves(list_moves,board, &size_list);

            valeur = 1000;
            
            for (int i=0; i<size_list; i++){
                board.move.possible_moves = list_moves;
                boardPlay(list_moves[i], &board);
                int *curr_valL = minMax(depth-1, board, 1);
                int curr_val=curr_valL[0];
                if (valeur > curr_val){
                    valeur = curr_val;
                    piece_p= board.pieceplayed;
                    circle_p = board.piece[board.pieceplayed];
                }

                free(curr_valL);
                boardUndo(&board);


            }
            for (int i=0;i<256;++i){
                free(list_moves[i]);
            }
            free(list_moves);



        }

        int *to_return=malloc(sizeof(int)*3);
        to_return[0]=valeur;
        to_return[1]=piece_p;
        to_return[2]=circle_p;

/*         printf("valeur du retour = %d\n", valeur);
        printf("piece à  jouer = %d\n", piece_p);
        printf("cercle à  jouer = %d\n", circle_p);

        printf("yo3\n"); */
        return(to_return);


    }


}




/* int *minMax(int depth, Board_state *board, int maximise)
{

    if (isGameOver(*board) == 0 || depth == 0)
    {
        int previous_piece = board->pieceplayed;
        int send_back[3];

        send_back[0] = valuate(board, maximise);
        send_back[1] = previous_piece;
        send_back[2] = board->piece[previous_piece];

        printf("valuate au niveau depth =%d est : %d\n", depth, send_back[0]);

        return (send_back);
    }

    else
    {

        //   les cobinaisons de coups possibles format (piece,case)
        //   concrètement possible_moves est egal au carré du nombre de peces jouables

        if (maximise == 1)
        {
            int *curr_move ;
            for (int i = 0; i < board->move.nb_moves; i++)
            {                                                    // Pour chaque board fils potentiel
                int prec_played = board->pieceplayed;            // On sauvegarde le coup précédent avant d'ajouter un coup et de le supprimer
                boardPlay(board->move.possible_moves[i], board); // on joue le coup et on stock la piece jouée dans pieceplayed
                int *val=malloc(sizeof(int)*3);
                val[0] = -1000;
                curr_move = minMax(depth - 1, board, 0); // on evalue les fils du nouveau board à la profondeur -1
                printf("val[0] = %d\n",val[0]);
                printf("curr_move = %d\n",curr_move[0]);


                if (val[0] > curr_move[0])
                { // on garde la plus grande des values quand on maximise en changeant les pieces et cases si necessaire

                    curr_move[0] = val[0];

                }

                boardUndo(board);                 // on annule le coup precedement joué
                board->pieceplayed = prec_played; // on rerempli le champ du precedent avec la valeur précedement sauvagardée
            }
            return (curr_move);
        }
        else
        {

            int *curr_move = NULL;
            for (int i = 0; i < board->move.nb_moves; i++)
            {
                int prec_played = board->pieceplayed; // On sauvegarde le coup précédent avant d'ajouter un coup et de le supprimer
                boardPlay(board->move.possible_moves[i], board);
                int val[2];
                val[0] = 1000;
                curr_move = minMax(depth - 1, board, 1);

                if (val[0] < curr_move[0])
                {

                    curr_move[0] = val[0];
                }
                boardUndo(board);
                board->pieceplayed = prec_played; // on rerempli le champ du precedent avec la valeur précedement sauvagardée
            }
            return (curr_move);
        }
    }
} */


/*

void minMax(int depth, Board_state *board, int maximise, int * curr_move)
{

    if (isGameOver(*board) == 0 || depth == 0)
    {
        int previous_piece = board->pieceplayed;

        curr_move[0] = valuate(board, maximise);
        curr_move[1] = previous_piece;
        curr_move[2] = board->piece[previous_piece];

        
    }

    else
    {

        //   les cobinaisons de coups possibles format (piece,case)
        //   concrètement possible_moves est egal au carré du nombre de peces jouables


        if (maximise == 1)
        {
            for (int i = 0; i < board->move.nb_moves; i++)
            {                                                    // Pour chaque board fils potentiel
                //int prec_played = board->pieceplayed;            // On sauvegarde le coup précédent avant d'ajouter un coup et de le supprimer

                if (i<3){displayPossibleMoves(*board); }//printf("Yoyoyo [%d,%d] \n", board->move.possible_moves[0][0], board->move.possible_moves[0][1]);}

                boardPlay(board->move.possible_moves[i], board); // on joue le coup et on stock la piece jouée dans pieceplayed
                int *val=malloc(sizeof(int)*3);
                val[0] = -1000;
                //printf("précédent interne= %d\n", board->pieceplayed);

                minMax(depth - 1, board, 0, curr_move); // on evalue les fils du nouveau board à la profondeur depth-1
                //printf("val[0] = %d\n",val[0]);
                //printf("curr_move = %d\n",curr_move[0]);


                if (val[0] < curr_move[0])
                { // on garde la plus grande des values quand on maximise en changeant les pieces et cases si necessaire

                    val[0] = curr_move[0];
                    val[1] = curr_move[1];
                    val[2] = curr_move[2];
                }
                else{
                    curr_move[0] = val[0] ;
                    curr_move[1] =val[1] ;
                    curr_move[2] =val[2];

                }
                printf("coup joué est %d en position %d\n", board->pieceplayed, board->piece[board->pieceplayed]);
                printf("apporte une value = %d, %d\n", curr_move[0], val[0]);
                if (i<3){displayPossibleMoves(*board);}


                boardUndo(board, i);                 // on annule le coup precedement joué
                //printf("nombre de coups possibles : %d\n",board->move.nb_moves );
                if (i<3){displayPossibleMoves(*board);}


                //board->pieceplayed = prec_played; // on rerempli le champ du precedent avec la valeur précedement sauvagardée

            }
        }
        else
        {

            for (int i = 0; i < board->move.nb_moves; i++)
            {
                int prec_played = board->pieceplayed; // On sauvegarde le coup précédent avant d'ajouter un coup et de le supprimer
                boardPlay(board->move.possible_moves[i], board);
                int * val=malloc(sizeof(int)*2);
                val[0] = 1000;
                minMax(depth - 1, board, 1, curr_move);

                if (val[0] < curr_move[0])
                {

                    curr_move[0] = val[0];
                }
                boardUndo(board, i);
                board->pieceplayed = prec_played; // on rerempli le champ du precedent avec la valeur précedement sauvagardée
            }
        }

    }


}

*/




 
int * toPlayMinMax(int depth, Board_state *board, int maximise)
{


    //printf("piece à  jouer = %d\n", minMax(depth, *board, maximise)[1]);


    int *toPlay = minMax(depth, *board, maximise);
    int *toReturn = malloc(sizeof(int)*2);
    toReturn[0] = toPlay[1];
    toReturn[1] = toPlay[2];

    //printf("piece à  jouer = %d\n", toReturn[0]);
    //printf("cercle à  jouer = %d\n", toReturn[1]);

    free(toPlay);
    return toReturn;
}
 

/* 
void toPlayMinMax(int depth, Board_state *board, int maximise,  int * curr_move)
{

    minMax(depth, board, maximise, curr_move);

    
} */