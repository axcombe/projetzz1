#ifndef LOGIQUE_H
#define LOGIQUE_H

#include "structure.h"

// Fonction pour sélectionner une pièce
void selectedPiece(Board_state *board_state, int numpiece);

// Fonction pour placer une pièce sélectionnée sur un cercle
void selectedCircle(Board_state *board_state, int numcircle);

// Fonction pour initialiser l'état du jeu
void initGameLogique(Board_state *board_state);

// Fonction pour vérifier si la partie est terminée
int isGameOver(Board_state board_state);

//Fonction pour savoir si un coup est jouable ou pas
int coupPossible(int multijoueur, int *joueur, int *select, int playerIntention, int min_max[2], Board_state *board_state);
// Fonction pour mettre à jour l'état du jeu
void updateGameLogique(int multijoueur, int *joueur, int *select, int playerIntention, int min_max[2], Board_state *board_state, int *running);


#endif 
