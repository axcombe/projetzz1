#include <stdio.h>
#include <stdlib.h>
#include "logique.h"


void selectedPiece(Board_state * board_state,int numpiece){
    board_state->pieceselected=numpiece;
}
void selectedCircle(Board_state * board_state,int numcircle){
    board_state->piece[board_state->pieceselected]= numcircle;
    board_state->circle[numcircle] = board_state->pieceselected ;
    board_state->pieceplayed=board_state->pieceselected; 
    board_state->pieceselected = -1;
}



void initPossibleMoves(Board_state *board)
{

    int i = 0;
    int j = 0;

    int nb_coups_poss = 0;


    board->move.possible_moves = malloc(sizeof(int*)*256);

    for (int i=0;i<256;i++ ){
        board->move.possible_moves[i]=malloc(sizeof(int)*2);
    }

    for (i = 0; i < 16; i++)
    {
        for (j = 0; j < 16; j++)
        {
            if (board->piece[j] == -1 && board->circle[i] == -1)
            {

                board->move.possible_moves[nb_coups_poss][0] = j;
                board->move.possible_moves[nb_coups_poss][1] = i;
                nb_coups_poss += 1;
            }
        }
    }
    board->move.nb_moves = nb_coups_poss;

}




void initGameLogique (Board_state *board_state) {
    int tableau[16][4] = {
        {0, 0, 0, 0},
        {0, 0, 0, 1},
        {0, 0, 1, 0},
        {0, 0, 1, 1},
        {0, 1, 0, 0},
        {0, 1, 0, 1},
        {0, 1, 1, 0},
        {0, 1, 1, 1},
        {1, 0, 0, 0},
        {1, 0, 0, 1},
        {1, 0, 1, 0},
        {1, 0, 1, 1},
        {1, 1, 0, 0},
        {1, 1, 0, 1},
        {1, 1, 1, 0},
        {1, 1, 1, 1},
    };

    for (int i = 0; i < 16; i++) {
        board_state->pieces[i].num = i;
        board_state->pieces[i].form = tableau[i][0];
        board_state->pieces[i].empty = tableau[i][1];
        board_state->pieces[i].ribbon = tableau[i][2];
        board_state->pieces[i].color = tableau[i][3];
        board_state->piece[i] = -1;
        board_state->circle[i] = -1;
    }
    board_state->pieceselected = -1;
    board_state->pieceplayed = -1;
    board_state->modified = 0;
    board_state->num_coup = 0;


    initPossibleMoves(board_state);
}

int isGameOver(Board_state board_state) {



    int result = 1;

    int validline, validcol, forml, formc, ribbonl, ribbonc, emptyc, emptyl, colorl, colorc;

    for (int i = 0; i < 4; i++) {
        validline = 1;
        validcol = 1;
        forml = formc = ribbonl = ribbonc = emptyc = emptyl = colorl = colorc = 0;

        for (int j = 0; j < 4; j++) {
            // Vérification des lignes
            if (board_state.circle[i + 4 * j] == -1) {
                validline = 0;
            } else {
                forml += board_state.pieces[board_state.circle[i + 4 * j]].form;
                colorl += board_state.pieces[board_state.circle[i + 4 * j]].color;
                emptyl += board_state.pieces[board_state.circle[i + 4 * j]].empty;
                ribbonl += board_state.pieces[board_state.circle[i + 4 * j]].ribbon;
            }

            // Vérification des colonnes
            if (board_state.circle[4 * i + j] == -1) {
                validcol = 0;
            } else {
                formc += board_state.pieces[board_state.circle[4 * i + j]].form;
                colorc += board_state.pieces[board_state.circle[4 * i + j]].color;
                emptyc += board_state.pieces[board_state.circle[4 * i + j]].empty;
                ribbonc += board_state.pieces[board_state.circle[4 * i + j]].ribbon;
            }
        }

        // Vérification des conditions pour les lignes
        if (validline == 1 && (forml == 0 || forml == 4 || colorl == 0 || colorl == 4 || emptyl == 0 || emptyl == 4 || ribbonl == 0 || ribbonl == 4)) {
            result = 0;
        }

        // Vérification des conditions pour les colonnes
        if (validcol == 1 && (formc == 0 || formc == 4 || colorc == 0 || colorc == 4 || emptyc == 0 || emptyc == 4 || ribbonc == 0 || ribbonc == 4)) {
            result = 0;
        }
    }

    int form, color, empty, ribbon;

    // Vérification de la première diagonale
    if (board_state.circle[0] != -1 && board_state.circle[5] != -1 && board_state.circle[10] != -1 && board_state.circle[15] != -1) {
        form = board_state.pieces[board_state.circle[0]].form + board_state.pieces[board_state.circle[5]].form + board_state.pieces[board_state.circle[10]].form + board_state.pieces[board_state.circle[15]].form;
        color = board_state.pieces[board_state.circle[0]].color + board_state.pieces[board_state.circle[5]].color + board_state.pieces[board_state.circle[10]].color + board_state.pieces[board_state.circle[15]].color;
        empty = board_state.pieces[board_state.circle[0]].empty + board_state.pieces[board_state.circle[5]].empty + board_state.pieces[board_state.circle[10]].empty + board_state.pieces[board_state.circle[15]].empty;
        ribbon = board_state.pieces[board_state.circle[0]].ribbon + board_state.pieces[board_state.circle[5]].ribbon + board_state.pieces[board_state.circle[10]].ribbon + board_state.pieces[board_state.circle[15]].ribbon;
        if (form == 0 || form == 4 || color == 0 || color == 4 || empty == 0 || empty == 4 || ribbon == 0 || ribbon == 4) {
            result = 0;
        }
    }

    // Vérification de la deuxième diagonale
    if (board_state.circle[3] != -1 && board_state.circle[6] != -1 && board_state.circle[9] != -1 && board_state.circle[12] != -1) {
        form = board_state.pieces[board_state.circle[3]].form + board_state.pieces[board_state.circle[6]].form + board_state.pieces[board_state.circle[9]].form + board_state.pieces[board_state.circle[12]].form;
        color = board_state.pieces[board_state.circle[3]].color + board_state.pieces[board_state.circle[6]].color + board_state.pieces[board_state.circle[9]].color + board_state.pieces[board_state.circle[12]].color;
        empty = board_state.pieces[board_state.circle[3]].empty + board_state.pieces[board_state.circle[6]].empty + board_state.pieces[board_state.circle[9]].empty + board_state.pieces[board_state.circle[12]].empty;
        ribbon = board_state.pieces[board_state.circle[3]].ribbon + board_state.pieces[board_state.circle[6]].ribbon + board_state.pieces[board_state.circle[9]].ribbon + board_state.pieces[board_state.circle[12]].ribbon;
        if (form == 0 || form == 4 || color == 0 || color == 4 || empty == 0 || empty == 4 || ribbon == 0 || ribbon == 4) {
            result = 0;
        }
    }

    if (result == 1 && board_state.num_coup == 16){
        result = 2;
    
    }
    return result;
}








int coupPossible(int multijoueur, int *joueur, int *select, int playerIntention, int min_max[2], Board_state *board_state) {
    int result = 1;
    //printf("result = %d\n", result);
    if (multijoueur == 0) {
        //printf("passe ici\n");
        if (*joueur == 1) {
            //printf("output minmax = %d\n", min_max[0]);
            //printf("piece correspondante = %d\n",board_state->piece[min_max[0]] );

            if (board_state->piece[min_max[0]] != -1 || board_state->circle[min_max[1]] != -1) {
                result = 0;
            }
        } else {
            if (*select == 0) {
                if (board_state->piece[playerIntention] != -1) {
                    result = 0;
                }
            } else {
                if (board_state->circle[playerIntention] != -1) {
                    result = 0;
                }
            }
        }
    } else {
        // Logique pour le mode multijoueur
        if (*select == 0) {
            if (board_state->piece[playerIntention] != -1) {
                result = 0;
            }
        } else {
            if (board_state->circle[playerIntention] != -1) {
                result = 0;
            }
        }
    }
    return result;
}




void updateGameLogique(int multijoueur, int *joueur, int *select, int playerIntention, int min_max[2], Board_state *board_state, int *running) {
    int possible=0;
    //printf("player intention = %d",playerIntention);
    if (playerIntention!=-1){
        possible=coupPossible(multijoueur, joueur, select, playerIntention, min_max, board_state);
    }
    //printf("possible ? = %d\n", possible );
    if (possible==1){
        if (multijoueur == 0) {

            if (*joueur == 1) {
                board_state->num_coup += 1;
                selectedPiece(board_state, min_max[0]);
                selectedCircle(board_state, min_max[1]);
                *running = isGameOver(*board_state);
                if (*running == 1) {
                    *joueur = 1 - *joueur;
                }
                board_state->modified = 1;
                
            } else {
                if (playerIntention >= 0) {
                    if (*select == 0) {
                        selectedPiece(board_state, playerIntention);
                        *select = 1;
                        board_state->modified = 0;
                    } else {
                        board_state->num_coup += 1;
                        selectedCircle(board_state, playerIntention);
                        *select = 0;
                        *running = isGameOver(*board_state);
                        if (*running == 1) {
                            *joueur = 1 - *joueur;
                        }
                        board_state->modified = 1;
                    }
                    
                } else {
                    board_state->modified = 0;
                }
            }
        } else {
            if (playerIntention != -1) {
                if (*select == 0) {
                    selectedPiece(board_state, playerIntention);
                    *select = 1;
                    board_state->modified = 0;
                } else {
                    board_state->num_coup += 1;
                    selectedCircle(board_state, playerIntention);
                    *select = 0;
                    *running = isGameOver(*board_state);
                    if (*running == 1) {
                        *joueur = 1 - *joueur;
                    }
                    board_state->modified = 1;
                }
                
            } else {
                board_state->modified = 0;
            }
        }
    }
    else{
        board_state->modified = 0;
    }
}