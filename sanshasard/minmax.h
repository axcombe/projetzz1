#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "structure.h"
#include "logique.h"


void displayPossibleMoves(Board_state board);


void connexity(Board_state *board, int *connexity);

int valuate(Board_state *board, int maximise);

void supprPossibleMoves(Board_state * board);

void addPossibleMoves(Board_state *board);

void boardPlay(int *datas, Board_state *board);

void boardUndo(Board_state *board);

//int * minMax(int depth, Board_state *board, int maximise);

//void minMax(int depth, Board_state *board, int maximise, int * curr_move);

// int * toPlayMinMax(int depth, Board_state *board, int maximise);

int * toPlayMinMax(int depth, Board_state *board, int maximise);