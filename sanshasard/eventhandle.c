#include <stdio.h>
#include <SDL2/SDL.h>

#include "eventhandle.h"

int pollEvent(SDL_Window * window,int select, Zone_SDL zone, int *running) {
    SDL_Event event;

    while (SDL_PollEvent(&event)) {
        switch (event.type) {
            case SDL_QUIT:
                *running = 3;  // Quitter le programme si on clique sur la croix de la fenêtre
                break;
            case SDL_MOUSEBUTTONDOWN:
                if (event.button.button == SDL_BUTTON_LEFT) {
                    int mouseX = event.button.x;
                    int mouseY = event.button.y;

                    if (select == 0) {
                        //a trouver par rapport a zone.rect avec un seul rectangle ca donnerai
                        int x = zone.rect->x;
                        int y = zone.rect->y;
                        int w = zone.rect->w; 
                        int h = zone.rect->h;

                        for (int i = 0; i < 8; i++) {
                            for (int j = 0; j < 2; j++) {
                                int rectX = x + j * w;
                                int rectY = y + i * h;
                                if (mouseX > rectX && mouseX < rectX + w &&
                                    mouseY > rectY && mouseY < rectY + h) {
                                    return 2 * i + j;  // Retourne l'indice du rectangle sélectionné
                                }
                            }
                        }
                        return -1;  // Aucun rectangle sélectionné
                    } else if (select == 1) {
                        for (int i = 0; i < 16; i++) {
                            int Mx = zone.box[i].Mx;  // Centre du cercle
                            int My = zone.box[i].My;
                            int R = zone.box[i].radius;  //
                            if ((mouseX - Mx) * (mouseX - Mx) + (mouseY - My) * (mouseY - My) < R * R) {
                                return i;  // Retourne l'indice du cercle sélectionné
                            }
                        }
                        return -1;  // Aucun cercle sélectionné
                    }
                }
                break;
            default:
                break;
        }
        if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_RESIZED) {
            int w, h;
            SDL_GetWindowSize(window, &w, &h);
            int minWidth = (int)(1.45 * h);
            if (w < minWidth) {
                SDL_SetWindowSize(window, minWidth, h);
            }
        }
    }
    return -1;  // Aucun événement à traiter
}