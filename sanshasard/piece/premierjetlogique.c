#include <stdio.h>

#include "structure.h"

void piece(Board_state * board_state,int numpiece){
    (*board_state)->pieceselected=numpiece;
}
void circle(Board_state * board_state,int numcircle){
    board_state->piece[board_state->pieceselected]= numcircle;
    board_state->circle[numcircle] = board_state->pieceselected ;
    board_state->pieceplayed=board_state->pieceselected; 
    board_state->pieceselected = -1;
}

void fin(Board_state * board_state){
    int result=1;
    int validline,validcol,forml,formc,ribbonl,ribbonc,emptyc,emptyl,colorl,colorc;
    for (int i = 0 ; i<4 ; i++){
        for (int j = 0; j<4 ; j++){
            if (circle[i+ 4*j] ==-1){
                validline=0;
            }
            else{
                forml += pieces[circle[i+4*j]]->form;
                colorl += pieces[circle[i+4*j]]->color;
                emptyl += pieces[circle[i+4*j]]->empty;
                ribbonl += pieces[circle[i+4*j]]->ribbon;
            }
            if (circle[4*i + j] ==-1){
                validcol=0;
            }
            else{
                formc += pieces[circle[4*i + j]]->form;
                colorc += pieces[circle[4*i + j]]->color;
                emptyc += pieces[circle[4*i + j]]->empty;
                ribbonc += pieces[circle[4*i + j]]->ribbon;
            }
        }
        if (validline=1 && (forml==0 ||forml = 4 || colorl == 0||colorl==4||emptyl==0||emptyl==4||ribbonl == 0||ribbonl==4)){
            result=0;
        }
        if (validcol=1 && (formc==0 ||formc = 4 || colorc == 0||colorc==4||emptyc==0||emptyc==4||ribbonc == 0||ribbonc==4)){
            result=0;
        }
    }
    int form,color,empty,ribbon;
    if (circle[0] !=-1||circle[5]!=-1||circle[10]!=-1||circle[15]!=-1){
        form = pieces[0]->form + pieces[5]->form + pieces[10]->form + pieces[15]->form;
        color = pieces[0]->color + pieces[5]->color + pieces[10]->color + pieces[15]->color;
        empty = pieces[0]->empty + pieces[5]->empty + pieces[10]->empty + pieces[15]->empty;
        ribbon =pieces[0]->ribbon + pieces[5]->ribbon + pieces[10]->ribbon + pieces[15]->ribbon;
        if (form==0 ||form = 4 || color == 0||color==4||empty==0||empty==4||ribbon == 0||ribbon==4){
            result=0;
        }
    }
    if (circle[3] !=-1||circle[6]!=-1||circle[9]!=-1||circle[12]!=-1){
            form = pieces[3]->form + pieces[6]->form + pieces[9]->form + pieces[12]->form;
            color = pieces[3]->color + pieces[6]->color + pieces[9]->color + pieces[12]->color;
            empty = pieces[3]->empty + pieces[6]->empty + pieces[9]->empty + pieces[12]->empty;
            ribbon =pieces[3]->ribbon + pieces[6]->ribbon + pieces[9]->ribbon + pieces[12]->ribbon;
            if (form==0 ||form = 4 || color == 0||color==4||empty==0||empty==4||ribbon == 0||ribbon==4){
                result=0;
            }
    }
}



void mainlogique(int * joueur,int * select,int playerIntention,int min_max[2],Board_state * board_state,int * running ){
    if (joueur==1){
        piece(board_state,min_max[0]);
        circle(board_state,min_max[1]);
        *running=fin(board_state);
        if (*running ==1){
            (*joueur)=1-(*joueur);
        }
        state_board->modified=1;
        state_board->num_coup += 1;
    }
    else{
        if (playerIntention!=-1){
            if (select==0){
                piece(board_state,playerIntention);
                *select = 1-(*select);
                state_board->modified = 0;
            }
            else{
                circle(board_state,playerIntention);
                *select = 1 -(*select);
                *running=fin(board_state)
                if (*running == 1){
                    (*joueur)=1-(*joueur);
                }
                state_board->modified = 1;
            }
            state_board->num_coup += 1;
            
        }
        else{
            state_board->modified=0;
        }
    }
}


void afficher(int tableau[16]) {
    printf("[");
    for (int i = 0; i < 16; i++) {
        printf(" %d,", tableau[i]);
    }
    printf("]\n");
}


void afficher2(PieceLo tableau[16]) {
    printf("[");
    for (int i = 0; i < 16; i++) {
        printf(" num=%d, form=%d, empty=%d, ribbon=%d, color=%d", tableau[i].num, tableau[i].form, tableau[i].empty, tableau[i].ribbon, tableau[i].color);
    }
    printf("]\n");
}

int main() {
    Board_state board_state ;
    init_logique(&board_state);
    printf("Initialisation du tableau:\n");
    afficher(board_state.piece);
    afficher(board_state.circle);
    piece(&board_state,15);
    circle(&board_state,0);
    printf("%d nombre de coup: %d\n",fin(board_state),board_state.num_coup);
    piece(&board_state,11);
    circle(&board_state,1);
    printf("%d\n",fin(board_state));
    piece(&board_state,0);
    circle(&board_state,9);
    printf("%d\n",fin(board_state));
    piece(&board_state,7);
    circle(&board_state,13);
    printf("%d\n",fin(board_state));
    afficher(board_state.piece);
    afficher(board_state.circle);

    return 0;
}
