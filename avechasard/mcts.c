#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


#include "structure.h"
#include "logicingame.h"
#include "mcts.h"


#define C sqrt(2)
#define SCORE_MERMAID 2

//============hachage==============

int compare_cards(const void *a, const void *b) { // Comparaison entre 2 Card par rapport au numsprite, pour le qsort
    const Card *cardA = (const Card *)a;
    const Card *cardB = (const Card *)b;
    return cardA->numsprite - cardB->numsprite;
}

void sort_cardstack(CardStack *cs) { // Sort d'un CardStack
    qsort(cs->stack, cs->size, sizeof(Card), compare_cards);
}

void copy_cardstack(CardStack *dest, CardStack *src) { // Copie d'une CardStack
    dest->size = src->size;
    for (int i = 0; i < src->size; i++) {
        dest->stack[i] = src->stack[i];
    }
}

unsigned long hash_card(Card *card) { // Hash de Card
    unsigned long hash = 5381;
    hash = ((hash << 5) + hash) + card->color;
    hash = ((hash << 5) + hash) + card->type;
    hash = ((hash << 5) + hash) + card->numsprite;
    return hash;
}

unsigned long hash_cardstack(CardStack *cs) { // Hash de CardStack
    CardStack copy;
    copy_cardstack(&copy, cs);
    sort_cardstack(&copy);

    unsigned long hash = 5381;
    for (int i = 0; i < copy.size; i++) {
        hash = ((hash << 5) + hash) + hash_card(&copy.stack[i]);
    }
    return hash;
}

unsigned long hash_node(Node *node) { // Hash de Node
    unsigned long hash = 5381;
    while (node != NULL) {
        hash = ((hash << 5) + hash) + node->index;
        node = node->next;
    }
    return hash;
}

unsigned long hash_hand(Hand *hand) { // Hash de Hand
    unsigned long hash = 5381;
    hash = ((hash << 5) + hash) + hash_cardstack(&hand->content);
    // for (int i = 0; i < 14; i++) {
    //     hash = ((hash << 5) + hash) + hash_node(hand->quickAccess[i].next);
    // }
    // for (int i = 0; i < 11; i++) {
    //     hash = ((hash << 5) + hash) + hand->nbcardpercolor[i];
    // }
    return hash;
}

unsigned long hash_player(Player *p) { // Hash de Player
    unsigned long hash = 5381;
    hash = ((hash << 5) + hash) + p->globalscore;
    hash = ((hash << 5) + hash) + p->roundscore;
    hash = ((hash << 5) + hash) + p->precroundscore;
    hash = ((hash << 5) + hash) + hash_hand(&p->hand);
    hash = ((hash << 5) + hash) + hash_cardstack(&p->pair);
    return hash;
}

unsigned long hash_view(View *view) { // Hash de View (Principal)
    unsigned long hash = 5381;
    hash = ((hash << 5) + hash) + hash_player(&view->player1);
    hash = ((hash << 5) + hash) + hash_cardstack(&view->pair1);
    hash = ((hash << 5) + hash) + view->sizehand1;
    hash = ((hash << 5) + hash) + view->sizedeck;
    hash = ((hash << 5) + hash) + hash_cardstack(&view->discard1);
    hash = ((hash << 5) + hash) + hash_cardstack(&view->discard2);
    return hash;
}


//     unsigned long hash = 5381;
//     hash = ((hash << 5) + hash) + hash_board(&state->board);
//     hash = ((hash << 5) + hash) + hash_player(&state->player1);
//     hash = ((hash << 5) + hash) + hash_player(&state->player2);
//     return hash;
// }

/*

unsigned long hash_cardstack(CardStack *stack);
// Hash function for Player (ignoring pair and hand.quickAccess/nbcardpercolor)
unsigned long hash_player(Player *player) {
    unsigned long hash_value = 17; // Initial seed

    // Hashing globalscore
    hash_value = hash_value * 31 + player->globalscore;

    // Hashing roundscore
    hash_value = hash_value * 31 + player->roundscore;

    // Hashing precroundscore
    hash_value = hash_value * 31 + player->precroundscore;

    // Hashing hand (ignoring quickAccess and nbcardpercolor)
    hash_value = hash_value * 31 + hash_cardstack(&player->hand.content);
    hash_value = hash_value * 31 + hash_cardstack(&player->pair);

    return hash_value;
}

// Hash function for CardStack (assuming it's defined elsewhere)
unsigned long hash_cardstack(CardStack *stack) {
    CardStack copy;
    copy_cardstack(&copy, stack);
    sort_cardstack(&copy);
    unsigned long hash_value = 17; // Initial seed

    // Hashing each card in the stack
    for (int i = 0; i < stack->size; ++i) {
        hash_value = hash_value * 31 + copy.stack[i].color;
        hash_value = hash_value * 31 + copy.stack[i].type;
        hash_value = hash_value * 31 + copy.stack[i].numsprite;
    }

    // Hashing size
    hash_value = hash_value * 31 + stack->size;

    return hash_value;
}
// Hash function for View (using hash_player and hashing relevant fields)
unsigned long hash_view(View *view) {
    unsigned long hash_value = 17; // Initial seed

    // Hashing player1
    hash_value = hash_value * 31 + hash_player(&view->player1);

    // Hashing pair1 (assuming it's a CardStack)
    hash_value = hash_value * 31 + hash_cardstack(&view->pair1);

    // Hashing sizehand1
    hash_value = hash_value * 31 + view->sizehand1;

    // Hashing sizedeck
    hash_value = hash_value * 31 + view->sizedeck;

    // Hashing discard1
    hash_value = hash_value * 31 + hash_cardstack(&view->discard1);

    // Hashing discard2
    hash_value = hash_value * 31 + hash_cardstack(&view->discard2);

    return hash_value;
}*/

// void getView(State *state, View * view);
//==============arbreRN==============
// Create a red-black tree
Tree *createTree(MCTSNode *node) {
    // printf("Création d'un arbre\n");
    Tree * tree = malloc(sizeof(Tree)); 
    tree->color = TRED;

    tree->link[0] = NULL;
    tree->link[1] = NULL;

    tree->node = node;


    View *view = malloc(sizeof(View));
    getView(&node->state,view);

    tree->hash = hash_view(view);

    free(view);

    return tree;
}

void rbRotateRight(Tree ** root, Tree ** y, Tree ** papa, int son) {
    if (y != NULL && *y != NULL) {
        Tree * temp = (*y)->link[0];
        if (temp != NULL) {
            (*y)->link[0] = temp->link[1];
            temp->link[1] = (*y);
            if (*y != *papa) {
                (*papa)->link[son] = temp;
            } else {
                *papa = temp; // Mise à jour si y est la racine
            }
            if (*root == *y) {
                *root = temp; // Mise à jour de la racine
            }
            // printf("Right rotation at node %lu\n", (*y)->hash);
        }
    }
}

void rbRotateLeft(Tree ** root, Tree ** y, Tree ** papa, int son) {
    if (y != NULL && *y != NULL) {
        Tree * temp = (*y)->link[1];
        if (temp != NULL) {
            (*y)->link[1] = temp->link[0];
            temp->link[0] = (*y);
            if (*y != *papa) {
                (*papa)->link[son] = temp;
            } else {
                *papa = temp; // Mise à jour si y est la racine
            }
            if (*root == *y) {
                *root = temp; // Mise à jour de la racine
            }
            // printf("Left rotation at node %lu\n", (*y)->hash);
        }
    }
}

void rbRotate(Tree ** root, Tree ** x, Tree ** papa, int right, int son) {
    if (right) {
        rbRotateRight(root, x, papa, son);
    } else {
        rbRotateLeft(root, x, papa, son);
    }
}

// void treeDisplay(Tree *root, int depth);

// Insert un tree et renvoie le tree ajouté 
Tree * insertTree(Tree ** root, MCTSNode *node) {
    // printf("Insertion d'un nouveau noeud\n");
    Tree *stack[100], *ptr, *newnode;
    int dir[100], ht = 0, index=0;
    ptr = *root;

    if (*root == NULL) {
        *root = createTree(node);
        (*root)->color = TBLACK;
        // printf("Création de la racine avec hash %lu\n", (*root)->hash);
        return *root;
    }

    View *view = malloc(sizeof(View));
    getView(&node->state, view);

    long unsigned hash = hash_view(view);
    free(view);

    stack[ht] = *root;
    dir[ht++] = 0;
    while (ptr != NULL) {
        index = (hash - ptr->hash) > 0 ? 1 : 0;
        stack[ht] = ptr;
        ptr = ptr->link[index];
        dir[ht++] = index;
    }

    stack[ht - 1]->link[index] = newnode = createTree(node);
    newnode->color = TRED;
    // printf("Insertion du noeud avec hash %lu\n", newnode->hash);

    while ((ht >= 3) && (stack[ht - 1]->color == TRED)) {
        if (stack[ht-2]->link[1-dir[ht-2]] != NULL && stack[ht-2]->link[1-dir[ht-2]]->color == TRED) {
            stack[ht-2]->color = TRED;
            stack[ht-2]->link[1-dir[ht-2]]->color = TBLACK;
            stack[ht-1]->color = TBLACK;
            ht = ht - 1;
            // printf("Rééquilibrage (cas 4) à hauteur %d\n", ht);
        } else {
            if (dir[ht-1] != dir[ht-2]) {
                // printf("Rééquilibrage (cas 5) à hauteur %d\n", ht);
                rbRotate(root, &(stack[ht-1]), &(stack[ht-2]), dir[ht-2], dir[ht-2]);
                Tree *temp = stack[ht-1];
                stack[ht-1] = newnode;
                newnode = temp;
                dir[ht-1] = dir[ht-2];
            }
            // printf("Rééquilibrage (cas 6) à hauteur %d\n", ht);
            rbRotate(root, &(stack[ht-2]), &(stack[ht-3]), 1-dir[ht-2], dir[ht-3]);
            stack[ht-1]->color = TBLACK;
            stack[ht-2]->color = TRED;
            if (stack[ht-3] == stack[ht-2]) {
                stack[ht-3] = stack[ht-1];
                *root = stack[ht-3];
            }
            ht = ht - 1;
        }
    }

    (*root)->color = TBLACK;
    // treeDisplay(*root, 0); // Ajoutez cette ligne pour imprimer l'état de l'arbre après chaque insertion
    return newnode;
}

// void treeDisplay(Tree * root){
//     if (NULL != root){
//         printf ("[%d(%d) (",root->node->number_move,root->color);
//         treeDisplay(root->link[0]);
//         printf (") (");
//         treeDisplay(root->link[1]);
//         printf(") ]");
//     }
// }

void treeDisplay(Tree *root, int depth) {
    if (root == NULL) {
        printf("%*s() \n", depth * 4, "");
        return;
    }
    printf("%*s[%lu(%d)]\n", depth * 4, "", root->hash, root->color);
    treeDisplay(root->link[0], depth + 1);
    treeDisplay(root->link[1], depth + 1);
}


void treeFree(Tree * root) {
    if (root){
        free(root->node);
    }
    if (root->link[0]){
        // printf("le fils existe1\n");
        treeFree(root->link[0]);
    }
    if (root->link[1]){
        // printf("le fils existe2\n");
        treeFree(root->link[1]);
    }

    free(root);
}

// prends un noeud et le numéro du coup i, et incrémente le nombre de fois qu'un coup a été joué
void incrMove(MCTSNode *node,int i){
    node->total_number+=1;
    node->moves[i].number+=1;
}

// donne le coups le plus interessant avec l'algo UCB et l'incrémente
Move *ucb(MCTSNode * node){
    int coef = 1;
    if (node->state.current_player==1){
        coef = -1;
    }
    int i,interet,indicemax=0,max = coef * (node->moves[0].gain/node->moves[0].number) + C * sqrt(log2(node->total_number)/node->moves[0].number);
    // int debug=0;
    // printf("NOMBRE COUPS : %d\n",node->number_move);
    for (i=0;node->number_move>i;i++){
        // printf("i : %d\n",i);
        interet = coef * (node->moves[i].gain/node->moves[i].number) + C * sqrt(log2(node->total_number)/node->moves[i].number);
        if (max<=interet){
            indicemax=i;
            max=interet;
            // debug = 1;
        }
    }
    
    if (indicemax>=7){
        printf("indicemax : %d\n",indicemax);
    }
    incrMove(node,indicemax);
    return &node->moves[indicemax];
}

void getMoves(State *state, MCTSNode *node){
    // printf("ON RECUP LES MOVES\n");
    node->number_move=4;
    int i;
    for (i=0;i<4;++i){
        node->moves[i].gain=0;
        node->moves[i].number=0;
        node->moves[i].intention=i;
    }
    i=4;
    int offset=0;
    if (state->board.discard1.size!=0){
        // printf("DISCARD 1 PAS VIDE\n");
        node->number_move+=1;
        node->moves[i].gain=0;
        node->moves[i].number=0;
        node->moves[i].intention=i;
        i+=1;
    }
    else {
        offset+=1;
    }
    if (state->board.discard2.size!=0){
        // printf("DISCARD 2 PAS VIDE\n");
        node->number_move+=1;
        node->moves[i].gain=0;
        node->moves[i].number=0;
        node->moves[i].intention=i+offset;
        i+=1;
    }
    else{
        offset+=1;
    }
    if (state->player2.roundscore >=7){
        node->number_move+=1;
        node->moves[i].gain=0;
        node->moves[i].number=0;
        node->moves[i].intention=i+offset;
        i+=1;
    }
}

MCTSNode *createMCTSNode(State *state){
    MCTSNode *node = malloc(sizeof(MCTSNode));
    node->state = *state;
    node->total_number = 0;
    getMoves(state,node);
    return node;
};

MCTSNode * getNode(State *state, Tree **course, Tree **root, int *created){ // cherche dans l'arbre un noeud associé au state, retourne le noeud s'il existe, NULL sinon
    // printf("pointeur du root dans getNode : %p\n",root);
    View *view = malloc(sizeof(View));
    getView(state,view);

    unsigned long hash = hash_view(view);
    // printf("HASH : %ld\n",hash);
    free(view);

    if (*course){
        View *viewnode = malloc(sizeof(View));
        getView(&(*course)->node->state,viewnode);

        unsigned long hashnode = hash_view(viewnode);

        free(viewnode);

        // printf("HASH NODE : %ld, HASH ROOT : %ld\n",hashnode,hash);
        // printf("RACINE : \n");
        for (int i=0; i< (*course)->node->number_move;++i)
            // printf("gain : %d, number %d\n",(*course)->node->moves[i].gain,(*course)->node->moves[i].number);
        if (hashnode == hash){
            *created=0;
            return (*course)->node;
        }
        else if (hashnode > hash){
            return getNode(state,&(*course)->link[0], root,created);
        }
        else if (hashnode < hash){
            return getNode(state,&(*course)->link[1], root,created);
        }
    }
    else{
        *created = 1;
        // printf("pre insertTree\n");
        return insertTree(root,createMCTSNode(state))->node;
    }
}

void botIntentionToPlayerIntention(int bot_intention,int player_intention[3]){

    switch (bot_intention)
    {
        case END:
            player_intention[0]=FIN;
            player_intention[1]=NONE;
            player_intention[2]=NONE;
            break;
        case DISCARD_0:
            player_intention[0]=DISCARD1;
            player_intention[1]=NONE;
            player_intention[2]=NONE;
            break;
        case DISCARD_1:
            player_intention[0]=DISCARD2;
            player_intention[1]=NONE;
            player_intention[2]=NONE;
            break;
        case DISCARD_0_0:
            player_intention[0]=DECK;
            player_intention[1]=SELECTCARD1;
            player_intention[2]=SELECTDISCARD1;
            break;
        case DISCARD_0_1:
            player_intention[0]=DECK;
            player_intention[1]=SELECTCARD1;
            player_intention[2]=SELECTDISCARD2;
            break;
        case DISCARD_1_0:
            player_intention[0]=DECK;
            player_intention[1]=SELECTCARD2;
            player_intention[2]=SELECTDISCARD1;
            break;
        case DISCARD_1_1:
            player_intention[0]=DECK;
            player_intention[1]=SELECTCARD2;
            player_intention[2]=SELECTDISCARD2;
            break;
        default:
            break;
    }
    
}

void getView(State *state, View * view){
    // printf("GETVIEW :\n");
    // printPlayer(&state->player1);
    view->player1 = state->player1;
    view->pair1 = state->player2.pair;
    view->sizehand1 = state->player2.hand.content.size;
    view->discard1 = state->board.discard1;
    view->discard2 = state->board.discard2;
    view->sizedeck = state->board.deck.size;
}

int eval(State *state){// evalue un state, avec player1 le bot
    if (state->player1.hand.nbcardpercolor[10]<4){
        return state->player1.roundscore - state->player2.roundscore + SCORE_MERMAID * state->player1.hand.nbcardpercolor[10];
    }
    return 102+C*4;
}

void displayMove(Move * move){
    printf("intention : %d\n",move->intention);
}
void displayMoves(MCTSNode *node){
    for (int i=0;i<node->number_move;++i){
        displayMove(&node->moves[i]);
    }
}

int simulation(State state, int initial_player_intention[3]){   // simule le jeu a partir d'un état en jouant un coup inital, puis en choisissant un coup au hasard pour les autres
    int finish=0;
    int currentplayer=state.current_player;
    int first_loop=1;
    // printf("INITIAL SIZE DECK : %d\n",state.board.deck.size);
    int random_move;
    int player_intention[3];
    srand(time(NULL));
    while (finish == 0){
        
        // printf("SIZE DECK : %d\n",state.board.deck.size);
        if (first_loop){
            // printf("INITIAL PLAYER INTENTION : [%d,%d,%d]\n",initial_player_intention[0],initial_player_intention[1],initial_player_intention[2]);
            playMove((&state),&state.current_player,initial_player_intention,&finish);
            first_loop=0;
            // printf("FINISH : %d\n",finish);
        }
        else{
            // printf("JE LANCE UNE FOIS GET MOVES\n");
            MCTSNode *node = createMCTSNode((&state));
            // getMoves((&state),node);

            // displayMoves(node);
            random_move = rand()%node->number_move;
            // printf("RANDOM : %d\n",random_move);
            // printf("INTENTION : %d\n",node->moves[random_move].intention);
            botIntentionToPlayerIntention(node->moves[random_move].intention,player_intention);
            // printf("PLAYER INTENTION : [%d,%d,%d]\n",player_intention[0],player_intention[1],player_intention[2]);



            playMove((&state),&(&state)->current_player,player_intention,&finish);
            // printPlayer(&state.player1);
            // free(node);
        }

    }

    // free_quickAccess(state.player1.hand.quickAccess);
    // free_quickAccess(state.player2.hand.quickAccess);

    return eval((&state));

}

void unique_play(State *state,int player_intention[3]){  // joue 1 coup
    int finish=0;
    playMove(state,&state->current_player,player_intention,&finish);

}

int isFinished(State *state, int player_intention[3]){
    if (player_intention[0]==END){  // on appuye sur END
        return 1;
    }
    if (state->board.deck.size==0){ // plus de deck
        return 1;
    }
    if (state->player1.hand.nbcardpercolor[10]==4){ // 4 sirènes
        return 1;
    }
    return 0; // la partie n'est pas finie
}

int cardMakePair(CardStack *card_stack,Card *card){
    // type<=1 : pair type et 1-type, else if type > 1 et type <= 4 : type et type esle if type <4 : pas de pair
    int res = 0;
    for (int i=0; i<card_stack->size;i++){
        if (card->type<=1){
            if (card_stack->stack[i].type == 1-card->type){
                res = 1;
            }
        }
        else if(card->type > 1 && card->type <= 4){
            if (card_stack->stack[i].type == card->type){
                res = 1;
            }
        } 
    }

    return res;

}

void createRandomState(State *state,State *random_state){  // penser a créer quickAccess et nbcardpercolor a partir de la nouvelle content du joueur 2 

    random_state->player1 = state->player1; // on le connais
    random_state->current_player = state->current_player; // on le connais
    random_state->player2 = state->player2; // on va le modifier
    random_state->board = state->board; // on va le modifier

    int size_deck, size_hand1;
    size_deck = state->board.deck.size;
    size_hand1 = state->player2.hand.content.size;
    // Card *toBeShuffle = malloc(sizeof(Card) * (size_deck+size_hand1));
    // CardStack toBeShuffle;

    
    int i,indicecardmakepair=0;


    for (i=0;i<size_hand1;++i){
        add_one_card(&random_state->player2.hand.content,&random_state->board.deck);
    }

    shuffle(&random_state->board.deck);

    for (i=0;i<size_hand1;++i){
        size_deck = state->board.deck.size;
        while (cardMakePair(&random_state->player2.hand.content,&random_state->board.deck.stack[size_deck - 1])){ // si la carte qu'on est sur le point d'ajouter va faire une pair
            Card temp = random_state->board.deck.stack[size_deck-1];
            random_state->board.deck.stack[size_deck-1] = random_state->board.deck.stack[indicecardmakepair];
            random_state->board.deck.stack[indicecardmakepair]=temp;
            indicecardmakepair++;
        }
        
        add_one_card(&random_state->board.deck,&random_state->player2.hand.content);
        oneMoreCardInQuickAccess(&random_state->player2.hand);
        colorManagement(&random_state->player2.hand);

    }

}

// Définition de la fonction copyState
void copyState(State *state_initial, State *state_copy) {

    // Copie de chaque champ de state_initial vers state_copy
    
    // Copie de board
    memcpy(&state_copy->board, &state_initial->board, sizeof(Board));
    
    // Copie de player1
    memcpy(&state_copy->player1, &state_initial->player1, sizeof(Player));
    
    // Copie de player2
    memcpy(&state_copy->player2, &state_initial->player2, sizeof(Player));
    
    // Copie de current_player
    state_copy->current_player = state_initial->current_player;
    
    // Par exemple, si vous avez d'autres champs dans State, continuez à les copier de la même manière.
}

void copyMCTSNode(MCTSNode *node_initial, MCTSNode *node_copy) {
    // Copie de chaque champ de node_initial vers node_copy
    
    // Copie de moves
    memcpy(node_copy->moves, node_initial->moves, sizeof(Move) * 7);
    
    // Copie de number_move
    node_copy->number_move = node_initial->number_move;
    
    // Copie de total_number
    node_copy->total_number = node_initial->total_number;
    
    // Copie de state
    memcpy(&node_copy->state, &node_initial->state, sizeof(State));
    
    // Par exemple, si vous avez d'autres champs dans MCTSNode, continuez à les copier de la même manière.
}
// faire fonction qui doit crée un state aléatoire en gardant les info connues (view) State *state createRandomState(State *state);
int mcts(State *state,Tree **root){
    // printf("pointeur du root dans mcts : %p\n",root);
    int result[3];
    int created;
    MCTSNode *node = getNode(state,root,root,&created);
    // treeDisplay(*root,0);

    // printf("AVANT MCTS :\n");
    // for (int i=0; i< node->number_move;++i)
    //         printf("gain : %d, number %d\n",node->moves[i].gain,node->moves[i].number);
    

    if (created == 0){ // si ce noeud existe deja
        Move *move;
        int bot_intention,player_intention[3],terminal_node=0;

        // on choisi le coup à jouer
        if (node->total_number < node->number_move){    // si on a pas fait tous les coups au moins une fois
            move = &node->moves[node->total_number]; // faire un coup qu'on a jamais fait (on fait les premier coups dans l'ordre du tableau de Move)
            incrMove(node,node->total_number);
        }
        else{   // si on les a deja tous fait au moins une fois
            move = ucb(node); // faire coup avec le plus d'interet, ucb incrémente automatiquement
        }
        bot_intention = move->intention;
        botIntentionToPlayerIntention(bot_intention,player_intention);

        State *newstate = malloc(sizeof(State));
        copyState(state,newstate);

        unique_play(newstate,player_intention);    // on joue le coups choisi sur le state

        terminal_node = isFinished(state,player_intention); // est-ce que le state issu du coup donne un noeud terminal ?

        int gain;
        if (terminal_node){ // si oui, on ajoute le gain au gain du coup joué directement et on renvoie la valeur du state lié au noeud final
            int gain = eval(newstate);
            free(newstate);
            move->gain += gain;
            return gain;
        }
        else{   // sinon on ajoute le gain lié au coup joué grace a l'appel recusif et on renvoie ce gain.
            gain = mcts(newstate,root);
            free(newstate);
            move->gain += gain;
            return gain;
        }
        
    }
    else{   // dans le cas ou le noeud existait pas, on joue le premier coup disponible et on simule a partir de ce coup 
        // printf("IL A ETE CREE\n");
        Move *move = &node->moves[0]; // on prends le premier coup disponible (pas besoin de vérifier s'il existe au moins 1 coup dispo)
        int bot_intention,player_intention[3];

        bot_intention = move->intention;
        botIntentionToPlayerIntention(bot_intention,player_intention);

        int gain = simulation(*state, player_intention);
        // printf("GAIN %d\n",gain);
        move->gain += gain;
        incrMove(node,node->total_number);

        // printf("APRES MCTS :\n");
        for (int i=0; i< node->number_move;++i)
                // printf("gain : %d, number %d\n",node->moves[i].gain,node->moves[i].number);
        return gain;
    }
}

// Tree *findTree(Tree *root,long unsigned hash){
//     View *view = malloc(sizeof(View));
//     getView(&root->node->state,view);
//     long unsigned current_hash = hash_view(view);
//     if (hash < current_hash){
//         return findTree(root->link[0],hash);
//     }
//     else if (hash > current_hash){
//         return findTree(root->link[1],hash);
//     }
//     else {
//         return root;
//     }
// }

void playBot(State *state, Tree **root, int nb_iter, int player_intention[3]){

    // View *view = malloc(sizeof(View));
    // getView(state,view);
    // long unsigned initial_root = hash_view(view);
    // printf("INITIAL HASH : %ld\n",initial_root);
    // printf("pointeur du root dans playBot : %p\n",root);
    State *random_state = malloc(sizeof(State));
    while (nb_iter!=0){
        printf("=====ITERATION DU MCTS : %d======\n",nb_iter);
        createRandomState(state,random_state);// on fait un state aléatoire a chaque fois qu'on lance mcts
        // printCardStack(&random_state->board.deck);
        // printCardStack(&state->board.deck);
        mcts(random_state, root);
        
        // printf("RACINE APRES MCTS : \n");
        // for (int i=0; i< (*root)->node->number_move;++i)
        //     printf("gain : %d, number %d\n",(*root)->node->moves[i].gain,(*root)->node->moves[i].number);
        // free_quickAccess(random_state->player1.hand.quickAccess);
        // free_quickAccess(random_state->player2.hand.quickAccess);
        // printf("====Affichage arbre : %d ======",2-nb_iter);
        // treeDisplay(*root);
        printf("\n");
        nb_iter--;
    }
    // free_quickAccess(random_state->player1.hand.quickAccess);
    // free_quickAccess(random_state->player2.hand.quickAccess);

    // Tree *init_root = findTree(*root,initial_root);
    int inutile;
    MCTSNode *initial_node = getNode(state,root,root,&inutile);

    // for (int i=0; i<initial_node->number_move;++i)
    // printf("gain : %d, number %d\n",initial_node->moves[i].gain,initial_node->moves[i].number);

    // on choisi le coup qui a le meilleur gain
    int nb_coup = initial_node->number_move;
    int max=0,indicemax=0;
    int gain,number;
    for (int i=0;i<nb_coup;++i){
        gain = initial_node->moves[i].gain;
        number = initial_node->moves[i].number;
        if (number != 0 && max < gain/number){
            max = gain/number;

            indicemax = i;
        }
    }


    int bot_intention = initial_node->moves[indicemax].intention;
    botIntentionToPlayerIntention(bot_intention,player_intention);
    free(random_state);
}

void displayView(View *view){
    printf("\nAFFICHAGE VIEW\n");
    printf("PLAYER : \n");
    printPlayer(&view->player1);

}

// int main(){

//     Player player1,player2;
//     Board board;
//     initPlayer(&player1);
//     initPlayer(&player2);
//     initBoard(&board);

//     State *state = malloc(sizeof(State));

//     state->board = board;
//     state->player1 = player1;
//     state->player2 = player2;
//     state->current_player = 0;

//     // State *random_state = malloc(sizeof(State));

//     // Tree *root;

//     // for (int i=2; i<10;++i){
//     //     // createRandomState(state,random_state);
//     //     add_one_card(&state->board.deck,&state->player1.hand.content);
//     //     // printf("STATE :\n");
//     //     // printPlayer(&state->player1);
//     //     // View *view = malloc(sizeof(View));
//     //     // getView(state,view);
//     //     // displayView(view);

//     //     // printf("HASH : %ld\n",hash_view(view));

//     //     insertTree(&root,createMCTSNode(state));
//     //     // treeDisplay(root);
//     //     printf("\n");
//     // }
    

//     // treeFree(root);
//     // free(state);
//     // free(random_state);



//     int player_want[3];

//     // MCTSNode *node = createMCTSNode(state);
//     // Tree *root=createTree(node);
//     Tree *root=NULL;
//     // printf("nombre coup possible : %d\n",node->number_move);
//     playBot(state,&root,1000,player_want);

//     // treeDisplay(root);
//     printf("[%d,%d,%d]\n",player_want[0],player_want[1],player_want[2]);


//     // if (root->link[0]){
//     //     printf("il existe fils gauche\n");
//     // }
//     // else {
//     //     printf("il existe pas fils gauche\n");
//     // }
//     // if (root->link[1]){
//     //     printf("il existe fils droit\n");
//     // }
//     // else{
//     //     printf("il existe pas fils droit\n");
//     // }

//     // printf("%p %p\n",node,root->node);

//     // treeDisplay(root);
//     // printf("\n");

//     // free_quickAccess(state->player1.hand.quickAccess);
//     // free_quickAccess(state->player2.hand.quickAccess);

//     free(state);

//     treeFree(root);



//     return 0;
// }