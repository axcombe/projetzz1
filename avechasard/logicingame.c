#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "structure.h"
#include "logicingame.h"

void initdeck(CardStack * deck){
    int initcolor[54] ={DARK_BLUE,LIGHT_BLUE,BLACK,GREEN,PURPLE,//SHARK
                        DARK_BLUE,LIGHT_BLUE,BLACK,ORANGE,YELLOW,//SWIMMER
                        DARK_BLUE,DARK_BLUE,LIGHT_BLUE,LIGHT_BLUE,BLACK,GREEN,GREY,YELLOW,YELLOW,//CRAB
                        DARK_BLUE,DARK_BLUE,LIGHT_BLUE,LIGHT_BLUE,BLACK,BLACK,YELLOW,YELLOW,//BOAT
                        DARK_BLUE,DARK_BLUE,LIGHT_BLUE,BLACK,BLACK,GREEN,YELLOW,//FISH
                        DARK_BLUE,LIGHT_BLUE,BLACK,GREEN,GREY,YELLOW,//SHELL
                        LIGHT_BLUE,GREEN,PURPLE,GREY,YELLOW,//OCOTOPUS
                        PURPLE,ORANGE,PINK,//PENGUIN
                        RED,PINK,//SAILOR
                        PURPLE,GREY,GREEN,ORANGE};

    int inittype[54]={SHARK,SHARK,SHARK,SHARK,SHARK,
                    SWIMMER,SWIMMER,SWIMMER,SWIMMER,SWIMMER,
                    CRAB,CRAB,CRAB,CRAB,CRAB,CRAB,CRAB,CRAB,CRAB,
                    BOAT,BOAT,BOAT,BOAT,BOAT,BOAT,BOAT,BOAT,
                    FISH,FISH,FISH,FISH,FISH,FISH,FISH,
                    SHELL,SHELL,SHELL,SHELL,SHELL,SHELL,
                    OCTOPUS,OCTOPUS,OCTOPUS,OCTOPUS,OCTOPUS,
                    PENGUIN,PENGUIN,PENGUIN,
                    SAILOR,SAILOR,
                    LIGHTHOUSE,SHOAL_OF_FISH,PENGUIN_COLONY,CAPTAIN};

    for (int i=0;i<54;i++){
        deck->stack[i].color=initcolor[i];
        deck->stack[i].type=inittype[i];
        deck->stack[i].numsprite = i;
    }

    for (int j=0;j<4;j++){
        deck->stack[54+j].color=WHITE;
        deck->stack[54+j].type= MERMAID;
        deck->stack[54+j].numsprite = 54;
    }

    deck->size=58;

}

void shuffle(CardStack * deck) {
     //srand(time(NULL)); voir avec prof parce que degue

    for (int i = deck->size - 1; i > 0; i--) {
        int j = rand() % (i + 1);

        Card temp = deck->stack[i];
        deck->stack[i]=deck->stack[j];
        deck->stack[j]= temp;
    }
}

void add_one_card(CardStack * source,CardStack * destination){
    destination->stack[destination->size] = source->stack[source->size -1 ];
    source->size -= 1;
    destination->size += 1;
}

void initCardStack(CardStack * cardstack){
    cardstack->size=0;
}

void initBoard(Board * board){
    board->animate.isCurrent=0;
    board->havePair=0;
    board->selectcard=0;
    initdeck(&board->deck);
    shuffle (&board->deck);
    initCardStack(&board->discard1);
    initCardStack(&board->discard2);
    add_one_card(&board->deck, &board->discard1);
    add_one_card(&board->deck, &board->discard2);
}

void initQuickAccess(Node *quickAccess) {
    for (int i = 0; i < 14; i++) { 
        quickAccess[i].index = 0;
        quickAccess[i].next = NULL;
    }
}

void initCardPerColor(int *nbcardpercolor) {
    for (int i = 0; i < 14; i++) {
        nbcardpercolor[i] = 0;
    }
}

void initPlayer(Player * player){
    player->globalscore = 0;
    player->roundscore = 0;
    player->precroundscore = 0;
    initCardStack(&player->hand.content);
    initQuickAccess(player->hand.quickAccess);
    initCardPerColor(player->hand.nbcardpercolor);
    initCardStack(&player->pair);
}

void initGame (Board *board,Player * player){
    initBoard(board);
    initPlayer(&player[0]);
    initPlayer(&player[1]);
}

Node * createNode (int index, Node * adress){
    Node * newNode = malloc (sizeof(Node));
    newNode->index = index;
    newNode->next = adress;
    return newNode;
}

void insertNode (Node ** head,int index){
    Node *newNode = createNode(index, *head);
    *head = newNode;
}
    

void oneMoreCardInQuickAccess(Hand *hand) {
    CardStack content = hand->content;
    int type = content.stack[content.size - 1].type;
    Node **head = &(hand->quickAccess[type].next);
    insertNode(head, content.size - 1);
    hand->quickAccess[type].index += 1;
}

void scoreEndRound(Player * player){
    for (int i = 0; i<2 ; i++ ){
        player[i].globalscore += player[i].roundscore;
        player[i].precroundscore = player[i].roundscore;
        player[i].roundscore = 0;
    }
}
void colorManagement(Hand * hand){
    Card lastcard = hand->content.stack[hand->content.size-1];
    hand->nbcardpercolor[lastcard.color] += 1;
}

void havepair(Hand * hand,int * pair, int * index){
    *pair=0;
    int typelastcard = hand->content.stack[hand->content.size-1].type;
    if (typelastcard <= 1){
        if ( hand->quickAccess[1-typelastcard].next !=NULL){
            *pair = 1;
            *index = hand->quickAccess[1-typelastcard].next->index;
        }
    }
    else if (typelastcard <=4){
        if(hand->quickAccess[typelastcard].next->next != NULL){
            *pair = 1;
            *index = hand->quickAccess[typelastcard].next->next->index;
        }
    }
}

int PointOfColor (int * cardPerColor){
    int k=-1;
    int l=-1;
    int result=0;
    int max;
    int indice;
    for (int i=0 ; i<cardPerColor[WHITE] ; i++){
        max=-1;
        for (int j=0 ; j<10 ; j++){
            if (j != k && j != l){
                if (max < cardPerColor[j]){
                    max = cardPerColor[j];
                    indice = j;
                }
            }
        }
        result += max;
        if (i==1){
            k=indice;
        }
        if (i==2){
            l=indice;
        }
    }
    return result;
}

void updatepoint(Player player[2]){ //why doing that for two player whene only one is playing
    for (int i=0 ; i<2 ; i++){
        int score = 0;
        Node * quickAccess = player[i].hand.quickAccess;
        //colection
        score += quickAccess[SHELL].index >=1 ? (quickAccess[SHELL].index-1) *2 : 0 ;
        score += quickAccess[OCTOPUS].index >=1 ? (quickAccess[OCTOPUS].index-1) *3 : 0 ;
        score += quickAccess[PENGUIN].index >=1 ? (quickAccess[PENGUIN].index-1) *2 + 1 : 0;
        score += quickAccess[SAILOR].index >=1 ? (quickAccess[SAILOR].index-1) *5 : 0;
        //mult
        score += quickAccess[LIGHTHOUSE].index * quickAccess[BOAT].index;
        score += quickAccess[SHOAL_OF_FISH].index* quickAccess[FISH].index;
        score += quickAccess[PENGUIN_COLONY].index* quickAccess[PENGUIN].index*2;
        score += quickAccess[CAPTAIN].index* quickAccess[SAILOR].index*3;
        
        //Mermaid color
        score += PointOfColor(player[i].hand.nbcardpercolor);

        //pair
        score += player[i].pair.size/2;

        player[i].roundscore=score;
       // printf("player[%d].roundscore %d\n",i,player[i].roundscore);
    }
}

void shiftleft(CardStack * cards,int start, int end, int step){
    for (int i = start; i<end ; i++){
        cards->stack[i-step] = cards->stack[i];
    }
}

void free_node_QA(Node * head){
    Node *current = head->next; 
    Node *temp;

    while (current != NULL) {
        temp = current;
        current = current->next;
        free(temp);
    }

    head->next = NULL;
}

void free_quickAccess(Node *quickAccess) {
    for (int i = 0; i < 14 ; i++) {
        free_node_QA(&quickAccess[i]);
    }
}

void updateafterpairquickaccess(Node *quickAccess,int indexpair){
    for (int i = 0 ; i<14 ; i++){
        Node * node = quickAccess[i].next;
        while ( node != NULL){
            if (node->index > indexpair){
                node->index -= 1;
            }
            node=node->next;
        }
    }
}


void updateGameLogic(int multi, Board * board,int * currentPlayer,Player * player,int * playerIntention,int * botIntention, int * state){
    
    int intention;
    if (multi==0){
        if (*currentPlayer==1){
            intention = botIntention [0];
        }
        else{
            intention = *playerIntention;
        }
    }  
    else {
        intention = *playerIntention;
    }
    if (board->animate.isCurrent == 0){
        Hand * hand = &player[*currentPlayer].hand;
        if (intention == EYE){
            *state = STATE_VIEW_PAIR;
        }
        else{
            if (hand->nbcardpercolor [10] != 4){
                Animation animate = board->animate;
                (void) animate;
                if (board->havePair == 1){
                    animate.isCurrent=0;
                    //animate.isCurrent=1;
                    animate.Source[0]=WHAND;
                    animate.Source[1]=WHAND;
                    animate.indexSource[0]=player[*currentPlayer].hand.content.size -1 ;
                    animate.indexSource[1]=board->indexPair;
                    animate.Destination[0]=WPAIR;
                    animate.Destination[1]=WPAIR;
                    animate.indexDestination[0]=player[*currentPlayer].pair.size;
                    animate.indexDestination[1]=player[*currentPlayer].pair.size +1 ;
                    animate.cards[0] = board->deck.stack[player[*currentPlayer].hand.content.size -1];
                    animate.cards[1] = board->deck.stack[board->indexPair];
                    animate.frame = 0;
                    
                    int type = player[*currentPlayer].hand.content.stack[player[*currentPlayer].hand.content.size -1].type;
                    Node * quickaccess = player[*currentPlayer].hand.quickAccess  ;
                    if (type <= 1){//rajouter le changement d'indice;
                        Node * temp = quickaccess[1-type].next;
                        quickaccess[1-type].next = quickaccess[1-type].next->next;//faire free du truc qui disparait
                        free (temp);
                        free_node_QA(&quickaccess[type]);
                    }
                    else if ( type <=4){
                        free_node_QA(&quickaccess[type]);
                    }
                    updateafterpairquickaccess(player[*currentPlayer].hand.quickAccess,board->indexPair);
                    add_one_card(&hand->content, &player[*currentPlayer].pair);
                    
                    player[*currentPlayer].pair.stack[player[*currentPlayer].pair.size] = hand->content.stack[board->indexPair];
                    
                    player[*currentPlayer].pair.size += 1;
                    shiftleft(&hand->content, board->indexPair +1, hand->content.size, 1);
                    hand->content.size -= 1;
                    board->havePair = 0;
                    updatepoint(player);
                }
                else{
                    if (board->deck.size > 1){
                        if (intention != NONE ){
                            if(intention == FIN){
                                if (player[*currentPlayer].roundscore >=7){
                                    scoreEndRound(player); //update score;
                                    if (player[0].globalscore>=20 || player[1].globalscore>=20){
                                        *state = STATE_END_SCREEN;
                                    }
                                    else{
                                        *state = STATE_PAUSE_BETWEEN_ROUNDS;
                                    }
                                }
                            }
                            if(intention == DISCARD1){
                                if (board->discard1.size > 0){
                                    //donner l'animation
                                    animate.isCurrent=0;
                                    //animate.isCurrent=1;
                                    animate.Source[0]=WDISCARD1;
                                    animate.Source[1]=-1;
                                    animate.indexSource[0]=board->discard1.size-1;
                                    animate.indexSource[1]=-1;
                                    animate.Destination[0]=WHAND;
                                    animate.Destination[1]=-1;
                                    animate.indexDestination[0]=player[*currentPlayer].hand.content.size;
                                    animate.indexDestination[1]=-1;
                                    animate.cards[0] = board->discard1.stack[board->deck.size-1];
                                    animate.frame = 0;
                                    add_one_card(&board->discard1,&hand->content);
                                    oneMoreCardInQuickAccess(hand); 
                                    colorManagement(hand);
                                    havepair(hand,&board->havePair,&board->indexPair);
                                    if (board->havePair == 0){
                                        *currentPlayer = (*currentPlayer + 1)%2;
                                        updatepoint(player);
                                    }
                                    *playerIntention = -1;
                                } 
                            }
                            if(intention == DISCARD2){
                                if (board->discard2.size > 0){
                                    animate.isCurrent=0;
                                    //animate.isCurrent=1;
                                    animate.Source[0]=WDISCARD2;
                                    animate.Source[1]=-1;
                                    animate.indexSource[0]=board->discard2.size-1;
                                    animate.indexSource[1]=-1;
                                    animate.Destination[0]=WHAND;
                                    animate.Destination[1]=-1;
                                    animate.indexDestination[0]=player[*currentPlayer].hand.content.size;
                                    animate.indexDestination[1]=-1;
                                    animate.cards[0] = board->discard2.stack[board->deck.size-1];
                                    animate.frame = 0;
                                    //donner l'annimation
                                    add_one_card(&board->discard2,&hand->content);
                                    oneMoreCardInQuickAccess(hand); 
                                    colorManagement(hand);
                                    havepair(hand,&board->havePair,&board->indexPair);
                                    if (board->havePair == 0){
                                        *currentPlayer = (*currentPlayer + 1)%2;
                                        updatepoint(player);
                                    }
                                    *playerIntention = -1;
                                } 
                            }
                            if(intention == DECK){
                                if (board->deck.size > 1){
                                    *state = STATE_SELECT_CARD;
                                }
                            }    
                        }
                    }   
                    else{
                        scoreEndRound(player);
                        if (player[0].globalscore>=20 || player[1].globalscore>=20){
                            *state = STATE_END_SCREEN;
                        }
                        else{
                            *state = STATE_PAUSE_BETWEEN_ROUNDS;
                        }
                    }
                }
            }
            else{
                scoreEndRound(player);
                *state = STATE_END_SCREEN;
            }
        }
    }            
}  



void updateLogiqueSelectCard(int multi, Board * board,int * currentPlayer,Player * player,int * playerIntention,int * botIntention, int * state){
    int intention =-1;
    if (multi == 0){
        if (*currentPlayer==1){
            if (board->selectcard==-1){
                printf("cas 1 \n");
                intention = botIntention[1];
            }
            else{
                printf("cas 2 \n");
                intention = botIntention[2];
                board->selectcard=-1;
            }
        }
        else{
            intention = *playerIntention;
        }
    }
    else{
         intention = *playerIntention;
    }
    if (intention !=NONE){
        
        if (intention == SELECTCARD1){
            board->selectcard = 1;
        }
        if (intention ==SELECTCARD2){
            board->selectcard = 2;
        }
        if (board->selectcard != 0){
            Animation animate = board->animate;
            (void) animate;
            
            if (intention == SELECTDISCARD1){
                if (board->selectcard==1){
                    //animation
                    animate.isCurrent=0;
                    //animate.isCurrent=1;
                    animate.Source[0]=WSELECT1;
                    animate.Source[1]=WSELECT2;
                    animate.indexSource[0]=-1;
                    animate.indexSource[1]=-1;
                    animate.Destination[0]=WDISCARD1;
                    animate.Destination[1]=WHAND;
                    animate.indexDestination[0]=board->discard1.size;
                    animate.indexDestination[1]=player[*currentPlayer].hand.content.size;
                    animate.cards[0] = board->deck.stack[board->deck.size-1];
                    animate.cards[1] = board->deck.stack[board->deck.size-2];
                    animate.frame = 0;
                    
                   

                    add_one_card(&board->deck, &board->discard1);
                    add_one_card(&board->deck, &player[*currentPlayer].hand.content);
                    oneMoreCardInQuickAccess(&player[*currentPlayer].hand);
                    colorManagement(&player[*currentPlayer].hand);
                }
                else{
                
                    animate.isCurrent=0;
                    //animate.isCurrent=1;
                    animate.Source[0]=WSELECT1;
                    animate.Source[1]=WSELECT2;
                    animate.indexSource[0]=-1;
                    animate.indexSource[1]=-1;
                    animate.Destination[0]=WHAND;
                    animate.Destination[1]=WDISCARD2;
                    animate.indexDestination[0]=player[*currentPlayer].hand.content.size;
                    animate.indexDestination[1]=board->discard1.size;
                    animate.cards[0] = board->deck.stack[board->deck.size-1];
                    animate.cards[1] = board->deck.stack[board->deck.size-2];
                    animate.frame = 0;

                    
                    add_one_card(&board->deck, &player[*currentPlayer].hand.content);
                    oneMoreCardInQuickAccess(&player[*currentPlayer].hand);
                    add_one_card(&board->deck, &board->discard1);
                    colorManagement(&player[*currentPlayer].hand);
                }
                board->selectcard = 0;
                *state=STATE_GAMEPLAY;
                *playerIntention=-1;
                havepair(&player[*currentPlayer].hand,&board->havePair,&board->indexPair);
                if (board->havePair == 0 ){
                    *currentPlayer = (*currentPlayer + 1)%2;
                    updatepoint(player);
                }
            }
            if (intention == SELECTDISCARD2){
                if (board->selectcard==1){
                    animate.isCurrent=0;
                    //animate.isCurrent=1;
                    animate.Source[0]=WSELECT1;
                    animate.Source[1]=WSELECT2;
                    animate.indexSource[0]=-1;
                    animate.indexSource[1]=-1;
                    animate.Destination[0]=WDISCARD2;
                    animate.Destination[1]=WHAND;
                    animate.indexDestination[0]=board->discard2.size;
                    animate.indexDestination[1]=player[*currentPlayer].hand.content.size;
                    animate.cards[0] = board->deck.stack[board->deck.size-1];
                    animate.cards[1] = board->deck.stack[board->deck.size-2];
                    animate.frame = 0;
                    
                    add_one_card(&board->deck, &board->discard2);
                    add_one_card(&board->deck, &player[*currentPlayer].hand.content);
                    oneMoreCardInQuickAccess(&player[*currentPlayer].hand);
                    colorManagement(&player[*currentPlayer].hand);
                }
                else{
                
                    animate.isCurrent=0;
                    //animate.isCurrent=1;
                    animate.Source[0]=WSELECT1;
                    animate.Source[1]=WSELECT2;
                    animate.indexSource[0]=-1;
                    animate.indexSource[1]=-1;
                    animate.Destination[0]=WHAND;
                    animate.Destination[1]=WDISCARD2;
                    animate.indexDestination[0]=player[*currentPlayer].hand.content.size;
                    animate.indexDestination[1]=board->discard1.size;
                    animate.cards[0] = board->deck.stack[board->deck.size-1];
                    animate.cards[1] = board->deck.stack[board->deck.size-2];
                    animate.frame = 0;

                    add_one_card(&board->deck, &player[*currentPlayer].hand.content);
                    oneMoreCardInQuickAccess(&player[*currentPlayer].hand);
                    add_one_card(&board->deck, &board->discard2);
                    colorManagement(&player[*currentPlayer].hand);
                }
                board->selectcard = 0;
                *state = STATE_GAMEPLAY;
                *playerIntention=-1;
                havepair(&player[*currentPlayer].hand,&board->havePair,&board->indexPair);
                if (board->havePair == 0){
                    *currentPlayer = (*currentPlayer + 1)%2;
                    updatepoint(player);
                }
            }
        }
    }
}  


void printCardStack(CardStack *stack) {
    for (int i = 0; i < stack->size; i++) {
        printf("Card %d: Color = %d, Type = %d, NumSprite = %d\n", i, stack->stack[i].color, stack->stack[i].type, stack->stack[i].numsprite);
    }
}

// Fonction pour afficher l'état d'un Player (à des fins de test)
void printPlayer(Player *player) {
    printf("Global Score: %d\n", player->globalscore);
    printf("Round Score: %d\n", player->roundscore);
    printf("Previous Round Score: %d\n", player->precroundscore);
    printf("Hand content:\n");
    printCardStack(&player->hand.content);
    printf("Pair content:\n");
    printCardStack(&player->pair);

}

// Fonction pour afficher les noeuds liés d'un Node
void printNodeList(Node *node) {
    while (node != NULL) {
        printf("Index: %d -> ", node->index);
        node = node->next;
    }
    printf("NULL\n");
}

// Fonction pour afficher le quickAccess d'un joueur
void printQuickAccess(Node *quickAccess) {
    for (int i = 0; i < 14; i++) {
        printf("quickAccess[%d]: ", i);
        printNodeList(&quickAccess[i]);
    }
}

/*
 int main (){
     Player player[2];
     initPlayer(&player[0]);
     initPlayer(&player[1]);
     printPlayer(&player[0]);
     printPlayer(&player[0]);

    return 1;
 }*/

void playMove (State * state,int * currentplayer,int intention[3],int * finish){
    Board board= state->board;
    Player player[2];
    player[0]=state->player1;
    player[1]=state->player2;
    if (intention[0]==END){
        updatepoint(player);
        *finish = 1;
    }
    if (intention[0]==DISCARD1){
        add_one_card(&board.discard1,&player[*currentplayer].hand.content);
        oneMoreCardInQuickAccess(&player[*currentplayer].hand);
        colorManagement(&player[*currentplayer].hand);
        havepair(&player[*currentplayer].hand,&board.havePair,&board.indexPair);
        if (board.havePair == 0){
            *currentplayer = (*currentplayer + 1)%2;
            updatepoint(player);
        }
        else{
            int type = player[*currentplayer].hand.content.stack[player[*currentplayer].hand.content.size -1].type;
            Node * quickaccess = player[*currentplayer].hand.quickAccess  ;
            if (type <= 1){//rajouter le changement d'indice;
                //Node * temp = quickaccess[1-type].next;
                quickaccess[1-type].next = quickaccess[1-type].next->next;
                //free (temp);
                free_node_QA(&quickaccess[type]);
            }
            else if ( type <=4){
                free_node_QA(&quickaccess[type]);
            }
            updateafterpairquickaccess(player[*currentplayer].hand.quickAccess,board.indexPair);
            add_one_card(&player[*currentplayer].hand.content, &player[*currentplayer].pair);
            player[*currentplayer].pair.stack[player[*currentplayer].pair.size]=player[*currentplayer].hand.content.stack[board.indexPair];
            player[*currentplayer].pair.size += 1;
            shiftleft(&player[*currentplayer].hand.content, board.indexPair +1, player[*currentplayer].hand.content.size, 1);
            player[*currentplayer].hand.content.size -= 1;
            board.havePair = 0;
            updatepoint(player);
        }

    }
    if (intention[0]==DISCARD2){
        add_one_card(&board.discard2,&player[*currentplayer].hand.content);
        oneMoreCardInQuickAccess(&player[*currentplayer].hand);
        colorManagement(&player[*currentplayer].hand);
        havepair(&player[*currentplayer].hand,&board.havePair,&board.indexPair);
        if (board.havePair == 0){
            *currentplayer = (*currentplayer + 1)%2;
            updatepoint(player);
        }
        else{
            int type = player[*currentplayer].hand.content.stack[player[*currentplayer].hand.content.size -1].type;
            Node * quickaccess = player[*currentplayer].hand.quickAccess  ;
            if (type <= 1){//rajouter le changement d'indice;
                //Node * temp = quickaccess[1-type].next;
                quickaccess[1-type].next = quickaccess[1-type].next->next;
                //free (temp);
                free_node_QA(&quickaccess[type]);
            }
            else if ( type <=4){
                free_node_QA(&quickaccess[type]);
            }
            updateafterpairquickaccess(player[*currentplayer].hand.quickAccess,board.indexPair);
            add_one_card(&player[*currentplayer].hand.content, &player[*currentplayer].pair);
            player[*currentplayer].pair.stack[player[*currentplayer].pair.size]=player[*currentplayer].hand.content.stack[board.indexPair];
            player[*currentplayer].pair.size += 1;
            shiftleft(&player[*currentplayer].hand.content, board.indexPair +1, player[*currentplayer].hand.content.size, 1);
            player[*currentplayer].hand.content.size -= 1;
            board.havePair = 0;
            updatepoint(player);
        }
    }
    if (intention[0]==DECK){
        if (intention[1]==SELECTCARD1){
            if (intention[2]==SELECTDISCARD1){
                add_one_card(&board.deck, &board.discard1);
                add_one_card(&board.deck, &player[*currentplayer].hand.content);
                oneMoreCardInQuickAccess(&player[*currentplayer].hand);
            }
            else{
                add_one_card(&board.deck, &player[*currentplayer].hand.content);
                oneMoreCardInQuickAccess(&player[*currentplayer].hand);
                add_one_card(&board.deck, &board.discard1);
            }
        }
        else{
            if (intention[2]==SELECTDISCARD1){
                add_one_card(&board.deck, &board.discard2);
                add_one_card(&board.deck, &player[*currentplayer].hand.content);
                oneMoreCardInQuickAccess(&player[*currentplayer].hand);
            }
            else{
                add_one_card(&board.deck, &player[*currentplayer].hand.content);
                oneMoreCardInQuickAccess(&player[*currentplayer].hand);
                add_one_card(&board.deck, &board.discard2);
            }
        }
        colorManagement(&player[*currentplayer].hand);
        havepair(&player[*currentplayer].hand,&board.havePair,&board.indexPair);
        if (board.havePair == 0){
            *currentplayer = (*currentplayer + 1)%2;
            updatepoint(player);
        }
        else{
            int type = player[*currentplayer].hand.content.stack[player[*currentplayer].hand.content.size -1].type;
            Node * quickaccess = player[*currentplayer].hand.quickAccess  ;
            if (type <= 1){//rajouter le changement d'indice;
                //Node * temp = quickaccess[1-type].next;
                quickaccess[1-type].next = quickaccess[1-type].next->next;
                //free (temp);
                free_node_QA(&quickaccess[type]);
            }
            else if ( type <=4){
                free_node_QA(&quickaccess[type]);
            }
            updateafterpairquickaccess(player[*currentplayer].hand.quickAccess,board.indexPair);
            add_one_card(&player[*currentplayer].hand.content, &player[*currentplayer].pair);
            player[*currentplayer].pair.stack[player[*currentplayer].pair.size]=player[*currentplayer].hand.content.stack[board.indexPair];
            player[*currentplayer].pair.size += 1;
            shiftleft(&player[*currentplayer].hand.content, board.indexPair +1, player[*currentplayer].hand.content.size, 1);
            player[*currentplayer].hand.content.size -= 1;
            board.havePair = 0;
            updatepoint(player);
        }

    }
    if (player[*currentplayer].hand.nbcardpercolor [10] == 4 || board.deck.size <= 1 ){
        updatepoint(player);
        *finish=1;
    }
    state->player1=player[0];
    state->player2=player[1];
    state->board=board;
}