#ifndef GRAPHIC_H
#define GRAPHIC_H

#include "structure.h"
#include "structureSDL.h"


//void endSDL(SDL_Window *window, SDL_Renderer *renderer);
//SDL_Texture* loadTexture(char *file_image_name, SDL_Window *window, SDL_Renderer *renderer);

void SetRect(SDL_Rect *rect,int x, int y, int w, int h);

//void initSDL(SDL_Window **window, SDL_Renderer **renderer, SDL_Texture **textures_mainMenu, SDL_Texture **texture_Eye, SDL_Texture **texture_Card, SDL_Texture **texture_Chest,SDL_Texture **texture_Back);
void getHandZone(Zone * zone, int nb_card0, int nb_card1,int sizewindowx,int sizewindowy);
void drawRect(SDL_Renderer *renderer, int x, int y, int w, int h);
void getZoneSDL(Zone *zone, SDL_Window *window,Player player[2],int current_player,int multiplayer);

void animation(Zone *zone,  SDL_Renderer *Render, Animation *anim, int *sprite_eye, SDL_Texture **texEye, int is_eye_over,  SDL_Texture **texChest, int is_chest_over, Player * player);
void SDL_displayChest(SDL_Renderer *Render, SDL_Rect chestZone, SDL_Texture **tex, int is_chest_over, Player * player);
void SDL_displayEye(SDL_Renderer *Render, SDL_Rect eyeZone, int *sprite_eye, SDL_Texture **tex, int is_eye_over);
void displayBoardCard(SDL_Window *window, SDL_Texture **Texture, SDL_Renderer *Renderer, Player player1, Player player2, Board board_state, Zone ZoneSDL, int current_player, int multiplayer, int is_card_over[2], int is_deck_discard_over[3]);
void showGame(SDL_Window *window, Board board_state,Zone zone, Player *player, int current_player, int multiplayer, SDL_Renderer *renderer, Animation *anim,
               int *sprite_eye, float *crenelage,SDL_Texture **texEye, SDL_Texture **texture_Chest, SDL_Texture **texture_Card, SDL_Texture ** texture_Back,int anim_white[2]);
void SLD_displayBackground(SDL_Window *window, SDL_Texture **texture, SDL_Renderer *renderer);
void getPairZone(Zone *zone);
void getBigPairZone(Zone *zone,int nb_pair_0,int nb_pair_1 ,int sizewindowx, int sizewindowy);
void displayStack(SDL_Window *window, SDL_Texture **Texture, SDL_Renderer *Renderer, CardStack Stack, SDL_Rect *toDRect, int flipy, int to_hide,  int is_card_over[2]);
void showViewPair(SDL_Window *window, Board board_state, Zone zone, Player *player, int current_player, int multiplayer, SDL_Renderer *renderer, Animation *anim,
              int *sprite_eye, float *crenelage, SDL_Texture **texEye, SDL_Texture **texture_Chest, SDL_Texture **texture_Card,SDL_Texture ** texture_Back,int anim_white[2]);

void displayCard(SDL_Window *window, SDL_Texture **Texture, SDL_Renderer *Renderer, Card Card, SDL_Rect toDRect, SDL_RendererFlip flipx, int flipy,  int to_hide, int size);
void showSelectCard(SDL_Window *window, Board board_state, Zone zone, Player *player, int current_player, int multiplayer, SDL_Renderer *renderer, Animation *anim,
              int *sprite_eye, float *crenelage, SDL_Texture **texEye, SDL_Texture **texture_Chest, SDL_Texture **texture_Card,SDL_Texture ** texture_Back,int anim_white[2]);

void SDL_displayBigPairs(SDL_Window *window, SDL_Texture **texture, SDL_Renderer *renderer, Zone zone, Player player[2], int current_player)
;
#endif