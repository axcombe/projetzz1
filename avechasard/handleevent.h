#ifndef HANDLEEVENT_H
#define HANDLEEVENT_H

void handleGameInput(SDL_Window * window, Zone zone, int * quit, int * playerIntention, int * select );

void handleSelectCardInput(SDL_Window * window, Zone zone, int * quit, int * playerIntention, int * select );

void handleViewPairInput (SDL_Window * window, Zone zone, int * quit, int * state, int * select ,int *  );

#endif