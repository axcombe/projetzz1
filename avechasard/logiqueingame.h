

#include "structure.h"

void initdeck(CardStack * deck);
void shuffle(CardStack * deck);
void add_one_card(CardStack * source,CardStack * destination);
void initCardStack(CardStack * cardstack);
void initBoard(Board * board);
void initQuickAccess(Node *quickAccess);
void initCardPerColor(int *nbcardpercolor);
void initPlayer(Player * player);
Node * createNode (int index, Node * adress);
void insertNode (Node ** head,int index);
void oneMoreCardInQuickAccess(Hand *hand);
void scoreEndRound(Player * player);
void colorManagement(Hand * hand);
void havepair(Hand * hand,int * pair, int * index);
int PointOfColor (int * cardPerColor);
void updatepoint(Player player[2]);
void shiftleft(CardStack * cards,int start, int end, int step);
void free_node_QA(Node head);
void free_quickAccess(Node *quickAccess);
void updateGameLogic(int multi, Board * board,int * currentPlayer,Player * player,int playerIntention,int * botIntention, int * state);
void updateLogiqueSelectCard(int multi, Board * board,int * currentPlayer,Player * player,int playerIntention,int * botIntention, int * state);
void printCardStack(CardStack *stack);
void printPlayer(Player *player);
void printNodeList(Node *node);
void printQuickAccess(Node *quickAccess);