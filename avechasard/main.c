#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "structure.h"
#include "structureSDL.h"
#include "SDL_commun.h"
#include "mainmenu.h"
#include "logicingame.h"
#include "graphic.h"
#include "handleevent.h"
#include "betweenround.h"
#include "end_screen.h"
#include "mcts.h"

//gcc -o main mainmenu.c main.c SDL_commun.c graphic.c  logicingame.c handleevent.c  -lSDL2_image -lSDL2 -lSDL2_ttf -Wall -Wextra -g

int main() {

    srand(time(NULL));

    // Initialize SDL and resources
    int multi = 1;
    (void) multi;
    int currentPlayer=0;
    (void) currentPlayer;
    
    SDL_Window *window = NULL;
    SDL_Renderer *renderer = NULL;
    SDL_Texture *texture_Chest;
    SDL_Texture *texture_Card;
    SDL_Texture *texture_Back;
    SDL_Texture *texture_end[2];
    SDL_Texture *Texturesscreentransition[6];
    for (int i =0 ; i<6 ; i++ ){
        Texturesscreentransition[i]=NULL;
    }
    SDL_Texture *texture_eye[2];
    for (int i =0 ; i<2 ; i++ ){
        texture_eye[i]=NULL;
        texture_end[i]=NULL;
    }

    Board board;
    Player player[2];
    Zone zone;
    
    initSDL(&window, &renderer, Texturesscreentransition,texture_eye,&texture_Card,&texture_Chest,&texture_Back, texture_end);
    initGame(&board, player);
    
    int gameState = STATE_MAIN_MENU; 
    int quit = 0;
    

    int taillefenx=0,taillefeny=0;


    SDL_Rect playRect={0}, quitRect={0};
    int input = -1;
    float offset[2] = {0,0};

    //truc pour graphic.c
    int sprite_eye = 0;
    float crenelage = 0;
    int select[2] = {2, -1};
    int playerintention = -1;
    (void) playerintention;
    Animation *anim = NULL;

    //truc pour logic
    int botIntention[3]={-1,-1,-1};

    //betweenround 
    SDL_Rect nextroundRect={0};




    while (!quit) {

        SDL_GetWindowSize(window, &taillefenx, &taillefeny);
        getZoneSDL(&zone,window,player,currentPlayer,multi);

        switch (gameState) {

            case STATE_MAIN_MENU:

                processMainMenuInput(&quit,playRect,quitRect,&input,&gameState);
                showMainMenu (window,renderer,Texturesscreentransition, taillefenx, taillefeny,&playRect,&quitRect,input, offset);
                SDL_RenderPresent(renderer);
                break;

            case STATE_GAMEPLAY:
                showGame(window, board, zone, player, currentPlayer, multi, renderer, anim,
                    &sprite_eye, &crenelage, texture_eye, &texture_Chest, &texture_Card, &texture_Back, select);
                SDL_RenderPresent(renderer);
                if (multi==0){
                    if (currentPlayer==1){
                        State *state = malloc(sizeof(State));
                        state->board = board;
                        state->player1 = player[0];
                        state->player2 = player[1];
                        state->current_player = 0;
                        Tree *root=NULL;
                        playBot(state,&root, 10 , botIntention);
                    }
                    else{
                        handleGameInput(window, zone, &quit, &playerintention, select );
                    }
                }  
                else {
                    handleGameInput(window, zone, &quit, &playerintention, select );
                }
                //handleGameInput(window, zone, &quit, &playerintention, select );
                updateGameLogic(multi, &board,&currentPlayer,player,&playerintention,botIntention, &gameState);
                break;
//
            case STATE_SELECT_CARD:
                showSelectCard(window, board, zone, player, currentPlayer, multi, renderer, anim,
                    &sprite_eye, &crenelage, texture_eye, &texture_Chest, &texture_Card, &texture_Back, select);
                SDL_RenderPresent(renderer);
                handleSelectCardInput(window, zone, &quit, &playerintention, select );
                updateLogiqueSelectCard( multi, &board, &currentPlayer,player, &playerintention, botIntention, &gameState);
                break;
//
//
            case STATE_VIEW_PAIR:
                showViewPair(window, board, zone, player, currentPlayer, multi, renderer, anim,
                    &sprite_eye, &crenelage, texture_eye, &texture_Chest, &texture_Card, &texture_Back, select);
                SDL_RenderPresent(renderer);
                handleViewPairInput (window, zone, &quit, &gameState, select, &playerintention );
                break;
//
// 
            case STATE_PAUSE_BETWEEN_ROUNDS://not sure
                showBetweenRound (window,renderer, Texturesscreentransition, taillefenx, taillefeny, &nextroundRect, input,player);
                processBetweenRoundInput(&quit,nextroundRect,&input,&board,player,&gameState);
                //updateInGamePauseBetweenRounds();
                break;
//
//            case STATE_PAUSE_INGAME:
//                showInGamePause();
//                processInGamePauseInput();
//                updateInGamePause();
//                break;
//
            case STATE_END_SCREEN:
                showEndScreen (window,renderer,texture_end,taillefenx,taillefeny, &playRect, &quitRect,input, offset, player , multi);
                processEndScreenInput(&quit, playRect, quitRect, &input,&board,player,&gameState,&currentPlayer);
                //updateEndScreen();
                break;

            default:
                quit = 1;
                break;
            }
    }
    free_quickAccess(player[0].hand.quickAccess);
    free_quickAccess(player[1].hand.quickAccess);
    endSDL(&window, &renderer, Texturesscreentransition,texture_eye,&texture_Card,&texture_Chest,&texture_Back,texture_end);
    return 0;
}