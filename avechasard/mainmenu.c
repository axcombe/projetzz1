#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <stdlib.h>

#include "structure.h"
#include "SDL_commun.h"
#include "mainmenu.h"

//gcc mainmenu.c -o main -Wall -Wextra -lSDL2 -lSDL2_ttf -lSDL2_image

/* 
SDL_Texture* loadTexture(char  *  file_image_name, SDL_Window *window, SDL_Renderer *renderer ){
    SDL_Surface *my_image = NULL;           // Variable de passage
    SDL_Texture* my_texture = NULL;         // La texture

    my_image = IMG_Load(file_image_name);   // Chargement de l'image dans la surface
                                            // image=SDL_LoadBMP(file_image_name); fonction standard de la SDL, 
                                            // uniquement possible si l'image est au format bmp 
    if (my_image == NULL) {
        printf("Erreur  %s\n", SDL_GetError());
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDL_Quit();
        exit(EXIT_FAILURE);
    }
    my_texture = SDL_CreateTextureFromSurface(renderer, my_image); // Chargement de l'image de la surface vers la texture
    SDL_FreeSurface(my_image);                                     // la SDL_Surface ne sert que comme élément transitoire 
    if (my_texture == NULL){
        printf("Erreur  %s\n", SDL_GetError());
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDL_Quit();
        exit(EXIT_FAILURE);
    }

    return my_texture;
}

SDL_Texture* renderText(SDL_Renderer *renderer, TTF_Font *font, char *text, SDL_Color color) {
    SDL_Surface *surface = TTF_RenderText_Blended(font, text, color);
    if (!surface) {
        printf("TTF_RenderText_Blended Error: %s\n", TTF_GetError());
        return NULL;
    }
    SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_FreeSurface(surface);
    if (!texture) {
        printf("SDL_CreateTextureFromSurface Error: %s\n", SDL_GetError());
    }
    return texture;
}

void endSDL(SDL_Window *window, SDL_Renderer *renderer,SDL_Texture ** textures) {
    if (renderer) {
        SDL_DestroyRenderer(renderer);
    }
    if (window) {
        SDL_DestroyWindow(window);
    }
   
    for (int i = 0; i < 6; ++i) {
        SDL_DestroyTexture(textures[i]);
    }

    TTF_Quit();
    SDL_Quit();
}

void initSDL1(SDL_Window **window, SDL_Renderer **renderer,SDL_Texture ** Textures) {
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
        endSDL1(*window, *renderer,Textures);
    }

    
    
    //dimension of the screen 
    SDL_DisplayMode dim;
    if (SDL_GetCurrentDisplayMode(0, &dim) != 0) {
        printf("SDL could not get display mode! SDL_Error: %s\n", SDL_GetError());
        endSDL1(*window, *renderer,Textures);
    }

    float WINDOW_SCALE = 0.8;   // percentage of the window size relative to the screen

    int sizedevicex = dim.w;
    int sizedevicey = dim.h;

    int sizewindowx = sizedevicex * WINDOW_SCALE;
    int sizewindowy = sizedevicey * WINDOW_SCALE;

    //window creation
    *window = SDL_CreateWindow("Quarto Board", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, sizewindowx, sizewindowy, SDL_WINDOW_RESIZABLE);
    if (*window == NULL) {
        printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
        endSDL1(*window, *renderer,Textures);
    }

    //renderer creation
    *renderer = SDL_CreateRenderer(*window, -1, SDL_RENDERER_ACCELERATED);
    if (*renderer == NULL) {
        printf("Renderer could not be created! SDL_Error: %s\n", SDL_GetError());
        endSDL1(*window, *renderer,Textures);
    }

    // line for alpha be considerate
    SDL_SetRenderDrawBlendMode(*renderer,SDL_BLENDMODE_BLEND);

    //init TTF 
    if (TTF_Init() == -1) {
        printf("TTF_Init: %s\n", TTF_GetError());
        endSDL1(*window, *renderer,Textures);
    }

    
    char* file_names[] = {"sprite/mainmenu/1.png", "sprite/mainmenu/2.png", "sprite/mainmenu/3.png", "sprite/mainmenu/4.png", "sprite/mainmenu/5.png","sprite/mainmenu/plage1.png"};

    for (int i = 0; i < 6; ++i) {
        Textures[i] = loadTexture(file_names[i], *window, *renderer);
    }

} */

void showMainMenu (SDL_Window * window,SDL_Renderer * renderer,SDL_Texture ** textures,int taillefenx,int taillefeny, SDL_Rect *playRect, SDL_Rect *quitRect,int input,float * offset){

    TTF_Font *font = TTF_OpenFont("font/fontcursive.ttf", 64);  // Replace with your font path
    if (!font) {
        printf("Failed to load font: %s\n", TTF_GetError());
        for (int i = 0; i < 6; ++i) {
            SDL_DestroyTexture(textures[i]);
        }
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        TTF_Quit();
        IMG_Quit();
        SDL_Quit();
    }

    SDL_Color black = {0, 0, 0, 255};
    SDL_Color white = {255, 255, 255, 255};

    // Create text textures
    SDL_Texture *titleTexture = renderText(renderer, font, "Sea, Salt and Paper", black);
    SDL_Texture *playTexture = renderText(renderer, font, "JOUER", white);
    SDL_Texture *quitTexture = renderText(renderer, font, "QUITTER", white);

    if (!titleTexture || !playTexture || !quitTexture) {
        TTF_CloseFont(font);
        for (int i = 0; i < 6; ++i) {
            SDL_DestroyTexture(textures[i]);
        }
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        TTF_Quit();
        IMG_Quit();
        SDL_Quit();
    }

    offset[0] -= 0.2;
    offset[1] -= 0.1;
    if (offset[0] <= -taillefenx){
        offset[0] += taillefenx;
    };
    if (offset[1] <= -taillefenx){
        offset[1] += taillefenx;
    };
    SDL_RenderClear(renderer);
    int width, height;

    // Calculate dimensions for images 1 to 3
    SDL_Rect rect1to3 = {0, 0, taillefenx, taillefeny};
    // Display images 1 to 3
    for (int i = 0; i < 3; ++i) {
        SDL_QueryTexture(textures[i], NULL, NULL, &width, &height);
        SDL_RenderCopy(renderer, textures[i], NULL, &rect1to3);
    }

    // Calculate dimensions for image 4 with parallax effect
    SDL_Rect rect4a = {offset[1], 0, taillefenx, taillefeny};
    SDL_Rect rect4b = {offset[1] + taillefenx, 0, taillefenx, taillefeny};

    // Display image 4 with parallax effect
    SDL_RenderCopy(renderer, textures[3], NULL, &rect4a);
    SDL_RenderCopy(renderer, textures[3], NULL, &rect4b);

    // Calculate dimensions for image 5 with parallax effect
    SDL_Rect rect5a = {offset[0], 0, taillefenx, taillefeny};
    SDL_Rect rect5b = {offset[0] + taillefenx, 0, taillefenx, taillefeny};

    // Display image 5 with parallax effect
    SDL_RenderCopy(renderer, textures[4], NULL, &rect5a);
    SDL_RenderCopy(renderer, textures[4], NULL, &rect5b);

    // Calculate dimensions for image 6
    SDL_Rect rectplage = {0, 0.22*taillefeny, taillefenx, taillefeny - 0.22*taillefeny};
    SDL_RenderCopy(renderer, textures[5], NULL, &rectplage);

    // Render title text
    SDL_QueryTexture(titleTexture, NULL, NULL, &width, &height);
    SDL_Rect titleRect = {taillefenx / 2 - width / 2, taillefeny / 4 - height / 2, width, height};
    SDL_RenderCopy(renderer, titleTexture, NULL, &titleRect);

    // Render play button
    SDL_QueryTexture(playTexture, NULL, NULL, &width, &height);
    *playRect = (SDL_Rect){50, taillefeny - height - 50, width, height};
    SDL_Rect playBgRect = {40, taillefeny - height - 60, width + 20, height + 20};
    SDL_SetRenderDrawColor(renderer, 0, 0, 255, input == 2? 255 : 100); // Blue color
    SDL_RenderFillRect(renderer, &playBgRect);
    SDL_RenderCopy(renderer, playTexture, NULL, playRect);

    // Render quit button
    SDL_QueryTexture(quitTexture, NULL, NULL, &width, &height);
    *quitRect = (SDL_Rect){taillefenx - width - 50, taillefeny - height - 50, width, height};
    SDL_Rect quitBgRect = {taillefenx - width - 60, taillefeny - height - 60, width + 20, height + 20};
    SDL_SetRenderDrawColor(renderer, 0, 0, 255, input == 1? 255 : 100); // Blue color
    SDL_RenderFillRect(renderer, &quitBgRect);
    SDL_RenderCopy(renderer, quitTexture, NULL, quitRect);


    SDL_RenderPresent(renderer);

    SDL_DestroyTexture(titleTexture);
    SDL_DestroyTexture(playTexture);
    SDL_DestroyTexture(quitTexture);
    TTF_CloseFont(font);
    
}
/* 
int isPointInRect(int x, int y, SDL_Rect rect) {
    return (x > rect.x) && (x < rect.x + rect.w) &&
           (y > rect.y) && (y < rect.y + rect.h);
} */


void processMainMenuInput(int * quit,SDL_Rect playRect,SDL_Rect quitRect,int * input,int * state){

    SDL_Event e;
    while (SDL_PollEvent(&e) != 0) {
        if (e.type == SDL_QUIT) {
            *quit = 1;
        }
        if (e.type == SDL_KEYUP) {
            if (e.key.keysym.sym == SDLK_UP) {
                *quit = 1;
            }
        }
        if (e.type ==SDL_MOUSEMOTION) {
            int x, y;
            SDL_GetMouseState(&x, &y);
            if (isPointInRect(x,y,quitRect)) {
                *input= 1;
            }
            else if (isPointInRect(x,y,playRect)) {
                *input= 2;
            }
            else{
                *input=-1;
            }
        }
        if (e.type == SDL_MOUSEBUTTONDOWN) {
            int x, y;
            SDL_GetMouseState(&x, &y);
            if (x >= quitRect.x && x <= quitRect.x + quitRect.w &&
                y >= quitRect.y && y <= quitRect.y + quitRect.h) {
                *quit = 1;
            }
            if (x >= playRect.x && x <= playRect.x + playRect.w &&
                y >= playRect.y && y <= playRect.y + playRect.h) {
                *state = STATE_GAMEPLAY;
                
            }

            
        }
    }
}

/*
int main() {
    
    SDL_Window* window = NULL;
    SDL_Renderer* renderer = NULL;
    SDL_Texture* textures[6];
    
    initSDL1(&window,&renderer,textures);
    int quit = 0;
    int taillefenx=0,taillefeny=0;
    SDL_Rect playRect={0}, quitRect={0};
    int input = -1;
    float offset[2] = {0,0};

    while (!quit) {

        SDL_GetWindowSize(window, &taillefenx, &taillefeny);
        processMainMenuInput(&quit,playRect,quitRect,&input);
        showMainMenu (window,renderer,textures, taillefenx, taillefeny,&playRect,&quitRect,input, offset);

    }
    endSDL1(window,renderer,textures);

    return 0;
}*/
