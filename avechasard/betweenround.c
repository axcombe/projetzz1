#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <stdlib.h>

//gcc mainmenu.c -o main -Wall -Wextra -lSDL2 -lSDL2_ttf -lSDL2_image
/* 
SDL_Texture* loadTexture(char  *  file_image_name, SDL_Window *window, SDL_Renderer *renderer ){
    SDL_Surface *my_image = NULL;
    SDL_Texture* my_texture = NULL;

    my_image = IMG_Load(file_image_name);
    if (my_image == NULL) {
        printf("Erreur  %s\n", SDL_GetError());
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDL_Quit();
        exit(EXIT_FAILURE);
    }
    my_texture = SDL_CreateTextureFromSurface(renderer, my_image);
    SDL_FreeSurface(my_image);
    if (my_texture == NULL){
        printf("Erreur  %s\n", SDL_GetError());
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDL_Quit();
        exit(EXIT_FAILURE);
    }

    return my_texture;
}

SDL_Texture* renderText(SDL_Renderer *renderer, TTF_Font *font, char *text, SDL_Color color) {
    SDL_Surface *surface = TTF_RenderText_Blended(font, text, color);
    if (!surface) {
        printf("TTF_RenderText_Blended Error: %s\n", TTF_GetError());
        return NULL;
    }
    SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_FreeSurface(surface);
    if (!texture) {
        printf("SDL_CreateTextureFromSurface Error: %s\n", SDL_GetError());
    }
    return texture;
} */

#include "structure.h"
#include "SDL_commun.h"
#include "betweenround.h"
#include "logicingame.h"

void showBetweenRound (SDL_Window * window, SDL_Renderer * renderer, SDL_Texture* Texturesscreentransition[], int taillefenx, int taillefeny,SDL_Rect * nextroundRect,int input, Player * player){
    
    float scoretotal1 = (float) player[0].globalscore;
    float scoretotal2 = (float) player[1].globalscore;

    TTF_Font *font = TTF_OpenFont("font/fontcursive.ttf", 64);  
    TTF_Font *scoreFont = TTF_OpenFont("font/fontcursive.ttf", 32);

    if (!font || !scoreFont) {
        printf("Failed to load font: %s\n", TTF_GetError());
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        TTF_Quit();
        IMG_Quit();
        SDL_Quit();
        exit(EXIT_FAILURE);
    }

    SDL_Color black = {0, 0, 0, 255};
    SDL_Color white = {255, 255, 255, 255};

    // Create text textures
    SDL_Texture *titleTexture = renderText(renderer, font, "Cumul des points :", black);
    SDL_Texture *joueur1Texture = renderText(renderer, font, "Joueur 1", black);
    SDL_Texture *joueur2Texture = renderText(renderer, font, "Joueur 2", black);
    SDL_Texture *nextRoundTexture = renderText(renderer, font, "Manche Suivante", white);

     char scoreTotal1Text[20];
    snprintf(scoreTotal1Text, sizeof(scoreTotal1Text), "Score: %d", (int) scoretotal1);
    SDL_Texture *scoreTotal1Texture = renderText(renderer, scoreFont, scoreTotal1Text, black);

    char scoreTotal2Text[20];
    snprintf(scoreTotal2Text, sizeof(scoreTotal2Text), "Score: %d", (int)scoretotal2);
    SDL_Texture *scoreTotal2Texture = renderText(renderer, scoreFont, scoreTotal2Text, black);

    if (!titleTexture || !joueur1Texture || !joueur2Texture || !nextRoundTexture || !scoreTotal1Texture || !scoreTotal2Texture) {
        TTF_CloseFont(font);
        TTF_CloseFont(scoreFont);
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        TTF_Quit();
        IMG_Quit();
        SDL_Quit();
        exit(EXIT_FAILURE);
    }

    SDL_RenderClear(renderer);

    // Display background image
    SDL_Rect rectBackground = {0, 0, taillefenx, taillefeny};
    SDL_RenderCopy(renderer,Texturesscreentransition[0], NULL, &rectBackground);

    // Render title text higher up
    int width, height;
    SDL_QueryTexture(titleTexture, NULL, NULL, &width, &height);
    SDL_Rect titleRect = {taillefenx / 2 - width / 2, taillefeny / 10 - height / 2, width, height};
    SDL_RenderCopy(renderer, titleTexture, NULL, &titleRect);

    // Render "Joueur 1" and "Joueur 2" on the left
    SDL_QueryTexture(joueur1Texture, NULL, NULL, &width, &height);
    SDL_Rect joueur1Rect = {50, taillefeny / 4, width, height};
    SDL_RenderCopy(renderer, joueur1Texture, NULL, &joueur1Rect);

    SDL_QueryTexture(scoreTotal1Texture, NULL, NULL, &width, &height);
    SDL_Rect score1Rect = {joueur1Rect.x, joueur1Rect.y + joueur1Rect.h + 10, width, height};
    SDL_RenderCopy(renderer, scoreTotal1Texture, NULL, &score1Rect);

    SDL_QueryTexture(joueur2Texture, NULL, NULL, &width, &height);
    SDL_Rect joueur2Rect = {50, taillefeny / 4 + joueur1Rect.h + score1Rect.h + 70, width, height}; // Increased spacing
    SDL_RenderCopy(renderer, joueur2Texture, NULL, &joueur2Rect);

    SDL_QueryTexture(scoreTotal2Texture, NULL, NULL, &width, &height);
    SDL_Rect score2Rect = {joueur2Rect.x, joueur2Rect.y + joueur2Rect.h + 10, width, height};
    SDL_RenderCopy(renderer, scoreTotal2Texture, NULL, &score2Rect);

    // Render graduation bars for Joueur 1 and Joueur 2

    float barWidth = taillefenx * 0.7;
    int barHeight = 20;
    int majorTickHeight = 30; // Height of major ticks
    int minorTickHeight = 20; // Height of minor ticks
    int tickWidth = 3;  // Width of each tick
    int minorTickStep = barWidth / 8; // Distance between each minor tick
    int xOffset = joueur1Rect.x + joueur1Rect.w + 20;
    int yOffset1 = joueur1Rect.y + height / 4 +20;
    int yOffset2 = joueur2Rect.y + height / 4 +20;

    // Render red filled bar behind Joueur 1
    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255); // Red color for the background bar
    SDL_Rect redBar1 = {xOffset, yOffset1, (barWidth*(scoretotal1/20.0))/1, barHeight};
    SDL_RenderFillRect(renderer, &redBar1);

    // Render Joueur 1 bar
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_Rect bar1 = {xOffset, yOffset1, barWidth, barHeight};
    SDL_RenderDrawRect(renderer, &bar1);
    for (int i = 0; i <= 8; ++i) {
        int tickHeight = (i % 2 == 0) ? majorTickHeight : minorTickHeight;
        SDL_Rect tickRect = {xOffset + i * minorTickStep - tickWidth / 2, yOffset1 - tickHeight / 2 + barHeight / 2, tickWidth, tickHeight};
        SDL_RenderFillRect(renderer, &tickRect);
    }

    // Render red filled bar behind Joueur 2
    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255); // Red color for the background bar
    SDL_Rect redBar2 = {xOffset, yOffset2, (barWidth*(scoretotal2/20.0))/1, barHeight};
    SDL_RenderFillRect(renderer, &redBar2);

    // Render Joueur 2 bar
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_Rect bar2 = {xOffset, yOffset2, barWidth, barHeight};
    SDL_RenderDrawRect(renderer, &bar2);
    for (int i = 0; i <= 8; ++i) {
        int tickHeight = (i % 2 == 0) ? majorTickHeight : minorTickHeight;
        SDL_Rect tickRect = {xOffset + i * minorTickStep - tickWidth / 2, yOffset2 - tickHeight / 2 + barHeight / 2, tickWidth, tickHeight};
        SDL_RenderFillRect(renderer, &tickRect);
    }

    // Render "Manche Suivante" button in a blue rectangle at the bottom right
    SDL_QueryTexture(nextRoundTexture, NULL, NULL, &width, &height);
    SDL_Rect nextRoundRect = {taillefenx - width - 50, taillefeny - height - 50, width, height};

    nextroundRect->x = taillefenx - width - 60 ;
    nextroundRect->y = taillefeny - height - 60 ;
    nextroundRect->w = width + 20 ;
    nextroundRect->h =  height + 20 ;

    SDL_SetRenderDrawColor(renderer, 0, 0, 255, input == 1? 255 : 100); // Blue color
    SDL_RenderFillRect(renderer, nextroundRect);
    SDL_RenderCopy(renderer, nextRoundTexture, NULL, &nextRoundRect);

    SDL_RenderPresent(renderer);

    SDL_DestroyTexture(titleTexture);
    SDL_DestroyTexture(joueur1Texture);
    SDL_DestroyTexture(joueur2Texture);
    SDL_DestroyTexture(nextRoundTexture);
    SDL_DestroyTexture(scoreTotal2Texture);
    SDL_DestroyTexture(scoreTotal1Texture);



    TTF_CloseFont(font);
    TTF_CloseFont(scoreFont);

}

/* void endSDL(SDL_Window *window, SDL_Renderer *renderer, SDL_Texture *texture) {
    if (renderer) {
        SDL_DestroyRenderer(renderer);
    }
    if (window) {
        SDL_DestroyWindow(window);
    }

    SDL_DestroyTexture(texture);

    TTF_Quit();
    SDL_Quit();
}

void initSDL(SDL_Window **window, SDL_Renderer **renderer, SDL_Texture **texture) {
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
        endSDL(*window, *renderer, *texture);
    }

    SDL_DisplayMode dim;
    if (SDL_GetCurrentDisplayMode(0, &dim) != 0) {
        printf("SDL could not get display mode! SDL_Error: %s\n", SDL_GetError());
        endSDL(*window, *renderer, *texture);
    }

    float WINDOW_SCALE = 0.8;   // percentage of the window size relative to the screen

    int sizedevicex = dim.w;
    int sizedevicey = dim.h;

    int sizewindowx = sizedevicex * WINDOW_SCALE;
    int sizewindowy = sizedevicey * WINDOW_SCALE;

    *window = SDL_CreateWindow("Quarto Board", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, sizewindowx, sizewindowy, SDL_WINDOW_RESIZABLE);
    if (*window == NULL) {
        printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
        endSDL(*window, *renderer, *texture);
    }

    *renderer = SDL_CreateRenderer(*window, -1, SDL_RENDERER_ACCELERATED);
    if (*renderer == NULL) {
        printf("Renderer could not be created! SDL_Error: %s\n", SDL_GetError());
        endSDL(*window, *renderer, *texture);
    }

    SDL_SetRenderDrawBlendMode(*renderer, SDL_BLENDMODE_BLEND);

    if (TTF_Init() == -1) {
        printf("TTF_Init: %s\n", TTF_GetError());
        endSDL(*window, *renderer, *texture);
    }

    char* file_name = "sprite/mainmenu/1.png";

    *texture = loadTexture(file_name, *window, *renderer);
}



int isPointInRect(int x, int y, SDL_Rect rect) {
    return (x > rect.x) && (x < rect.x + rect.w) &&
           (y > rect.y) && (y < rect.y + rect.h);
} */

void processBetweenRoundInput(int *quit, SDL_Rect nextroundRect,int * input , Board * board , Player * player, int * state) {
    SDL_Event e;
    int x,y;

    while (SDL_PollEvent(&e) != 0) {
        if (e.type == SDL_QUIT) {
            *quit = 1;
        }
        if (e.type == SDL_KEYUP) {
            if (e.key.keysym.sym == SDLK_UP) {
                *quit = 1;
            }
        }
        if (e.type == SDL_MOUSEBUTTONDOWN) {
            SDL_GetMouseState(&x, &y);
            if (isPointInRect(x, y, nextroundRect)) {
                int score1 = player[0].globalscore;
                int score2 = player[1].globalscore;
                initGame(board, player);
                player[0].globalscore = score1;
                player[1].globalscore = score2;
                *state = STATE_GAMEPLAY;
            }
        }
        if(e.type ==SDL_MOUSEMOTION){
            SDL_GetMouseState(&x, &y);
            *input=0;
            if (isPointInRect(x, y, nextroundRect)) {
                *input = 1;
            }
        }
        
    }
}

/* int main() {
    SDL_Window* window = NULL;
    SDL_Renderer* renderer = NULL;
    SDL_Texture* texture = NULL;
    SDL_Rect nextroundRect={0};
    int input;

    initSDL(&window, &renderer, &texture);
    int quit = 0;
    int taillefenx = 0, taillefeny = 0;

    while (!quit) {
        SDL_GetWindowSize(window, &taillefenx, &taillefeny);
        processBetweenRoundInput(&quit,nextroundRect,&input);
        showBetweenRound(window, renderer, texture, taillefenx, taillefeny,&nextroundRect,input);
    }
    endSDL(window, renderer, texture);

    return 0;
}
 */