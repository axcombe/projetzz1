#ifndef STRUCTURE_H
#define STRUCTURE_H

enum Color {
    DARK_BLUE,
    LIGHT_BLUE,
    BLACK,
    YELLOW,
    GREEN,
    PURPLE,
    GREY,
    ORANGE,
    PINK,
    RED,
    WHITE,
};

enum Type{
    SHARK,
    SWIMMER,
    CRAB,
    BOAT,
    FISH,
    SHELL,
    OCTOPUS,
    PENGUIN,
    SAILOR,
    LIGHTHOUSE,// mult for boat
    SHOAL_OF_FISH,//mult for fish
    PENGUIN_COLONY,//mult for penguin
    CAPTAIN,//mult for sailor
    MERMAID,
};

enum STATE{
    STATE_MAIN_MENU,
    STATE_GAMEPLAY,
    STATE_SELECT_CARD,
    STATE_VIEW_PAIR,
    STATE_PAUSE_BETWEEN_ROUNDS,
    STATE_PAUSE_INGAME,
    STATE_END_SCREEN,
    STATE_END_SCREEN_MERMAID,
};

enum PlayerIntention{
    FIN,
    DISCARD1,
    DISCARD2,
    DECK,
    SELECTCARD1,
    SELECTCARD2,
    SELECTDISCARD1,
    SELECTDISCARD2,
    EYE,
    NONE = -1,
};

enum Where{
    WHAND,
    WDISCARD1,
    WDISCARD2,
    WDECK,
    WPAIR,
    WSELECT1,
    WSELECT2,
};

typedef struct{
    int color;
    int type;
    int numsprite;
}Card;

typedef struct{
    Card stack[58];
    int size;
} CardStack;

typedef struct{
    int isCurrent;
    int Source[2];//surement plus un enum non?
    int indexSource[2];
    int Destination[2];//de même 
    int indexDestination[2];
    Card cards[2];
    int frame;

}Animation;

typedef struct Node{
    int index;
    struct Node * next;
}Node;

typedef struct{
    CardStack content;
    Node quickAccess[14]; //c'est à free parce que malloc
    int nbcardpercolor[11];
}Hand;

typedef struct{
    int globalscore;
    int roundscore;
    int precroundscore;
    Hand hand;
    CardStack pair;
}Player;

typedef struct{
    CardStack deck;
    CardStack discard1;
    CardStack discard2;
    int selectcard;
    int havePair;
    int indexPair;
    Animation animate;

}Board;

//=============MCTS=================
enum NodeColor {
    TRED,
    TBLACK
};

enum BotIntention{
    DISCARD_0_0,
    DISCARD_0_1,
    DISCARD_1_0,
    DISCARD_1_1,
    DISCARD_0,
    DISCARD_1,
    END
};

typedef struct{
    Board board;
    Player player1;
    Player player2;
    int current_player; // 0 = bot, 1 = utilisateur
}State;

typedef struct{
    Player player1;
    CardStack pair1;
    int sizehand1;
    int sizedeck;
    CardStack discard1;
    CardStack discard2;
}View;

typedef struct{
    int number;
    int gain;
    int intention;
}Move;

typedef struct{
    Move moves[7];
    int number_move; // nombre de coups disponible
    int total_number; // nombre de coups joués au total
    State state;
}MCTSNode;

// arbre rouge noir
typedef struct Tree{
    struct Tree *link[2];
    int color;

    MCTSNode *node;
    unsigned long hash;
}Tree;

#endif 
