#ifndef BETWEENROUND_H
#define BETWEENROUND_H

void showBetweenRound (SDL_Window * window, SDL_Renderer * renderer, SDL_Texture* Texturesscreentransition[], int taillefenx, int taillefeny,SDL_Rect * nextroundRect,int input, Player * player);

void processBetweenRoundInput(int *quit, SDL_Rect nextroundRect,int * input,Board * board , Player * player,int * state);

#endif