#include <SDL2/SDL.h>

#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <stdlib.h>

#include "SDL_commun.h"


SDL_Texture* loadTexture(char *file_image_name, SDL_Window *window, SDL_Renderer *renderer) {
    SDL_Surface *my_image = IMG_Load(file_image_name);   // Load image into the surface
    if (my_image == NULL) {
        printf("Error %s\n", SDL_GetError());
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDL_Quit();
        exit(EXIT_FAILURE);
    }
    SDL_Texture* my_texture = SDL_CreateTextureFromSurface(renderer, my_image); // Load image from the surface to the texture
    SDL_FreeSurface(my_image);  // SDL_Surface is only used as a transition
    if (my_texture == NULL){
        printf("Error %s\n", SDL_GetError());
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDL_Quit();
        exit(EXIT_FAILURE);
    }
    return my_texture;
}


void initSDL(SDL_Window **window, SDL_Renderer **renderer, SDL_Texture* Texturesscreentransition[], SDL_Texture* texture_eye[], SDL_Texture** texture_Card, SDL_Texture** texture_Chest,SDL_Texture **texture_Back,SDL_Texture ** texture_end) {
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
        endSDL(window, renderer, Texturesscreentransition,texture_eye,texture_Card,texture_Chest,texture_Back,texture_end);
    }

    
    
    //dimension of the screen 
    SDL_DisplayMode dim;
    if (SDL_GetCurrentDisplayMode(0, &dim) != 0) {
        printf("SDL could not get display mode! SDL_Error: %s\n", SDL_GetError());
        endSDL(window, renderer, Texturesscreentransition,texture_eye,texture_Card,texture_Chest,texture_Back,texture_end);
    }

    float WINDOW_SCALE = 0.8;   // percentage of the window size relative to the screen

    int sizedevicex = dim.w;
    int sizedevicey = dim.h;

    int sizewindowx = sizedevicex * WINDOW_SCALE;
    int sizewindowy = sizedevicey * WINDOW_SCALE;

    //window creation
    *window = SDL_CreateWindow("sea, salt and paper", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, sizewindowx, sizewindowy, SDL_WINDOW_RESIZABLE);
    if (*window == NULL) {
        printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
        endSDL(window, renderer, Texturesscreentransition,texture_eye,texture_Card,texture_Chest,texture_Back,texture_end);
    }

    //renderer creation
    *renderer = SDL_CreateRenderer(*window, -1, SDL_RENDERER_ACCELERATED);
    if (*renderer == NULL) {
        printf("Renderer could not be created! SDL_Error: %s\n", SDL_GetError());
        endSDL(window, renderer, Texturesscreentransition,texture_eye,texture_Card,texture_Chest,texture_Back,texture_end);
    }

    // line for alpha be considerate
    SDL_SetRenderDrawBlendMode(*renderer,SDL_BLENDMODE_BLEND);

    //init TTF 
    if (TTF_Init() == -1) {
        printf("TTF_Init: %s\n", TTF_GetError());
        endSDL(window, renderer, Texturesscreentransition,texture_eye,texture_Card,texture_Chest,texture_Back,texture_end);
    }

    //load texture  bonne chance pour nos 200 texture
   char* file_names[] = {"sprite/mainmenu/1.png", "sprite/mainmenu/2.png", "sprite/mainmenu/3.png", "sprite/mainmenu/4.png", "sprite/mainmenu/5.png","sprite/mainmenu/plage1.png"};

    for (int i = 0; i < 6; ++i) {
        Texturesscreentransition[i] = loadTexture(file_names[i], *window, *renderer);
    }

    // Chargement texture de l'oeil
    texture_eye[0] = loadTexture("sprite/anim/oeil.png", *window, *renderer);
    texture_eye[1] = loadTexture("sprite/anim/oeilPink.png", *window, *renderer);

    // Chargement texture cartes

    *texture_Card = loadTexture("sprite/cards/spritesheet.png", *window, *renderer);

    *texture_Chest = loadTexture("sprite/anim/chest.png", *window, *renderer);

    *texture_Back = loadTexture("sprite/game_screen/lake.jpg", *window, *renderer);

    texture_end[0] = loadTexture("sprite/end_screen/loose.png", *window, *renderer);
    texture_end[1] = loadTexture("sprite/end_screen/night.png", *window, *renderer);
}

void freeTextures(SDL_Texture* Texturesscreentransition[], SDL_Texture* texture_Eye[], SDL_Texture* texture_Card, SDL_Texture* texture_Chest,SDL_Texture *texture_Back,SDL_Texture ** texture_end) {
    for (int i = 0; i < 6; ++i) {
        if (Texturesscreentransition[i] != NULL) {
            SDL_DestroyTexture(Texturesscreentransition[i]);
            Texturesscreentransition[i] = NULL;
        }
    }
    
    for (int i = 0; i < 2; ++i) {
        if (texture_Eye[i] != NULL) {
            SDL_DestroyTexture(texture_Eye[i]);
            texture_Eye[i] = NULL;
        }
    }

    if (texture_Card != NULL) {
        SDL_DestroyTexture(texture_Card);
        texture_Card = NULL;
    }

    if (texture_Chest != NULL) {
        SDL_DestroyTexture(texture_Chest);
        texture_Chest = NULL;
    }

    if (texture_Back != NULL) {
        SDL_DestroyTexture(texture_Back);
        texture_Back = NULL;
    }

    for (int i = 0; i < 2; ++i) {
        if (texture_end[i] != NULL) {
            SDL_DestroyTexture(texture_end[i]);
            texture_end[i] = NULL;
        }
    }
}

void endSDL(SDL_Window **window, SDL_Renderer **renderer, SDL_Texture* Texturesscreentransition[], SDL_Texture* texture_eye[], SDL_Texture** texture_Card, SDL_Texture** texture_Chest,SDL_Texture **texture_Back,SDL_Texture ** texture_end) {
    if (renderer) {
        SDL_DestroyRenderer(*renderer);
    }
    if (window) {
        SDL_DestroyWindow(*window);
    }
   
    freeTextures(Texturesscreentransition, texture_eye, *texture_Card, *texture_Chest,*texture_Back,texture_end);

    TTF_Quit();
    SDL_Quit();
}

SDL_Texture* renderText (SDL_Renderer *renderer, TTF_Font *font, char *text, SDL_Color color) {
    SDL_Surface *surface = TTF_RenderText_Blended(font, text, color);
    if (!surface) {
        printf("TTF_RenderText_Blended Error: %s\n", TTF_GetError());
        return NULL;
    }
    SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_FreeSurface(surface);
    if (!texture) {
        printf("SDL_CreateTextureFromSurface Error: %s\n", SDL_GetError());
    }
    return texture;
}

int isPointInRect(int x, int y, SDL_Rect rect) {
    return (x > rect.x) && (x < rect.x + rect.w) &&
           (y > rect.y) && (y < rect.y + rect.h);
}