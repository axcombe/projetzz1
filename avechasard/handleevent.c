#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include "structureSDL.h"
#include "structure.h"
#include "SDL_commun.h"

void handleGameInput(SDL_Window * window, Zone zone, int * quit, int * playerIntention, int * select ) {
    (void) window;
    SDL_Event event;
     int x, y;
    while (SDL_PollEvent(&event)) {
        switch (event.type) {
            case SDL_QUIT:
                *quit=1;  // Quitter le programme si on clique sur la croix de la fenêtre
                break;
            case SDL_MOUSEMOTION:
                SDL_GetMouseState(&x, &y);
                select[0]= -1;
                select[1]= -1;
                if (isPointInRect(x, y, zone.deck)) {
                   select[0]=Deck;
                }
                if (isPointInRect(x, y, zone.discard0)) {
                    select[0]=Discard0;
                }
                if (isPointInRect(x, y, zone.discard1)) {
                    select[0]=Discard1;
                }
                if (isPointInRect(x, y, zone.eye)) {
                    select[0]=Eye;
                }
                if (isPointInRect(x, y, zone.end)) {
                    select[0]=End;
                }
                for (int i=0 ; i < zone.counthand0 ; i++) {
                    if (isPointInRect(x, y, zone.handcard0[i])) {
                        select[0]=Hand0;
                        select[1]=i;
                    }
                }
                /* for (int i=0 ; i < zone.counthand1 ; i++) {
                    if (isPointInRect(x, y, zone.handcard1[i])) {
                        select[0]=Hand1;
                        select[1]=i;
                    }
                } */
                break;
            case SDL_MOUSEBUTTONDOWN:
                SDL_GetMouseState(&x, &y);
                if (isPointInRect(x, y, zone.deck)) {
                    *playerIntention = DECK;

                }
                if (isPointInRect(x, y, zone.discard0)) {
                    *playerIntention = DISCARD1;
                }
                if (isPointInRect(x, y, zone.discard1)) {
                    *playerIntention = DISCARD2;
                }
                if (isPointInRect(x, y, zone.end)) {
                    *playerIntention = FIN;
                }
                if (isPointInRect(x, y, zone.eye)) {
                    *playerIntention = EYE;
                }
                break;
        }
    }
}

void handleSelectCardInput(SDL_Window * window, Zone zone, int * quit, int * playerIntention, int * select ){
    (void) window;
    SDL_Event event;
     int x, y;

    while (SDL_PollEvent(&event)) {
        switch (event.type) {
            case SDL_QUIT:
                *quit=1;  // Quitter le programme si on clique sur la croix de la fenêtre
                break;
            case SDL_MOUSEMOTION:
                SDL_GetMouseState(&x, &y);
                select[0]= -1;
                if (isPointInRect(x, y, zone.selectedCard0)) {
                    select[0]=selectedCard0;
                }
                if (isPointInRect(x, y, zone.selectedCard1)) {
                    select[0]=selectedCard1;
                }
                if (isPointInRect(x, y, zone.discard0)) {
                    select[0]=Discard0;
                }
                if (isPointInRect(x, y, zone.discard1)) {
                    select[0]=Discard1;

                break;
            case SDL_MOUSEBUTTONDOWN:
                SDL_GetMouseState(&x, &y);


                if (isPointInRect(x, y, zone.selectedCard0)) {
                    *playerIntention = SELECTCARD1;
                    select[1]=selectedCard0;
                }
                if (isPointInRect(x, y, zone.selectedCard1)) {
                    *playerIntention = SELECTCARD2;
                    select[1]=selectedCard1;
                }
                if (isPointInRect(x, y, zone.discard0)) {
                    *playerIntention = SELECTDISCARD1;
                }
                if (isPointInRect(x, y, zone.discard1)) {
                    *playerIntention =SELECTDISCARD2;
                }
                

                break;
        }
    }
}
}

void handleViewPairInput (SDL_Window * window, Zone zone, int * quit, int * state, int * select ,int * playerintention){
    (void) window;
    SDL_Event event;
    int x, y;
    while (SDL_PollEvent(&event)) {
        switch (event.type) {
            case SDL_QUIT:
                *quit=1;  // Quitter le programme si on clique sur la croix de la fenêtre
                break;
            //case SDL_KEYUP:
            //    *quit = 1;
            //    break;
            case SDL_MOUSEMOTION:
                SDL_GetMouseState(&x, &y);
                select[0]= -1;
                select[1]= -1;
                if (isPointInRect(x, y, zone.eye)) {
                    select[0]=Eye;
                }
                break;
            case SDL_MOUSEBUTTONDOWN:
                SDL_GetMouseState(&x, &y);
                if (isPointInRect(x, y, zone.eye)) {
                    *state = STATE_GAMEPLAY;
                    *playerintention = -1;
                }
                break;
        }
    }
}