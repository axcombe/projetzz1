#ifndef ENDSCREEN_H
#define ENDSCREEN_H

#include "structure.h"
#include "structureSDL.h"




void showEndScreen (SDL_Window * window,SDL_Renderer * renderer,SDL_Texture * backgroundTex[2],int taillefenx,int taillefeny, SDL_Rect *playRect, SDL_Rect *quitRect,int input,float * offset, Player player[2],int multiplayer);

void processEndScreenInput(int * quit,SDL_Rect ReplayRect,SDL_Rect quitRect,int * input, Board * board,Player *player,int * state, int * currentPlayer);


#endif

