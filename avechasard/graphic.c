#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "graphic.h"
#include "structure.h"
#include "structureSDL.h"
#include "SDL_commun.h"
#include "logicingame.h"

// compil : gcc -o graphic graphic.c -lSDL2_image -lSDL2 -lSDL2_ttf -Wall -Wextra -Werror

/* SDL_Texture *loadTexture(char *file_image_name, SDL_Window *window, SDL_Renderer *renderer)
{
    SDL_Surface *my_image = IMG_Load(file_image_name); // Load image into the surface
    if (my_image == NULL)
    {
        printf("Error %s\n", SDL_GetError());
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDL_Quit();
        exit(EXIT_FAILURE);
    }
    SDL_Texture *my_texture = SDL_CreateTextureFromSurface(renderer, my_image); // Load image from the surface to the texture
    SDL_FreeSurface(my_image);                                                  // SDL_Surface is only used as a transition
    if (my_texture == NULL)
    {
        printf("Error %s\n", SDL_GetError());
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDL_Quit();
        exit(EXIT_FAILURE);
    }
    return my_texture;
}

void initSDL(SDL_Window **window, SDL_Renderer **renderer, SDL_Texture **textures_mainMenu, SDL_Texture **texture_Eye, SDL_Texture **texture_Card, SDL_Texture **texture_Chest,SDL_Texture **texture_Back)
{
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
        endSDL(*window, *renderer);
    }

    // dimension of the screen
    SDL_DisplayMode dim;
    if (SDL_GetCurrentDisplayMode(0, &dim) != 0)
    {
        printf("SDL could not get display mode! SDL_Error: %s\n", SDL_GetError());
        endSDL(*window, *renderer);
    }

    float WINDOW_SCALE = 0.8; // percentage of the window size relative to the screen

    int sizedevicex = dim.w;
    int sizedevicey = dim.h;

    int sizewindowx = sizedevicex * WINDOW_SCALE;
    int sizewindowy = sizedevicey * WINDOW_SCALE;

    // window creation
    *window = SDL_CreateWindow("Quarto Board", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, sizewindowx, sizewindowy, SDL_WINDOW_RESIZABLE);
    if (*window == NULL)
    {
        printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
        endSDL(*window, *renderer);
    }

    // renderer creation
    *renderer = SDL_CreateRenderer(*window, -1, SDL_RENDERER_ACCELERATED);
    if (*renderer == NULL)
    {
        printf("Renderer could not be created! SDL_Error: %s\n", SDL_GetError());
        endSDL(*window, *renderer);
    }

    // line for alpha be considerate
    SDL_SetRenderDrawBlendMode(*renderer, SDL_BLENDMODE_BLEND);

    // init TTF
    if (TTF_Init() == -1)
    {
        printf("TTF_Init: %s\n", TTF_GetError());
        endSDL(*window, *renderer);
    }

    // load texture  bonne chance pour nos 200 texture
    textures_mainMenu[0] = loadTexture("sprite/mainmenu/1.png", *window, *renderer);
    textures_mainMenu[1] = loadTexture("sprite/mainmenu/2.png", *window, *renderer);
    textures_mainMenu[2] = loadTexture("sprite/mainmenu/3.png", *window, *renderer);
    textures_mainMenu[3] = loadTexture("sprite/mainmenu/4.png", *window, *renderer);

    // Chargement texture de l'oeil
    texture_Eye[0] = loadTexture("sprite/anim/oeil.png", *window, *renderer);
    texture_Eye[1] = loadTexture("sprite/anim/oeilPink.png", *window, *renderer);

    // Chargement texture cartes

    *texture_Card = loadTexture("sprite/cards/spritesheet.png", *window, *renderer);

    *texture_Chest = loadTexture("sprite/anim/chest.png", *window, *renderer);

    *texture_Back = loadTexture("sprite/game_screen/lake.jpg", *window, *renderer);


}

void endSDL(SDL_Window *window, SDL_Renderer *renderer)
{
    if (renderer)
    {
        SDL_DestroyRenderer(renderer);
    }
    if (window)
    {
        SDL_DestroyWindow(window);
    }

    // free les textures???

    TTF_Quit();
    SDL_Quit();
} */

void drawRect(SDL_Renderer *renderer, int x, int y, int w, int h)
{
    SDL_Rect rectangle;

    rectangle.x = x;
    rectangle.y = y;
    rectangle.w = w;
    rectangle.h = h;

    SDL_RenderFillRect(renderer, &rectangle);
}

void getZoneSDL(Zone *zone, SDL_Window *window, Player player[2], int current_player, int multiplayer)
{

    int x, y, w, h, sizewindowx, sizewindowy;

    SDL_GetWindowSize(window, &sizewindowx, &sizewindowy);
    // Main 0
    x = (3.0 / 26.0) * sizewindowx;
    y = (11.0 / 14.0) * sizewindowy;
    w = (11.0 / 13.0) * sizewindowx;
    h = (3.0 / 14.0) * sizewindowy;
    SetRect(&zone->handzone0, x, y, w, h);

    // Main 1
    x = (3.0 / 26.0) * sizewindowx;
    y = 0;
    w = (11.0 / 13.0) * sizewindowx;
    h = (3.0 / 14.0) * sizewindowy;
    SetRect(&zone->handzone1, x, y, w, h);

    // Pairs 0
    x = (3.0 / 26.0) * sizewindowx;
    y = (2.0 / 7.0) * sizewindowy;
    w = (17.0 / 26.0) * sizewindowx;
    h = (11.0 / 56.0) * sizewindowy;
    SetRect(&zone->pair1, x, y, w, h);

    // Pairs 1
    x = (3.0 / 26.0) * sizewindowx;
    y = (29.0 / 56.0) * sizewindowy;
    w = (17.0 / 26.0) * sizewindowx;
    h = (11.0 / 56.0) * sizewindowy;
    SetRect(&zone->pair0, x, y, w, h);

    // Deck
    x = (41.0 / 52.0) * sizewindowx;
    y = (15.0 / 56.0) * sizewindowy;
    w = (9.0 / 52.0) * sizewindowx;
    h = (18.0 / 77.0) * sizewindowy;
    SetRect(&zone->deck, x, y, w, h);

    // Défausse 0
    x = (41.0 / 52.0) * sizewindowx;
    y = (29.0 / 56.0) * sizewindowy;
    w = (17.0 / 208.0) * sizewindowx;
    h = (3.0 / 14.0) * sizewindowy;
    SetRect(&zone->discard0, x, y, w, h);

    // Défausse 1
    x = (183.0 / 208.0) * sizewindowx;
    y = (29.0 / 56.0) * sizewindowy;
    w = (17.0 / 208.0) * sizewindowx;
    h = (3.0 / 14.0) * sizewindowy;
    SetRect(&zone->discard1, x, y, w, h);

    // Oeil
    x = (1.0 / 52.0) * sizewindowx;
    y = (3.0 / 7.0) * sizewindowy;
    w = (1.0 / 13.0) * sizewindowx;
    h = (1.0 / 7.0) * sizewindowy;
    SetRect(&zone->eye, x, y, w, h);
    // Fin
    x = (1.0 / 52.0) * sizewindowx;
    y = (23.0 / 28.0) * sizewindowy;
    w = (1.0 / 13.0) * sizewindowx;
    h = (1.0 / 7.0) * sizewindowy;
    SetRect(&zone->end, x, y, w, h);

    x = (2.0 / 7.0) * sizewindowx;
    y = (1.0 / 3.0) * sizewindowy;
    w = (1.0 / 7.0) * sizewindowx;
    h = (3.0 / 7.0) * sizewindowy;
    SetRect(&zone->selectedCard0, x, y, w, h);

    x = (4.0 / 7.0) * sizewindowx;
    y = (1.0 / 3.0) * sizewindowy;
    w = (1.0 / 7.0) * sizewindowx;
    h = (3.0 / 7.0) * sizewindowy;
    SetRect(&zone->selectedCard1, x, y, w, h);

    int nb_pair_0 = 0;
    int nb_pair_1 = 0;

    if (multiplayer == 1)
    {
        if (current_player == 1)
        {
            zone->counthand1 = player[0].hand.content.size; // player[0].hand.content.size;
            zone->counthand0 = player[1].hand.content.size; // player[1].hand.content.size;
            nb_pair_0 = player[1].pair.size;
            nb_pair_1 = player[0].pair.size;
        }
        else
        {
            zone->counthand0 = player[0].hand.content.size; // player[0].hand.content.size;
            zone->counthand1 = player[1].hand.content.size; // player[1].hand.content.size;
            nb_pair_0 = player[0].pair.size;
            nb_pair_1 = player[1].pair.size;
        }
    }
    else
    {
        zone->counthand0 = player[0].hand.content.size; // player[0].hand.content.size;
        zone->counthand1 = player[1].hand.content.size; // player[1].hand.content.size;
        nb_pair_0 = player[0].pair.size;
        nb_pair_1 = player[1].pair.size;
    }

    getHandZone(zone, zone->counthand0, zone->counthand1, sizewindowx, sizewindowy);

    getPairZone(zone);
    getBigPairZone(zone, nb_pair_0, nb_pair_1, sizewindowx, sizewindowy);
}

void SetRect(SDL_Rect *rect, int x, int y, int w, int h)
{
    rect->x = x;
    rect->y = y;
    rect->w = w;
    rect->h = h;
}

void SDL_displayEye(SDL_Renderer *Render, SDL_Rect eyeZone, int *sprite_eye, SDL_Texture **tex, int is_eye_over)
{
    SDL_Rect source = {0};

    source.h = 300;
    source.w = 300;
    source.y = 0;

    source.x = *sprite_eye * 300;

    if (is_eye_over == 1)
    {

        SDL_RenderCopy(Render, tex[1], &source, &eyeZone); // Préparation de l'affichage
    }
    else
    {
        SDL_RenderCopy(Render, tex[0], &source, &eyeZone); // Préparation de l'affichage
    }
}

void SDL_displayChest(SDL_Renderer *Render, SDL_Rect chestZone, SDL_Texture **tex, int is_chest_over, Player *player)
{
    SDL_Rect source = {0};

    source.h = 160;
    source.w = 153;
    source.y = 60;

    if (is_chest_over == 1)
    {

        if (player->roundscore >= 7)
        {
            source.x = 0;
            SDL_RenderCopy(Render, *tex, &source, &chestZone); // Préparation de l'affichage
        }
        else
        {
            source.x = 2 * 153;
            SDL_RenderCopy(Render, *tex, &source, &chestZone); // Préparation de l'affichage
        }
    }

    else
    {
        source.x = 153;
        SDL_RenderCopy(Render, *tex, &source, &chestZone); // Préparation de l'affichage
    }
}

void SDL_displayWhiteLayer(SDL_Renderer *renderer, SDL_Rect toDRect)
{

    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 90);

    SDL_RenderFillRect(renderer, &toDRect);
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 90);
}

void getHandZone(Zone *zone, int nb_card0, int nb_card1, int sizewindowx, int sizewindowy)
{
    int cardh = (3.0 / 14.0) * sizewindowy;
    int cardw = (8.0 / 11.0) * cardh;

    int cardy0 = (11.0 / 14.0) * sizewindowy;
    int cardy1 = 0;

    int handrightx = (25.0 / 26.0) * sizewindowx;
    int handleftx = (3.0 / 26.0) * sizewindowx;

    int handw = (11.0 / 13.0) * sizewindowx;

    int handcardstackw = 0;

    int handstackw = handw - cardw;

    if (nb_card0 > 0)
    {

        if (nb_card0 > 9)
        {

            handcardstackw = handstackw / (nb_card0 - 1);

            int margin = (handw - ((nb_card0 - 1) * handcardstackw + cardw));

            zone->handcard0[nb_card0 - 1].x = handrightx - cardw - (margin / 2);

            // printf("zone->handcard0[nb_card0 - 1].x = %d\n",zone->handcard0[nb_card0 - 1].x);
            zone->handcard0[nb_card0 - 1].y = cardy0;
            // printf("zone->handcard0[nb_card0 - 1].y = %d\n",zone->handcard0[nb_card0 - 1].y);

            zone->handcard0[nb_card0 - 1].w = cardw;
            // printf("zone->handcard0[nb_card0 - 1].w = %d\n",zone->handcard0[nb_card0 - 1].w);

            zone->handcard0[nb_card0 - 1].h = cardh;
            // printf("zone->handcard0[nb_card0 - 1].h = %d\n",zone->handcard0[nb_card0 - 1].h);

            for (int i = 0; i < nb_card0 - 1; i++)
            {
                zone->handcard0[i].x = handleftx + i * handcardstackw + (margin / 2);
                zone->handcard0[i].y = cardy0;
                zone->handcard0[i].w = handcardstackw;

                zone->handcard0[i].h = cardh;
            }
        }
        else
        {

            handcardstackw = (handw - (nb_card0 * cardw)) / (nb_card0 + 1);

            for (int i = 0; i < nb_card0; i++)
            {
                zone->handcard0[i].x = handleftx + i * cardw + (i + 1) * handcardstackw; // handleftx + i * handcardstackw;

                zone->handcard0[i].y = cardy0;
                zone->handcard0[i].w = cardw;

                zone->handcard0[i].h = cardh;
            }
        }
    }

    if (nb_card1 > 0)
    {

        if (nb_card1 > 9)
        {

            handcardstackw = handstackw / (nb_card1 - 1);

            int margin = (handw - ((nb_card1 - 1) * handcardstackw + cardw));

            zone->handcard1[nb_card1 - 1].x = handleftx + (margin / 2);
            zone->handcard1[nb_card1 - 1].y = cardy1;

            zone->handcard1[nb_card1 - 1].w = cardw;
            zone->handcard1[nb_card1 - 1].h = cardh;

            for (int i = 0; i < nb_card1 - 1; i++)
            {
                zone->handcard1[i].x = handleftx + i * handcardstackw + (margin / 2) + cardw;
                zone->handcard1[i].y = cardy1;
                zone->handcard1[i].w = handcardstackw;

                zone->handcard1[i].h = cardh;
            }
        }
        else
        {

            handcardstackw = (handw - (nb_card1 * cardw)) / (nb_card1 + 1);

            for (int i = 0; i < nb_card1; i++)
            {
                zone->handcard1[i].x = handleftx + i * cardw + (i + 1) * handcardstackw;
                zone->handcard1[i].y = cardy1;
                zone->handcard1[i].w = cardw;

                zone->handcard1[i].h = cardh;
            }
        }
    }
}

void getPairZone(Zone *zone)
{

    int pairw = zone->pair0.w / 4;
    int pairh = zone->pair0.h;

    int pairx = zone->pair0.x;
    int cardh = (3.0 / 4.0) * pairh;
    int cardw = (8.0 / 11.0) * cardh;

    // printf("cardh =%d, cardw=%d\n", cardh, cardw);

    int pairy0 = zone->pair0.y + (pairh - cardh) / 2;
    int pairy1 = zone->pair1.y + (pairh - cardh) / 2;

    for (int i = 0; i < 7; i += 2)
    {
        zone->pairzone0[i].x = ((i / 2) * pairw) + (pairw / 2) - (cardw / 2) + pairx;
        zone->pairzone0[i].y = pairy0;
        zone->pairzone0[i + 1].x = ((i / 2) * pairw) + (pairw / 2) + pairx;
        zone->pairzone0[i + 1].y = pairy0;

        zone->pairzone1[i].x = ((i / 2) * pairw) + (pairw / 2) - (cardw / 2) + pairx;
        zone->pairzone1[i].y = pairy1;
        zone->pairzone1[i + 1].x = ((i / 2) * pairw) + (pairw / 2) + pairx;
        zone->pairzone1[i + 1].y = pairy1;

        zone->pairzone0[i].w = cardw;
        zone->pairzone0[i + 1].w = cardw;

        zone->pairzone1[i].w = cardw;
        zone->pairzone1[i + 1].w = cardw;

        zone->pairzone0[i].h = cardh;
        zone->pairzone0[i + 1].h = cardh;

        zone->pairzone1[i].h = cardh;
        zone->pairzone1[i + 1].h = cardh;
    }
}

void getBigPairZone(Zone *zone, int nb_pair_0, int nb_pair_1, int sizewindowx, int sizewindowy)
{

    zone->bigPair0.x = zone->eye.w;
    zone->bigPair1.x = zone->eye.w;

    zone->bigPair1.y = (2*sizewindowy) / 9.0;
    zone->bigPair0.y = (5 * sizewindowy) / 9.0;

    zone->bigPair0.h = (2.0 * sizewindowy) / 9.0;
    zone->bigPair1.h = (2.0 * sizewindowy) / 9.0;

    zone->bigPair0.w = sizewindowx - zone->eye.x;
    zone->bigPair1.w = sizewindowx - zone->eye.x;

    for (int i = 0; i < nb_pair_0; i += 2)
    {
        if (nb_pair_0>5){
            zone->bigPaircard0[i].w = zone->bigPair0.w/(1.5*nb_pair_0);
            zone->bigPaircard0[i+1].w = zone->bigPair0.w/(1.5*nb_pair_0);

        }
        else{
            zone->bigPaircard0[i].w = zone->bigPair0.w/(1.5*8);
            zone->bigPaircard0[i+1].w = zone->bigPair0.w/(1.5*8);

        }
        zone->bigPaircard0[i].h = (11.0*zone->bigPaircard0[i].w)/8.0;

        zone->bigPaircard0[i+1].h = (11.0*zone->bigPaircard0[i+1].w)/8.0;

        zone->bigPaircard0[i].x = zone->bigPair0.x + (i * (zone->bigPair0.w / (nb_pair_0+1)));
        zone->bigPaircard0[i].y = zone->bigPair0.y;
        zone->bigPaircard0[i + 1].x = zone->bigPair0.x + (i * (zone->bigPair0.w / (nb_pair_0+1)))+(zone->bigPaircard0[i+1].w)/2; //+ largeur d'une carte /2 ;
        zone->bigPaircard0[i + 1].y = zone->bigPair0.y;
    }

    for (int i = 0; i < nb_pair_1; i += 2)
    {

        if (nb_pair_1>5){
            zone->bigPaircard1[i].w = zone->bigPair1.w/(1.5*nb_pair_1);
            zone->bigPaircard1[i+1].w = zone->bigPair1.w/(1.5*nb_pair_1);

        }
        else{
            zone->bigPaircard1[i].w = zone->bigPair1.w/(1.5*8);
            zone->bigPaircard1[i+1].w = zone->bigPair1.w/(1.5*8);

        }
        zone->bigPaircard1[i].h = (11.0*zone->bigPaircard1[i].w)/8.0;

        zone->bigPaircard1[i+1].h = (11.0*zone->bigPaircard1[i+1].w)/8.0;


        zone->bigPaircard1[i].x = zone->bigPair1.x + (i * (zone->bigPair1.w / (nb_pair_1+1)));
        zone->bigPaircard1[i].y = zone->bigPair1.y;
        zone->bigPaircard1[i + 1].x = zone->bigPair1.x + (i * (zone->bigPair1.w / (nb_pair_1+1)))+(zone->bigPaircard1[i+1].w)/2; //+ largeur d'une carte /2 ;
        zone->bigPaircard1[i + 1].y = zone->bigPair1.y;
    }
}

void animation(Zone *zone, SDL_Renderer *Render, Animation *anim, int *sprite_eye, SDL_Texture **texEye, int is_eye_over, SDL_Texture **texChest, int is_chest_over, Player *player)
{

    SDL_Rect eyeZone = zone->eye;
    SDL_Rect chestZone = zone->end;

    SDL_displayEye(Render, eyeZone, sprite_eye, texEye, is_eye_over);
    SDL_displayChest(Render, chestZone, texChest, is_chest_over, player);

    /*     for (int i=0; i<3; i++){//On libère le programme principal
            anim->numanimation[i]=-1;
        } */
}

void displayCard(SDL_Window *window, SDL_Texture **Texture, SDL_Renderer *Renderer, Card Card, SDL_Rect toDRect, SDL_RendererFlip flipx, int flipy, int to_hide, int size)
{

    SDL_Rect source = {0};
    SDL_Point center = {0};

    center.x = toDRect.w / 2;
    center.y = toDRect.h / 2;

    source.h = 552;
    source.y = 0;
    source.w = 400;

    // partie reponsable du flip du deck

    if (flipy == 1)
    {
        int temp = toDRect.h;
        toDRect.h = toDRect.w;
        toDRect.w = temp;
        toDRect.x -= toDRect.w;
        center.x = toDRect.w;
        center.y = 0;
        flipy = 3;
    }

    if (to_hide == 1)
    {

        int sizewindowx, sizewindowy;

        SDL_GetWindowSize(window, &sizewindowx, &sizewindowy);

        int cardh = (3.0 / 14.0) * sizewindowy;
        int cardw = (8.0 / 11.0) * cardh;

        int handw = (11.0 / 13.0) * sizewindowx;
        int handstackw = handw - cardw;

        float ratio = (float)handstackw / (size * cardw);
        source.w *= ratio;
    }

    if (Card.numsprite < 29)
    {
        source.x = Card.numsprite * 400;
    }
    else
    {
        source.x = (Card.numsprite - 29) * 400;
        source.y = 552;
    }

    SDL_RenderCopyEx(Renderer, *Texture, &source, &toDRect, flipy * 90, &center, flipx);
}

void displayStack(SDL_Window *window, SDL_Texture **Texture, SDL_Renderer *Renderer, CardStack Stack, SDL_Rect *toDRect, int flipy, int to_hide, int is_card_over[2])
{ // to hide indique si la stack forme une pile superposant plusieurs cartes

    if (Stack.stack[0].numsprite == 55)
    {
        for (int i = 0; i < Stack.size; i++)
        {
            if (i == Stack.size - 1)
            {
                to_hide = 0;
            }
            displayCard(window, Texture, Renderer, Stack.stack[0], toDRect[i], SDL_FLIP_NONE, flipy, to_hide, Stack.size);
            if (is_card_over[0] == Hand1 && is_card_over[1] == i)
            {
                SDL_displayWhiteLayer(Renderer, toDRect[i]);
            }
        }
    }
    else
    {
        for (int i = 0; i < Stack.size; i++)
        {
            if (i == Stack.size - 1)
            {
                to_hide = 0;
            }
            displayCard(window, Texture, Renderer, Stack.stack[i], toDRect[i], SDL_FLIP_NONE, flipy, to_hide, Stack.size);

            if (is_card_over[0] == Hand0 && is_card_over[1] == i)
            {
                SDL_displayWhiteLayer(Renderer, toDRect[i]);
            }
        }
    }
}

void displayPair(SDL_Window *window, SDL_Texture **Texture, SDL_Renderer *Renderer, CardStack Stack, SDL_Rect *toDRect)
{
    for (int i = 0; i < Stack.size; i++)
    {

        displayCard(window, Texture, Renderer, Stack.stack[i], toDRect[i], 0, 0, 0, 0);
    }
}

void displayBoardCard(SDL_Window *window, SDL_Texture **Texture, SDL_Renderer *Renderer, Player player1, Player player2, Board board_state, Zone ZoneSDL, int current_player, int multiplayer, int is_card_over[2], int is_deck_discard_over[3])
{

    Card dos;

    dos.numsprite = 55;

    CardStack dosStack;

    dosStack.stack[0] = dos;
    dosStack.size = 1;

    int to_hide1 = 0;
    int to_hide2 = 0;

    if (player1.hand.content.size > 9)
    {
        to_hide1 = 1;
    }
    if (player2.hand.content.size > 9)
    {
        to_hide2 = 1;
    }

    if (multiplayer == 1)
    {

        if (current_player == 0)
        {
            displayStack(window, Texture, Renderer, player1.hand.content, ZoneSDL.handcard0, 0, to_hide1, is_card_over);
            dosStack.size = player2.hand.content.size;
            displayStack(window, Texture, Renderer, dosStack, ZoneSDL.handcard1, 2, to_hide2, is_card_over);
            displayPair(window, Texture, Renderer, player1.pair, ZoneSDL.pairzone0);
            displayPair(window, Texture, Renderer, player2.pair, ZoneSDL.pairzone1);
        }
        else
        {
            dosStack.size = player1.hand.content.size;
            displayStack(window, Texture, Renderer, dosStack, ZoneSDL.handcard1, 2, to_hide1, is_card_over);
            displayPair(window, Texture, Renderer, player1.pair, ZoneSDL.pairzone1);
            displayStack(window, Texture, Renderer, player2.hand.content, ZoneSDL.handcard0, 0, to_hide2, is_card_over);
            displayPair(window, Texture, Renderer, player2.pair, ZoneSDL.pairzone0);
        }
    }
    else
    {
        displayStack(window, Texture, Renderer, player1.hand.content, ZoneSDL.handcard0, 0, to_hide1, is_card_over);
        dosStack.size = player2.hand.content.size;
        displayStack(window, Texture, Renderer, dosStack, ZoneSDL.handcard1, 2, to_hide2, is_card_over);
        displayPair(window, Texture, Renderer, player1.pair, ZoneSDL.pairzone0);
        displayPair(window, Texture, Renderer, player2.pair, ZoneSDL.pairzone1);
    }

    displayCard(window, Texture, Renderer, dos, ZoneSDL.deck, SDL_FLIP_NONE, 1, 0, 0); // Texture 55 est la texture du dos de carte
    if (is_deck_discard_over[0] == 1)
    {
        SDL_displayWhiteLayer(Renderer, ZoneSDL.deck);
    }

    if (board_state.discard1.size > 0)
    {
        displayCard(window, Texture, Renderer, board_state.discard1.stack[board_state.discard1.size-1], ZoneSDL.discard0, SDL_FLIP_NONE, 0, 0, 0); // board_state à la place des textures fixes
        if (is_deck_discard_over[1] == 1)
        {
            SDL_displayWhiteLayer(Renderer, ZoneSDL.discard0);
        }
    }
    else{
        SDL_displayWhiteLayer(Renderer, ZoneSDL.discard0);

    }
    if (board_state.discard2.size > 0)
    {

        displayCard(window, Texture, Renderer, board_state.discard2.stack[board_state.discard2.size-1], ZoneSDL.discard1, SDL_FLIP_NONE, 0, 0, 0);
        if (is_deck_discard_over[2] == 1)
        {
            SDL_displayWhiteLayer(Renderer, ZoneSDL.discard1);
        }
    }
    else{
        SDL_displayWhiteLayer(Renderer, ZoneSDL.discard1);

    }
}

void SDL_displayBigCardOver(SDL_Window *window, SDL_Renderer *renderer, SDL_Texture **texture, Card to_display_card)
{

    int modew,modeh;
    SDL_GetWindowSize(window, &modew, &modeh);

    SDL_Rect toDRect = {0};
    toDRect.w = (1.0 / 3.0) * modew;
    toDRect.h = (11.0 / 8.0) * toDRect.w;
    toDRect.x = (1.0 / 3.0) * modew;
    toDRect.y = (modeh - toDRect.h) / 3.0;

    SDL_Rect display = {0};
    display.w = modew;
    display.h = modeh;

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 90);
    SDL_RenderFillRect(renderer, &display);

    displayCard(window, texture, renderer, to_display_card, toDRect, SDL_FLIP_NONE, 0, 0, 0);
}

void SDL_displaySelectedCards(SDL_Window *window, SDL_Texture **texture, SDL_Renderer *renderer, SDL_Rect *toDRect, CardStack upperDeck)
{

    SDL_DisplayMode mode;
    SDL_GetCurrentDisplayMode(0, &mode);

    SDL_Rect display = {0};
    display.w = mode.w;
    display.h = mode.h;

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 150);
    SDL_RenderFillRect(renderer, &display);
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);

    if (upperDeck.size > 0)
    {
        displayCard(window, texture, renderer, upperDeck.stack[upperDeck.size - 1], toDRect[0], SDL_FLIP_NONE, 0, 0, 0);
    }
    if (upperDeck.size > 1)
    {
        displayCard(window, texture, renderer, upperDeck.stack[upperDeck.size - 2], toDRect[1], SDL_FLIP_NONE, 0, 0, 0);
    }
}

void SDL_displayBigPairs(SDL_Window *window, SDL_Texture **texture, SDL_Renderer *renderer, Zone zone, Player player[2], int current_player)
{
    int is_card_over [2]={0,0};

    SDL_DisplayMode mode;
    SDL_GetCurrentDisplayMode(0, &mode);

    SDL_Rect display = {0};
    display.w = mode.w;
    display.h = mode.h;

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 190);
    SDL_RenderFillRect(renderer, &display);
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);

    //VALABLE UNIQUEMENT SU CURRENT_PLAYER ALTERNE ENTRE 0 ET 1

    if (player[current_player].pair.size>0){
        displayStack(window,texture,renderer,player[current_player].pair, zone.bigPaircard0,0,0,is_card_over);
    }

    if (player[1- current_player].pair.size>0){
        displayStack(window,texture,renderer,player[1-current_player].pair, zone.bigPaircard1,0,0,is_card_over);

    }



}

void SDL_displayBackground(SDL_Window *window, SDL_Texture **texture, SDL_Renderer *renderer){

    SDL_DisplayMode mode;
    SDL_GetCurrentDisplayMode(0, &mode);

    SDL_Rect display = {0};
    display.w = mode.w;
    display.h = mode.h;

    SDL_RenderCopyEx(renderer, *texture, NULL, &display,0,NULL, 0);


}

void showGame(SDL_Window *window, Board board_state, Zone zone, Player *player, int current_player, int multiplayer, SDL_Renderer *renderer, Animation *anim,
              int *sprite_eye, float *crenelage, SDL_Texture **texEye, SDL_Texture **texture_Chest, SDL_Texture **texture_Card,SDL_Texture ** texture_Back, int anim_white[2])
{


//_______________________________________Ajout des textes__________________________________
    TTF_Font * scoreFont = TTF_OpenFont("font/fontcursive.ttf", 45);  
    TTF_Font * scoreFont2 = TTF_OpenFont("font/fontcursive.ttf", 35);  
    if (!scoreFont) {
        printf("Failed to load font: %s\n", TTF_GetError());

        SDL_DestroyWindow(window);
        TTF_Quit();
        IMG_Quit();
        SDL_Quit();
    }

    SDL_Color black = {0, 0, 0, 255};

    char buffer [17];

    //Fill buffer with end message
    if (current_player==0){
        snprintf(buffer, 17, "Joueur %d ", 1);
    }
    else{
        snprintf(buffer, 17, "Joueur %d", 2);
    }

    SDL_Texture *currentplayerTexture = renderText(renderer, scoreFont, buffer, black);


    char scoreTotal1Text[20];
    snprintf(scoreTotal1Text, sizeof(scoreTotal1Text), "Score joueur 1: %d", player[0].globalscore);
    SDL_Texture *scoreTotal1Texture = renderText(renderer, scoreFont2, scoreTotal1Text, black);

    char scoreTotal2Text[20];
    snprintf(scoreTotal2Text, sizeof(scoreTotal2Text), "Score joueur 2: %d", player[1].globalscore);
    SDL_Texture *scoreTotal2Texture = renderText(renderer, scoreFont2, scoreTotal2Text, black);

//__________________________________Ajout des textes

    SDL_RenderClear(renderer);

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_displayBackground(window,texture_Back,renderer);

    int is_eye_over = 0;
    int is_chest_over = 0;
    int is_deck_discard_over[3] = {0, 0, 0};

    switch (anim_white[0])
    {
    case Eye:
        is_eye_over = 1;
        break;

    case End:
        is_chest_over = 1;
        break;

    case Deck:
        is_deck_discard_over[0] = 1;
        break;
    case Discard0:
        is_deck_discard_over[1] = 1;
        break;
    case Discard1:
        is_deck_discard_over[2] = 1;
        break;

    default:
        break;
    }

    animation(&zone, renderer, anim, sprite_eye, texEye, is_eye_over, texture_Chest, is_chest_over, &player[current_player]);
    displayBoardCard(window, texture_Card, renderer, player[0], player[1], board_state, zone, current_player, multiplayer, anim_white, is_deck_discard_over);


    *crenelage += 1;
    if (*crenelage == 20.0)
    {
        *sprite_eye += 1;
        *sprite_eye %= 49;

        *crenelage = 0.0;
    }

    if (anim_white[0] == Hand0)
    {
        Card current_card_over = player[current_player].hand.content.stack[anim_white[1]];
        SDL_displayBigCardOver(window, renderer, texture_Card, current_card_over);
    }
    

    //_________________________________________ajout texte______________________

    int taillefenx=0,taillefeny=0;
    SDL_GetWindowSize(window, &taillefenx, &taillefeny);

    SDL_Rect currentplayerRect = zone.end;
    currentplayerRect.y = taillefeny - currentplayerRect.y ;

    
    int width,height;
    // Render current player
    SDL_QueryTexture(currentplayerTexture, NULL, NULL, &width, &height);
    currentplayerRect.w = width;
    currentplayerRect.h = height;

    SDL_RenderCopy(renderer, currentplayerTexture, NULL, &currentplayerRect);

    SDL_Rect scoreTotal1Rect = zone.end;
    scoreTotal1Rect.y = 1.05*taillefeny - scoreTotal1Rect.y ;

    SDL_QueryTexture(scoreTotal1Texture, NULL, NULL, &width, &height);
    scoreTotal1Rect.w = width;
    scoreTotal1Rect.h = height;
    SDL_RenderCopy(renderer, scoreTotal1Texture, NULL, &scoreTotal1Rect);



    SDL_Rect scoreTotal2Rect = zone.end;
    scoreTotal2Rect.y = 1.10*taillefeny - scoreTotal2Rect.y;

    SDL_QueryTexture(scoreTotal2Texture, NULL, NULL, &width, &height);
    scoreTotal2Rect.w = width;
    scoreTotal2Rect.h = height;
    SDL_RenderCopy(renderer, scoreTotal2Texture, NULL, &scoreTotal2Rect);





    //// Render score 1 button
    //SDL_QueryTexture(quitTexture, NULL, NULL, &width, &height);
    //SDL_RenderCopy(renderer, quitTexture, NULL, &score1Rect);
//
    //// Render score 2 button
    //SDL_QueryTexture(quitTexture, NULL, NULL, &width, &height);
    //SDL_RenderCopy(renderer, quitTexture, NULL, &score2Rect);

    SDL_DestroyTexture(currentplayerTexture);
    SDL_DestroyTexture(scoreTotal1Texture );
    SDL_DestroyTexture(scoreTotal2Texture );
    TTF_CloseFont(scoreFont);
    TTF_CloseFont(scoreFont2);
//__________________________________________ajout texte________________________

    // Rajout des layers blancs de survol ou animations alternatives
}

void showSelectCard(SDL_Window *window, Board board_state, Zone zone, Player *player, int current_player, int multiplayer, SDL_Renderer *renderer, Animation *anim,
                    int *sprite_eye, float *crenelage, SDL_Texture **texEye, SDL_Texture **texture_Chest, SDL_Texture **texture_Card,SDL_Texture ** texture_Back, int anim_white[2])
{
    // disable animations during showgame
    int anim_white0 = anim_white[0];
    anim_white[0] = -1;

    SDL_Rect toDRect_selected[2];
    toDRect_selected[0] = zone.selectedCard0;
    toDRect_selected[1] = zone.selectedCard1;

    showGame(window, board_state, zone, player, current_player, multiplayer, renderer, anim,
             sprite_eye, crenelage, texEye, texture_Chest, texture_Card,texture_Back, anim_white);

    // Enable animation during show selected cards
    anim_white[0] = anim_white0;
    // ajout d'un fond noir non opaque pour mise en valeur des cartes selectionnées
    // SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);

    // Ajout d'un bord à la carte à selectionner si survolée

    if (anim_white[1] == selectedCard0)
    {
        SDL_Rect cardedge = {0};
        cardedge.x = zone.selectedCard0.x - (0.1 * zone.selectedCard0.w);
        cardedge.y = zone.selectedCard0.y - (0.1 * zone.selectedCard0.h);
        cardedge.w = 1.20 * zone.selectedCard0.w;
        cardedge.h = 1.20 * zone.selectedCard0.h;
        SDL_SetRenderDrawColor(renderer, 0, 50, 255, 255);
        SDL_RenderFillRect(renderer, &cardedge);
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    }

    if (anim_white[1] == selectedCard1)
    {
        SDL_Rect cardedge = {0};
        cardedge.x = zone.selectedCard1.x - (0.1 * zone.selectedCard1.w);
        cardedge.y = zone.selectedCard1.y - (0.1 * zone.selectedCard1.h);
        cardedge.w = 1.20 * zone.selectedCard1.w;
        cardedge.h = 1.20 * zone.selectedCard1.h;
        SDL_SetRenderDrawColor(renderer, 0, 50, 255, 255);
        SDL_RenderFillRect(renderer, &cardedge);
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    }
    // affichage des cartes selectionnées

    SDL_displaySelectedCards(window, texture_Card, renderer, toDRect_selected, board_state.deck);

    // réaffichage des defausses en valeur
    if ((anim_white[0]) == Discard0)
        {
            SDL_Rect cardedge = {0};
            cardedge.x = zone.discard0.x - (0.1 * zone.discard0.w);
            cardedge.y = zone.discard0.y - (0.1 * zone.discard0.h);
            cardedge.w = 1.20 * zone.discard0.w;
            cardedge.h = 1.20 * zone.discard0.h;
            SDL_SetRenderDrawColor(renderer, 0, 50, 255, 255);
            SDL_RenderFillRect(renderer, &cardedge);
            SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        }
    if (board_state.discard1.size > 0)
    {
        displayCard(window, texture_Card, renderer, board_state.discard1.stack[board_state.discard1.size-1], zone.discard0, SDL_FLIP_NONE, 0, 0, 0); // board_state à la place des textures fixes
    }


    if ((anim_white[0]) == Discard1)
        {
            SDL_Rect cardedge = {0};
            cardedge.x = zone.discard1.x - (0.1 * zone.discard1.w);
            cardedge.y = zone.discard1.y - (0.1 * zone.discard1.h);
            cardedge.w = 1.20 * zone.discard1.w;
            cardedge.h = 1.20 * zone.discard1.h;
            SDL_SetRenderDrawColor(renderer, 0, 50, 255, 255);
            SDL_RenderFillRect(renderer, &cardedge);
            SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        }

    if (board_state.discard2.size > 0)
    {
        displayCard(window, texture_Card, renderer, board_state.discard2.stack[board_state.discard2.size-1], zone.discard1, SDL_FLIP_NONE, 0, 0, 0);
    }

}

void showViewPair(SDL_Window *window, Board board_state, Zone zone, Player *player, int current_player, int multiplayer, SDL_Renderer *renderer, Animation *anim,
                  int *sprite_eye, float *crenelage, SDL_Texture **texEye, SDL_Texture **texture_Chest, SDL_Texture **texture_Card,SDL_Texture ** texture_Back, int anim_white[2])
{

    // disable animations during showgame
    int anim_white0 = anim_white[0];
    anim_white[0] = -1;

    showGame(window, board_state, zone, player, current_player, multiplayer, renderer, anim,
             sprite_eye, crenelage, texEye, texture_Chest, texture_Card,texture_Back, anim_white);

    anim_white[0] = anim_white0;


    SDL_displayBigPairs(window,texture_Card,renderer,zone, player,current_player);
    SDL_displayEye(renderer, zone.eye, sprite_eye, texEye, (anim_white0 == Eye ? 1 : 0));

    
}


/*
int main()
{
    srand(time(NULL));
    

    int running = 1;
    int sprite_eye = 0;
    float crenelage = 0;
    int multiplayer = 1;
    int current_player = 0;

    int anim_white[2] = {2, -1};

    SDL_Window *window = NULL;
    SDL_Renderer *renderer = NULL;
    SDL_Texture *textures[4];
    textures[0] = NULL;
    textures[1] = NULL;
    textures[2] = NULL;
    textures[3] = NULL;

    SDL_Texture *texEye[2];
    texEye[0] = NULL;
    texEye[1] = NULL;

    SDL_Texture *texture_Card = NULL;

    SDL_Texture *texture_Chest = NULL;

    SDL_Texture *texture_Back = NULL;


    Zone zone;

    Card carte1;
    carte1.numsprite = 6;

    Player player[2];
    initPlayer(&player[0]);
    initPlayer(&player[1]);

    for (int i = 0; i < 14; i++)
    {
        player[0].hand.content.stack[i] = carte1;
        player[0].hand.content.size += 1;
    }

    for (int i = 0; i < 7; i++)
    {
        player[1].hand.content.stack[i] = carte1;
        player[1].hand.content.size += 1;
    }

    for (int i = 0; i < 8; i++)
    {
        player[0].pair.stack[i] = carte1;
        player[0].pair.size += 1;
        player[1].pair.stack[i] = carte1;
        player[1].pair.size += 1;
    }

    Animation *anim = NULL;

    Board board_state;

    initBoard(&board_state);

    


        printf("player[0].size = %d\n",player[0].hand.content.size );
        printf("player[1].size = %d\n",player[1].hand.content.size );

     
     

    initSDL(&window, &renderer, textures, texEye, &texture_Card, &texture_Chest,&texture_Back);

    int playerintention = -1;

    while (running)
    {
        getZoneSDL(&zone, window, player, current_player, multiplayer);

        handleSelectCardInput(window, zone, &running,  &playerintention,anim_white );

        //handleGameInput(window, zone, &running, &playerintention, anim_white);
        
        if (playerintention == DECK)
        {
            showSelectCard(window, board_state, zone, player, current_player, multiplayer, renderer, anim, &sprite_eye, &crenelage,
                           texEye, &texture_Chest, &texture_Card,&texture_Back, anim_white);
        }
        else if(playerintention  == EYE){        showViewPair(window, board_state, zone, player, current_player, multiplayer, renderer, anim, &sprite_eye, &crenelage,
                     texEye, &texture_Chest, &texture_Card,&texture_Back, anim_white);}
        else
        {
            showGame(window, board_state, zone, player, current_player, multiplayer, renderer, anim, &sprite_eye, &crenelage,
                     texEye, &texture_Chest, &texture_Card,&texture_Back ,anim_white);


        }
    }
    return(0);
}*/
/*
         showSelectCard(window, board_state, zone, player, current_player, multiplayer, renderer, anim, &sprite_eye, &crenelage,
                           texEye, &texture_Chest, &texture_Card,&texture_Back, anim_white);

        SDL_RenderPresent(renderer);

        SDL_Delay(16);
    }

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    TTF_Quit();
    SDL_Quit();
    return 0;
}*/