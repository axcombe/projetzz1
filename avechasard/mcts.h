#ifndef MCTS_H
#define MCTS_H

#include "structure.h"
#include "logicingame.h"

int compare_cards(const void *a, const void *b);
void sort_cardstack(CardStack *cs);
void copy_cardstack(CardStack *dest, CardStack *src);
unsigned long hash_card(Card *card);
unsigned long hash_cardstack(CardStack *cs);
unsigned long hash_node(Node *node);
unsigned long hash_hand(Hand *hand);
unsigned long hash_player(Player *p);
unsigned long hash_view(View *view);
Tree *createTree(MCTSNode *node) ;

void rbRotateRight(Tree ** root, Tree ** y, Tree ** papa, int son) ;
void rbRotateLeft(Tree ** root, Tree ** y, Tree ** papa, int son);
void rbRotate(Tree ** root, Tree ** x, Tree ** papa, int right, int son);
Tree * insertTree(Tree ** root, MCTSNode *node);
void treeDisplay(Tree *root, int depth);
void treeFree(Tree * root);
void incrMove(MCTSNode *node,int i);
Move *ucb(MCTSNode * node);
void getMoves(State *state, MCTSNode *node);
MCTSNode *createMCTSNode(State *state);
MCTSNode * getNode(State *state, Tree **course, Tree **root, int *created);
void botIntentionToPlayerIntention(int bot_intention,int player_intention[3]);
void getView(State *state, View * view);
int eval(State *state);
void displayMove(Move * move);
void displayMoves(MCTSNode *node);
int simulation(State state, int initial_player_intention[3]);
void unique_play(State *state,int player_intention[3]);
int isFinished(State *state, int player_intention[3]);
int cardMakePair(CardStack *card_stack,Card *card);
void createRandomState(State *state,State *random_state);
void copyState(State *state_initial, State *state_copy) ;
void copyMCTSNode(MCTSNode *node_initial, MCTSNode *node_copy);
int mcts(State *state,Tree **root);

void playBot(State *state, Tree **root, int nb_iter, int player_intention[3]);

void displayView(View *view);
#endif