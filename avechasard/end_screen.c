#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <stdlib.h>

#include "structure.h"
#include "structureSDL.h"
#include "logicingame.h"
#include "end_screen.h"
#include "SDL_commun.h"

// gcc end_screen.c -o end -Wall -Wextra -lSDL2 -lSDL2_ttf -lSDL2_image

/* 
int isPointInRect(int x, int y, SDL_Rect rect) {
    return (x > rect.x) && (x < rect.x + rect.w) &&
           (y > rect.y) && (y < rect.y + rect.h);
}


SDL_Texture* loadTexture(char  *  file_image_name, SDL_Window *window, SDL_Renderer *renderer ){
    SDL_Surface *my_image = NULL;
    SDL_Texture* my_texture = NULL;

    my_image = IMG_Load(file_image_name);
    if (my_image == NULL) {
        printf("Erreur  %s\n", SDL_GetError());
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDL_Quit();
        exit(EXIT_FAILURE);
    }
    my_texture = SDL_CreateTextureFromSurface(renderer, my_image);
    SDL_FreeSurface(my_image);
    if (my_texture == NULL){
        printf("Erreur  %s\n", SDL_GetError());
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDL_Quit();
        exit(EXIT_FAILURE);
    }

    return my_texture;
}

SDL_Texture* renderText(SDL_Renderer *renderer, TTF_Font *font, char *text, SDL_Color color) {
    SDL_Surface *surface = TTF_RenderText_Blended(font, text, color);
    if (!surface) {
        printf("TTF_RenderText_Blended Error: %s\n", TTF_GetError());
        return NULL;
    }
    SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_FreeSurface(surface);
    if (!texture) {
        printf("SDL_CreateTextureFromSurface Error: %s\n", SDL_GetError());
    }
    return texture;
} */


/* 
void initSDL(SDL_Window **window, SDL_Renderer **renderer, SDL_Texture * texture[2]) {
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());

    }

    SDL_DisplayMode dim;
    if (SDL_GetCurrentDisplayMode(0, &dim) != 0) {
        printf("SDL could not get display mode! SDL_Error: %s\n", SDL_GetError());

    }

    float WINDOW_SCALE = 0.8;   // percentage of the window size relative to the screen

    int sizedevicex = dim.w;
    int sizedevicey = dim.h;

    int sizewindowx = sizedevicex * WINDOW_SCALE;
    int sizewindowy = sizedevicey * WINDOW_SCALE;

    *window = SDL_CreateWindow("Quarto Board", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, sizewindowx, sizewindowy, SDL_WINDOW_RESIZABLE);
    if (*window == NULL) {
        printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
    }

    *renderer = SDL_CreateRenderer(*window, -1, SDL_RENDERER_ACCELERATED);
    if (*renderer == NULL) {
        printf("Renderer could not be created! SDL_Error: %s\n", SDL_GetError());
    }

    SDL_SetRenderDrawBlendMode(*renderer, SDL_BLENDMODE_BLEND);

    if (TTF_Init() == -1) {
        printf("TTF_Init: %s\n", TTF_GetError());
    }

    char* file_name = "sprite/end_screen/loose.png";
    char* file_name1 = "sprite/end_screen/night.png";


    texture[0] = loadTexture(file_name, *window, *renderer);
    texture[1] = loadTexture(file_name1, *window, *renderer);



} */

void showEndScreen (SDL_Window * window,SDL_Renderer * renderer,SDL_Texture * backgroundTex[2],int taillefenx,int taillefeny, SDL_Rect *playRect, SDL_Rect *quitRect,int input,float * offset, Player player[2],int multijoueur){
    (void) offset;
    TTF_Font *font = TTF_OpenFont("font/fontcursive.ttf", 64);  // Replace with your font path
    float scoretotal1 = (float) player[0].globalscore;
    float scoretotal2 = (float) player[1].globalscore;
    TTF_Font *scoreFont = TTF_OpenFont("font/fontcursive.ttf", 32);


    if (!font || !scoreFont) {
        printf("Failed to load font: %s\n", TTF_GetError());
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        TTF_Quit();
        IMG_Quit();
        SDL_Quit();
        exit(EXIT_FAILURE);
    }

    SDL_Color black = {0, 0, 0, 255};
    (void) black;
    SDL_Color white = {255, 255, 255, 255};


    int width, height;



    // Create text textures
    char buffer [50];
    //Fill buffer with end message
    
    if (player[0].hand.nbcardpercolor [10] == 4){
        snprintf(buffer, 50, "Joueur %d a gagne grace aux sirenes", 1);
    }
    else if (player[0].hand.nbcardpercolor [10] == 4){
        snprintf(buffer, 50, "Joueur %d a gagne grace aux sirenes", 2);
    }
    else if (multijoueur==0){
        if (player[0].globalscore>=player[1].globalscore){
            snprintf(buffer, 50, "Joueur %d a gagne", 1);
        }
        else if (player[0].globalscore<player[1].globalscore){
            snprintf(buffer, 50, "Joueur %d a perdu", 1);
        }
    }
    else{
        if (player[0].globalscore>=player[1].globalscore){
            snprintf(buffer, 50, "Joueur %d a gagne", 1);

        }
        else{
            snprintf(buffer, 50, "Joueur %d a gagne", 2);

        }
    }



    SDL_Texture *titleTexture = renderText(renderer, font, buffer, white);
    SDL_Texture *playTexture = renderText(renderer, font, "REJOUER", white);
    SDL_Texture *quitTexture = renderText(renderer, font, "QUITTER", white);
    SDL_Texture *joueur1Texture = renderText(renderer, font, "Joueur 1", white);
    SDL_Texture *joueur2Texture = renderText(renderer, font, "Joueur 2", white);


    char scoreTotal1Text[20];
    snprintf(scoreTotal1Text, sizeof(scoreTotal1Text), "Score: %d", (int) scoretotal1);
    SDL_Texture *scoreTotal1Texture = renderText(renderer, scoreFont, scoreTotal1Text, white);

    char scoreTotal2Text[20];
    snprintf(scoreTotal2Text, sizeof(scoreTotal2Text), "Score: %d", (int)scoretotal2);
    SDL_Texture *scoreTotal2Texture = renderText(renderer, scoreFont, scoreTotal2Text, white);


    if (!titleTexture || !joueur1Texture || !joueur2Texture || !playTexture || !scoreTotal1Texture || !scoreTotal2Texture) {
        TTF_CloseFont(font);
        TTF_CloseFont(scoreFont);
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        TTF_Quit();
        IMG_Quit();
        SDL_Quit();
        exit(EXIT_FAILURE);
    }


    SDL_RenderClear(renderer);


    if (multijoueur==0){
        if (player[0].globalscore>=player[1].globalscore){
            SDL_RenderCopy(renderer, backgroundTex[1], NULL, NULL);

        }
        else if (player[0].globalscore<player[1].globalscore){
            SDL_RenderCopy(renderer, backgroundTex[0], NULL, NULL);

        }
    }
    else{
            SDL_RenderCopy(renderer, backgroundTex[1], NULL, NULL);

        }
    


    // Render title text
    SDL_QueryTexture(titleTexture, NULL, NULL, &width, &height);
    SDL_Rect titleRect = {taillefenx / 2 - width / 2, taillefeny / 5 - height / 2, width, height};
    SDL_RenderCopy(renderer, titleTexture, NULL, &titleRect);

    // Render replay button
    SDL_QueryTexture(playTexture, NULL, NULL, &width, &height);
    *playRect = (SDL_Rect){50, taillefeny - height - 50, width, height};
    SDL_Rect playBgRect = {40, taillefeny - height - 60, width + 20, height + 20};
    SDL_SetRenderDrawColor(renderer, 0, 0, 255, input == 2? 255 : 100); // Blue color
    SDL_RenderFillRect(renderer, &playBgRect);
    SDL_RenderCopy(renderer, playTexture, NULL, playRect);

    // Render quit button
    SDL_QueryTexture(quitTexture, NULL, NULL, &width, &height);
    *quitRect = (SDL_Rect){taillefenx - width - 50, taillefeny - height - 50, width, height};
    SDL_Rect quitBgRect = {taillefenx - width - 60, taillefeny - height - 60, width + 20, height + 20};
    SDL_SetRenderDrawColor(renderer, 0, 0, 255, input == 1? 255 : 100); // Blue color
    SDL_RenderFillRect(renderer, &quitBgRect);
    SDL_RenderCopy(renderer, quitTexture, NULL, quitRect);


    // Render "Joueur 1" and "Joueur 2" on the left
    SDL_QueryTexture(joueur1Texture, NULL, NULL, &width, &height);
    SDL_Rect joueur1Rect = {50, taillefeny / 4, width, height};
    SDL_RenderCopy(renderer, joueur1Texture, NULL, &joueur1Rect);

    SDL_QueryTexture(scoreTotal1Texture, NULL, NULL, &width, &height);
    SDL_Rect score1Rect = {joueur1Rect.x, joueur1Rect.y + joueur1Rect.h + 10, width, height};
    SDL_RenderCopy(renderer, scoreTotal1Texture, NULL, &score1Rect);

    SDL_QueryTexture(joueur2Texture, NULL, NULL, &width, &height);
    SDL_Rect joueur2Rect = {50, taillefeny / 4 + joueur1Rect.h + score1Rect.h + 70, width, height}; // Increased spacing
    SDL_RenderCopy(renderer, joueur2Texture, NULL, &joueur2Rect);

    SDL_QueryTexture(scoreTotal2Texture, NULL, NULL, &width, &height);
    SDL_Rect score2Rect = {joueur2Rect.x, joueur2Rect.y + joueur2Rect.h + 10, width, height};
    SDL_RenderCopy(renderer, scoreTotal2Texture, NULL, &score2Rect);


    // Render graduation bars for Joueur 1 and Joueur 2

    float barWidth = taillefenx * 0.7;
    int barHeight = 20;
    int majorTickHeight = 30; // Height of major ticks
    int minorTickHeight = 20; // Height of minor ticks
    int tickWidth = 3;  // Width of each tick
    int minorTickStep = barWidth / 8; // Distance between each minor tick
    int xOffset = joueur1Rect.x + joueur1Rect.w + 20;
    int yOffset1 = joueur1Rect.y + height / 4 +20;
    int yOffset2 = joueur2Rect.y + height / 4 +20;

    // Render red filled bar behind Joueur 1
    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255); // Red color for the background bar
    SDL_Rect redBar1 = {xOffset, yOffset1, (barWidth*(scoretotal1/20.0))/1, barHeight};
    SDL_RenderFillRect(renderer, &redBar1);

    // Render Joueur 1 bar
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_Rect bar1 = {xOffset, yOffset1, barWidth, barHeight};
    SDL_RenderDrawRect(renderer, &bar1);
    for (int i = 0; i <= 8; ++i) {
        int tickHeight = (i % 2 == 0) ? majorTickHeight : minorTickHeight;
        SDL_Rect tickRect = {xOffset + i * minorTickStep - tickWidth / 2, yOffset1 - tickHeight / 2 + barHeight / 2, tickWidth, tickHeight};
        SDL_RenderFillRect(renderer, &tickRect);
    }

    // Render red filled bar behind Joueur 2
    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255); // Red color for the background bar
    SDL_Rect redBar2 = {xOffset, yOffset2, (barWidth*(scoretotal2/20.0))/1, barHeight};
    SDL_RenderFillRect(renderer, &redBar2);

    // Render Joueur 2 bar
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_Rect bar2 = {xOffset, yOffset2, barWidth, barHeight};
    SDL_RenderDrawRect(renderer, &bar2);
    for (int i = 0; i <= 8; ++i) {
        int tickHeight = (i % 2 == 0) ? majorTickHeight : minorTickHeight;
        SDL_Rect tickRect = {xOffset + i * minorTickStep - tickWidth / 2, yOffset2 - tickHeight / 2 + barHeight / 2, tickWidth, tickHeight};
        SDL_RenderFillRect(renderer, &tickRect);
    }

    SDL_RenderPresent(renderer);

    SDL_DestroyTexture(titleTexture);
    SDL_DestroyTexture(joueur1Texture);
    SDL_DestroyTexture(joueur2Texture);
    SDL_DestroyTexture(playTexture);
    SDL_DestroyTexture(scoreTotal2Texture);
    SDL_DestroyTexture(scoreTotal1Texture);

    SDL_DestroyTexture(quitTexture);
    TTF_CloseFont(font);
    TTF_CloseFont(scoreFont);
    
}

void processEndScreenInput(int * quit,SDL_Rect ReplayRect,SDL_Rect quitRect,int * input,Board * board,Player *player,int * state,int * currentPlayer){

    SDL_Event e;
    while (SDL_PollEvent(&e) != 0) {
        if (e.type == SDL_QUIT) {
            *quit = 1;
        }

        if (e.type ==SDL_MOUSEMOTION) {
            int x, y;
            SDL_GetMouseState(&x, &y);
            if (isPointInRect(x,y,quitRect)) {
                *input= 1;
            }
            else if (isPointInRect(x,y,ReplayRect)) {
                *input= 2;
            }
            else{
                *input=-1;
            }
        }
        if (e.type == SDL_MOUSEBUTTONDOWN) {
            int x, y;
            SDL_GetMouseState(&x, &y);
            if (x >= quitRect.x && x <= quitRect.x + quitRect.w &&
                y >= quitRect.y && y <= quitRect.y + quitRect.h) {
                *quit = 1;
            }
            if (x >= ReplayRect.x && x <= ReplayRect.x + ReplayRect.w &&
                y >= ReplayRect.y && y <= ReplayRect.y + ReplayRect.h) {
                initGame(board, player);
                *currentPlayer=0;
                *state = STATE_GAMEPLAY;
            }

            
        }
    }
}
/* 
int main() {
    
    SDL_Window* window = NULL;
    SDL_Renderer* renderer = NULL;
    SDL_Texture* textures[2];
    textures[0]=NULL;
    textures[1]=NULL;
    
    initSDL(&window,&renderer,textures);
    int quit = 0;
    int taillefenx=0,taillefeny=0;
    SDL_Rect playRect={0}, quitRect={0};
    int input = -1;
    float offset[2] = {0,0};

    int multiplayer =1;

    Player player[2];
    initPlayer(&player[0]);
    initPlayer(&player[1]);

    player[0].globalscore =12;
    player[1].globalscore =52;


    while (!quit) {

        SDL_GetWindowSize(window, &taillefenx, &taillefeny);
        processEndScreenInput(&quit,playRect,quitRect,&input);
        showEndScreen (window,renderer,textures, taillefenx, taillefeny,&playRect,&quitRect,input, offset, player, multiplayer);

    }

    return 0;
} */