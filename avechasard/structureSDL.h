#ifndef STRUCTURESDL_H
#define STRUCTURESDL_H

#include <stdio.h>
#include <SDL2/SDL.h>

typedef struct{
    SDL_Rect handzone0; // la ou ya les cartes
    SDL_Rect handzone1;
    SDL_Rect handcard0[30];
    SDL_Rect handcard1[30];
    int counthand0; // nombre de carte dans la main
    int counthand1;
    SDL_Rect pair0;
    SDL_Rect pair1;
    SDL_Rect pairzone0[8];
    SDL_Rect pairzone1[8];
    SDL_Rect deck;
    SDL_Rect discard0;
    SDL_Rect discard1;
    SDL_Rect eye;
    SDL_Rect end;
    SDL_Rect selectedCard0;
    SDL_Rect selectedCard1;
    SDL_Rect bigPair0;
    SDL_Rect bigPair1;
    SDL_Rect bigPaircard0[57];
    SDL_Rect bigPaircard1[57];


    
}Zone;

enum Effet{
    End,
    Eye,
    Discard0,
    Discard1,
    Deck,
    Hand0,
    Hand1,
    selectedCard0,
    selectedCard1,
};

#endif 