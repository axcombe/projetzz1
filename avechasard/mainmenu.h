#ifndef MAINMENU_H
#define MAINMENU_H

void showMainMenu (SDL_Window * window,SDL_Renderer * renderer,SDL_Texture ** textures,int taillefenx,int taillefeny, SDL_Rect *playRect, SDL_Rect *quitRect,int input,float * offset);
void processMainMenuInput(int * quit,SDL_Rect playRect,SDL_Rect quitRect,int * input,int * state);

#endif



