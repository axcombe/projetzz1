
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include "../structure.h"

void fillCard(card *card, int color, int type, int numsprite)
{

    card->color = color;
    card->type = type;
    card->numsprite = numsprite;
}

void copyCard(card sourceCard, card *targetCard)
{

    targetCard->color = sourceCard.color;
    targetCard->type = sourceCard.type;
    targetCard->numsprite = sourceCard.numsprite;
}

void displayCard(card card)
{

    printf("La couleur de la carte est %d,\n", card.color);
    printf("Le type de la carte est %d,\n", card.type);
    printf("Le numero de sprite de la carte est %d.\n", card.numsprite);
}

int areEqualCards(card sourceCard, card targetCard)
{ // renvoie 0 si les cartes ne sont pas égales
    return (sourceCard.color == targetCard.color && sourceCard.type == targetCard.type && sourceCard.numsprite == targetCard.numsprite);
}

void addScorePlayer(player *player, int add)
{
    player->globalscore += add;
    player->roundscore += add;
}

void addCardHandPlayer(player *player, card card)
{

    player->hand[player->nb_cards_hand] = card;
    player->nb_cards_hand += 1;
}

void deleteCardHandPlayer(player *player, card card)
{
    int is_delete = 0;
    int i = 0;
    while (is_delete == 0 || i < player->nb_cards_hand)
    {
        if (areEqualCards(player->hand[i], card) != 0)
        { // si les cartes sont égales

            copyCard(player->hand[player->nb_cards_hand], player->hand[i]); // on copie la derniere carte de la mains à l'emplacement de la carte à supprimer
            player->hand[player->nb_cards_hand] = NULL;                     // On supprime la derniere carte de la main
            player->nb_cards_hand -= 1;                                     // on diminue le nombre de cartes dans la main du joueur
            is_delete = 1;
        }
        else
        {
            i++
        }
    }
}

