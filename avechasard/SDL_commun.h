#ifndef SDLCOMMUN_H
#define SDLCOMMUN_H

SDL_Texture* loadTexture(char  *  file_image_name, SDL_Window *window, SDL_Renderer *renderer );

void initSDL(SDL_Window **window, SDL_Renderer **renderer, SDL_Texture* Texturesscreentransition[], SDL_Texture* texture_Eye[], SDL_Texture** texture_Card, SDL_Texture** texture_Chest,SDL_Texture **texture_Back,SDL_Texture ** texture_end);

void freeTextures(SDL_Texture* Texturescrantransition[], SDL_Texture* texture_Eye[], SDL_Texture* texture_Card, SDL_Texture* texture_Chest,SDL_Texture *texture_Back,SDL_Texture ** texture_end);

void endSDL(SDL_Window **window, SDL_Renderer **renderer, SDL_Texture* Texturesscreentransition[], SDL_Texture* texture_Eye[], SDL_Texture** texture_Card, SDL_Texture** texture_Chest,SDL_Texture **texture_Back,SDL_Texture ** texture_end);

SDL_Texture* renderText(SDL_Renderer *renderer, TTF_Font *font, char *text, SDL_Color color);

int isPointInRect(int x, int y, SDL_Rect rect);

#endif