#include <stdio.h>
#include <SDL2/SDL.h>
#include <stdlib.h>
#include <time.h>



void randomCouleur(int * r, int * g, int * b){
    *b = rand() % 256;
    *r = rand() % 256;
    *g = rand() % 256;
    
}

void degrader(int * color,int coordonee, int taillemax){
    
    *color=coordonee/(taillemax/256);
}

int min(int a, int b) {
    if (a < b) {
        return a;
    } else {
        return b;
    }
}

void clear(int nombre,SDL_Window* windows[],SDL_Renderer* renderers[]){
    for (int i = 0; i < nombre; ++i) {
        SDL_DestroyRenderer(renderers[i]);
        SDL_DestroyWindow(windows[i]);
    }
}
/*    
int f(int x){
    return -(x-500)(x-1500)+200;
}*/

//crée fentre et renderer
void creationWindow(int nombre,SDL_Window* fenetres[],SDL_Renderer* couleur[],char * titre,int L, int H,int taillex,int tailley, int nbfen){
    for (int i=0;i< nbfen;i++){
        
        //boucle pour les nom des fenêtres
        char title[50];
        sprintf(title, "%s %d",titre, i+1);

        int x = L+ taillex *i;
        int y = H -((x+90)-500)*((x+90)-1500)/1000;

        fenetres[i] = SDL_CreateWindow(title, x, y, taillex, tailley, 0);

        //si probleme
         if (!fenetres[i]) {
            printf("Erreur de création de la fenêtre %d: %s\n", i + 1, SDL_GetError());
            clear(i,fenetres,couleur);
            SDL_Quit();
            exit(EXIT_FAILURE);
        }

        couleur[i] = SDL_CreateRenderer(fenetres[i], -1, SDL_RENDERER_ACCELERATED);

        //si probleme
        if (!couleur[i]) {
            printf("Erreur de création du renderer pour la fenêtre %d: %s\n", i + 1, SDL_GetError());
            SDL_DestroyWindow(fenetres[i]);
            clear(i,fenetres,couleur);
            SDL_Quit();
            exit(EXIT_FAILURE);
        }

        
    }
}

void eye (SDL_Window* fenetres[],SDL_Renderer* couleur[],char * titre,int L, int H){
        
        int x1 = L+ 200;
        int x2 = L- 300;
        int y = H - H/2;

        fenetres[1] = SDL_CreateWindow(titre, x1, y, 100, 100, 0);
        fenetres[2] = SDL_CreateWindow(titre, x2, y, 100, 100, 0);


        //si probleme
         if (!fenetres[1]||!fenetres[2]) {
            for (int i=0;i<2;i++){
            printf("Erreur de création de la fenêtre %d: %s\n", i + 1, SDL_GetError());
            clear(i,fenetres,couleur);
            SDL_Quit();
            exit(EXIT_FAILURE);
            }
        }

        couleur[1] = SDL_CreateRenderer(fenetres[1], -1, SDL_RENDERER_ACCELERATED);
        couleur[2] = SDL_CreateRenderer(fenetres[2], -1, SDL_RENDERER_ACCELERATED);

        //si probleme
        if (!couleur[1]||!couleur[2]) {
            for (int i=0;i<2;i++){
            printf("Erreur de création du renderer pour la fenêtre %d: %s\n", i + 1, SDL_GetError());
            SDL_DestroyWindow(fenetres[i]);
            clear(i,fenetres,couleur);
            SDL_Quit();
            exit(EXIT_FAILURE);
            }
        }

        
    }



void setcouleur(int nombre,SDL_Renderer * couleur[],SDL_Window* fenetres[],int H,int W,int r,int g,int b,int rgb){
    for (int i=0;i< nombre;i++){
       
        int w=0,h=0;
        SDL_GetWindowPosition(fenetres[i], &w, &h);
        switch (rgb) {
            case 0:
                degrader(&g,w,W);
                break;
            case 1:
               degrader(&r,w,W);
                break;
            case 2:
                degrader(&b,w,W);
                break;
            default:
                printf("Unexpected value for rgb\n");
                break;
            }
        

        // set up le render
        SDL_SetRenderDrawColor(couleur[i], r, g, b, 255);
        SDL_RenderClear(couleur[i]);
        SDL_RenderPresent(couleur[i]);
    }
}




int main(){
    //SDL_init
    if(SDL_Init(SDL_INIT_VIDEO)!=0){
        fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError()); 
        return EXIT_FAILURE; 
    }
    //init le temps pour la couleur aléatoire
    srand(time(NULL));

    //On récupère les dimensions de l'écran
    SDL_DisplayMode mode;
    
    SDL_GetCurrentDisplayMode(0,&mode);
    


    int milieuX=mode.w/2;
    int milieuY=mode.h/2;
    int taillexfenetre=100;
    int tailleyfenetre=100;
    
    /*
    int nbrefenetre =min((mode.h-100)/tailleyfenetre,(mode.w-100)/taillexfenetre);
    printf("%d %d\n",(mode.h-100)/taillexfenetre,(mode.w-100)/tailleyfenetre);

    if (nbrefenetre%2==0){
        nbrefenetre--;
    }*/
    int nbrefenetre=13;
    SDL_Window* windows[nbrefenetre];
    SDL_Renderer* renderers[nbrefenetre];
    SDL_Window* windows2[2];
    SDL_Renderer* renderers2[2];


    int baseX = milieuX - taillexfenetre * (nbrefenetre / 2) - taillexfenetre/2;
    int baseY = milieuY ;

    printf("milieu x %d, basex %d, milieuy %d,basey %d\n",milieuX,baseX,milieuY,baseY);

    int  r, g, b; 
    randomCouleur(&r, &g, &b);
    int rgb=rand()%3;

    creationWindow(nbrefenetre, windows, renderers, "(", baseX, baseY, taillexfenetre, tailleyfenetre, nbrefenetre);
    setcouleur(nbrefenetre, renderers,windows,mode.h,mode.w,r,g,b,rgb);
    eye(windows2,renderers2,"eye",milieuX,milieuY);
   // creationWindow(nbrefenetre, windows2, renderers2, "/", baseX, baseY, taillexfenetre, tailleyfenetre, 0);
    //setcouleur(nbrefenetre, renderers2,windows2,mode.h,mode.w,r,g,b,axe,rgb);
    

    SDL_Delay(5000);
    //on nettoie
    clear(nbrefenetre,windows,renderers);
   // clear(nbrefenetre,windows2,renderers2);
    

    SDL_Quit();
    return 0;
}