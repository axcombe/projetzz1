#include <stdio.h>
#include <SDL2/SDL.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>

int running = 1;
//je comprends pas mais j'en ai besoin pour fermer mes fenetres sans kill violent
void* monitor_input() {
    char input[4];
    while (running) {
        fgets(input, 4, stdin);
        if (atoi(input) == 999) {
            running = 0;
        }
    }
    return NULL;
}

void contour(SDL_Window *fen[], SDL_Renderer *couleur[], int ox, int oy, int tx, int ty) {
    int x[8] = {ox-2.5*tx, ox-1.5*tx, ox-0.5*tx, ox+0.5*tx, ox+1.5*tx, ox+0.5*tx, ox-0.5*tx, ox-1.5*tx};
    int y[8] = {oy-2*ty, oy-3*ty, oy-3.5*ty, oy-3*ty, oy-2*ty, oy+2*ty, oy+2.5*ty, oy+2*ty};
    int tailley[8] = {4*ty, ty, ty, ty, 4*ty, ty, ty, ty};
    int taillex[8] = {tx, tx, tx, tx, tx, tx, tx, tx};

    for (int i = 0; i < 8; i++) { 
        char title[50];
        sprintf(title, "%d", i + 1);   
        fen[i] = SDL_CreateWindow(title, x[i], y[i], taillex[i], tailley[i], SDL_WINDOW_RESIZABLE);
        if (!fen[i]) {
            printf("Erreur de création de la fenêtre %d: %s\n", i + 1, SDL_GetError());
            SDL_Quit();
            exit(EXIT_FAILURE);
        }
        couleur[i] = SDL_CreateRenderer(fen[i], -1, SDL_RENDERER_ACCELERATED);
        if (!couleur[i]) {
            printf("Erreur de création du renderer pour la fenêtre %d: %s\n", i + 1, SDL_GetError());
            SDL_DestroyWindow(fen[i]);
            SDL_Quit();
            exit(EXIT_FAILURE);
        }
    }
}

void deplacer(SDL_Window *pupille, int *x, int *y, int *dirx, int *diry, int Mx, int My, int taillex, int tailley, int w, int h) {
    *x += (*dirx);
    *y += (*diry);

    if (*x <= Mx - taillex || *x + w >= Mx + taillex) {
        (*dirx) = -(*dirx);
    }

    if (*y <= My - tailley || *y + h >= My + tailley) {
        (*diry) = -(*diry);
    }

    SDL_SetWindowPosition(pupille, *x, *y);
}

void redimensionner(SDL_Window *pupille, int *w, int *h, int *grandir, int taillex, int tailley) {
    if ((*grandir) == 0) {
        *w = (int)(*w * 0.9);
        *h = (int)(*h * 0.9);
    } else {
        *w = (int)(*w * 1.1);
        *h = (int)(*h * 1.1);
    }

    if (*w > taillex || *h > tailley) {
        (*grandir) = 0;
    } else if (*w <= taillex / 4 || *h <= tailley / 4) {
        (*grandir) = 1;
    }

    if (*w > taillex) *w = taillex;
    if (*h > tailley) *h = tailley;

    SDL_SetWindowSize(pupille, *w, *h);
}

int main() {
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError());
        return EXIT_FAILURE;
    }

    SDL_DisplayMode dim;
    SDL_GetCurrentDisplayMode(0, &dim);
    
    int milieux = dim.w / 2;
    int milieuy = dim.h / 2;
    int tailleyfenetre = dim.h * 3.0 / 4.0 / 8.0;
    int taillexfenetre = tailleyfenetre;

    SDL_Window *fen[16];
    SDL_Renderer *renderer[16];

    contour(fen, renderer, milieux - milieux / 3, milieuy, taillexfenetre, tailleyfenetre);

    contour(&fen[8], &renderer[8], milieux + milieux / 3, milieuy, taillexfenetre, tailleyfenetre);

    SDL_Window *centre1, *centre2;
    SDL_Renderer *couleurcentre1, *couleurcentre2;

    int cx1 = milieux - milieux / 3;
    int cy1 = milieuy;
    int cx2 = milieux + milieux / 3;
    int cy2 = milieuy;

    int w1 = taillexfenetre / 4;
    int h1 = tailleyfenetre / 4;
    int w2 = taillexfenetre / 4;
    int h2 = tailleyfenetre / 4;

    centre1 = SDL_CreateWindow("pupille1", cx1, cy1, w1, h1, SDL_WINDOW_RESIZABLE);
    centre2 = SDL_CreateWindow("pupille2", cx2, cy2, w2, h2, SDL_WINDOW_RESIZABLE);

    if (!centre1 || !centre2) {
        printf("Erreur de création des fenêtres pupille: %s\n", SDL_GetError());
        for (int i = 0; i < 16; i++) {
            SDL_DestroyWindow(fen[i]);
            SDL_DestroyRenderer(renderer[i]);
        }
        if (centre1) SDL_DestroyWindow(centre1);
        if (centre2) SDL_DestroyWindow(centre2);
        SDL_Quit();
        return EXIT_FAILURE;
    }

    couleurcentre1 = SDL_CreateRenderer(centre1, -1, SDL_RENDERER_ACCELERATED);
    couleurcentre2 = SDL_CreateRenderer(centre2, -1, SDL_RENDERER_ACCELERATED);

    if (!couleurcentre1 || !couleurcentre2) {
        printf("Erreur de création des renderers pour les pupilles: %s\n", SDL_GetError());
        SDL_DestroyWindow(centre1);
        SDL_DestroyWindow(centre2);
        for (int i = 0; i < 16; i++) {
            SDL_DestroyWindow(fen[i]);
            SDL_DestroyRenderer(renderer[i]);
        }
        SDL_Quit();
        return EXIT_FAILURE;
    }

    int dirx1 = -5, diry1 = -4;
    int dirx2 = 5, diry2 = 4;
    int grandir1 = 1, grandir2 = 1;
//pas compris
    pthread_t input_thread;
    if (pthread_create(&input_thread, NULL, monitor_input, NULL) != 0) {
        fprintf(stderr, "Erreur de création du thread d'entrée utilisateur\n");
        SDL_DestroyWindow(centre1);
        SDL_DestroyRenderer(couleurcentre1);
        SDL_DestroyWindow(centre2);
        SDL_DestroyRenderer(couleurcentre2);
        for (int i = 0; i < 16; i++) {
            SDL_DestroyWindow(fen[i]);
            SDL_DestroyRenderer(renderer[i]);
        }
        SDL_Quit();
        return EXIT_FAILURE;
    }

    while (running) {
        deplacer(centre1, &cx1, &cy1, &dirx1, &diry1, milieux - milieux / 2, milieuy, taillexfenetre, tailleyfenetre, w1, h1);
        deplacer(centre2, &cx2, &cy2, &dirx2, &diry2, milieux + milieux / 2, milieuy, taillexfenetre, tailleyfenetre, w2, h2);
        redimensionner(centre1, &w1, &h1, &grandir1, taillexfenetre, tailleyfenetre);
        redimensionner(centre2, &w2, &h2, &grandir2, taillexfenetre, tailleyfenetre);
        SDL_Delay(10);
    }
//pas compris
    pthread_join(input_thread, NULL);

    for (int i = 15; i >= 0; i--) {
        SDL_DestroyWindow(fen[i]);
        SDL_DestroyRenderer(renderer[i]);
    }
    SDL_DestroyWindow(centre1);
    SDL_DestroyRenderer(couleurcentre1);
    SDL_DestroyWindow(centre2);
    SDL_DestroyRenderer(couleurcentre2);
    SDL_Quit();
    return 0;
}
