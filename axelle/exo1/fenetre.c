#include <stdio.h>
#include <SDL2/SDL.h>
#include <stdlib.h>
#include <time.h>



void randomCouleur(int * r, int * g, int * b){
    *b = rand() % 256;
    *r = rand() % 256;
    *g = rand() % 256;
    
}

void degrader(int * color,int coordonee, int taillemax){
    
    *color=coordonee/(taillemax/256);
}

int min(int a, int b) {
    if (a < b) {
        return a;
    } else {
        return b;
    }
}

void clear(int nombre,SDL_Window* windows[],SDL_Renderer* renderers[]){
    for (int i = 0; i < nombre; ++i) {
        SDL_DestroyRenderer(renderers[i]);
        SDL_DestroyWindow(windows[i]);
    }
}
    

//crée fentre et renderer
void creationWindow(int nombre,SDL_Window* fenetres[],SDL_Renderer* couleur[],char * titre,int L, int H,int taillex,int tailley, int monte){
    for (int i=0;i< nombre;i++){
        
        //boucle pour les nom des fenêtres
        char title[50];
        sprintf(title, "%s %d",titre, i + 1);

        int x = L+ taillex *(monte ? i: (nombre-i-1));
        int y = H + tailley*i;

        fenetres[i] = SDL_CreateWindow(title, x, y, taillex, tailley, 0);

        //si probleme
         if (!fenetres[i]) {
            printf("Erreur de création de la fenêtre %d: %s\n", i + 1, SDL_GetError());
            clear(i,fenetres,couleur);
            SDL_Quit();
            exit(EXIT_FAILURE);
        }

        couleur[i] = SDL_CreateRenderer(fenetres[i], -1, SDL_RENDERER_ACCELERATED);

        //si probleme
        if (!couleur[i]) {
            printf("Erreur de création du renderer pour la fenêtre %d: %s\n", i + 1, SDL_GetError());
            SDL_DestroyWindow(fenetres[i]);
            clear(i,fenetres,couleur);
            SDL_Quit();
            exit(EXIT_FAILURE);
        }
    }
}


void setcouleur(int nombre,SDL_Renderer * couleur[],SDL_Window* fenetres[],int H,int W,int r,int g,int b,int axe,int rgb){
    for (int i=0;i< nombre;i++){
       
        int w=0,h=0;
        SDL_GetWindowPosition(fenetres[i], &w, &h);
        
        if (axe) {
            switch (rgb) {
            case 0:
                degrader(&g,h,H);
                break;
            case 1:
               degrader(&r,h,H);
                break;
            case 2:
                degrader(&b,h,H);
                break;
            default:
                printf("Unexpected value for rgb\n");
                break;
            }
             
        }
        else{
              switch (rgb) {
            case 0:
                degrader(&g,w,W);
                break;
            case 1:
               degrader(&r,w,W);
                break;
            case 2:
                degrader(&b,w,W);
                break;
            default:
                printf("Unexpected value for rgb\n");
                break;
            }
        }
       
        

        // set up le render
        SDL_SetRenderDrawColor(couleur[i], r, g, b, 255);
        SDL_RenderClear(couleur[i]);
        SDL_RenderPresent(couleur[i]);
    }
}




int main(){
    //SDL_init
    if(SDL_Init(SDL_INIT_VIDEO)!=0){
        fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError()); 
        return EXIT_FAILURE; 
    }
    //init le temps pour la couleur aléatoire
    srand(time(NULL));

    //On récupère les dimensions de l'écran
    SDL_DisplayMode mode;
    
    SDL_GetCurrentDisplayMode(0,&mode);
    
    int milieuX=mode.w/2;
    int milieuY=mode.h/2;
    int taillexfenetre=100;
    int tailleyfenetre=100;
    
    
    int nbrefenetre =min((mode.h-100)/tailleyfenetre,(mode.w-100)/taillexfenetre);

    if (nbrefenetre%2==0){
        nbrefenetre--;
    }
    SDL_Window* windows[nbrefenetre];
    SDL_Renderer* renderers[nbrefenetre];
    SDL_Window* windows2[nbrefenetre];
    SDL_Renderer* renderers2[nbrefenetre];


    int baseX = milieuX - taillexfenetre * (nbrefenetre / 2) - taillexfenetre/2;
    int baseY = milieuY - tailleyfenetre * (nbrefenetre / 2) - tailleyfenetre/2 ;

    printf("milieu x %d, basex %d, milieuy %d,basey %d\n",milieuX,baseX,milieuY,baseY);

    int  r, g, b; 
    randomCouleur(&r, &g, &b);
    int axe=rand()%2;
    int rgb=rand()%3;

    creationWindow(nbrefenetre, windows, renderers, "\\", baseX, baseY, taillexfenetre, tailleyfenetre, 1);
    setcouleur(nbrefenetre, renderers,windows,mode.h,mode.w,r,g,b,axe,rgb);
    creationWindow(nbrefenetre, windows2, renderers2, "/", baseX, baseY, taillexfenetre, tailleyfenetre, 0);
    setcouleur(nbrefenetre, renderers2,windows2,mode.h,mode.w,r,g,b,axe,rgb);
    

    SDL_Delay(5000);
    //on nettoie
    clear(nbrefenetre,windows,renderers);
    clear(nbrefenetre,windows2,renderers2);
    

    SDL_Quit();
    retutn 0;
}