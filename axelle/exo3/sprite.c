#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>


enum direction {
	H = 0,
	B = 4,
	G = 6,
    D = 2
};


void initrect(SDL_Rect avancer[8][3]){
    for (int i=0;i<8;i++){
        SDL_Rect rect = {16*i, 24*9,16, 24 };
        SDL_Rect rect1 = {16*i, 24*10,16, 24 };
        SDL_Rect rect2 = {16*i, 24*11,16, 24 };
        avancer[i][0] = rect ;
        avancer[i][1] = rect1 ;
        avancer[i][2] = rect2 ;
    }
}

SDL_Texture* loadTexture(char  *  file_image_name, SDL_Window *window, SDL_Renderer *renderer ){
    SDL_Surface *my_image = NULL;           // Variable de passage
    SDL_Texture* my_texture = NULL;         // La texture

    my_image = IMG_Load(file_image_name);   // Chargement de l'image dans la surface
                                            // image=SDL_LoadBMP(file_image_name); fonction standard de la SDL, 
                                            // uniquement possible si l'image est au format bmp */
    if (my_image == NULL) {
        printf("Erreur  %s\n", SDL_GetError());
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDL_Quit();
        exit(EXIT_FAILURE);
    }
    my_texture = SDL_CreateTextureFromSurface(renderer, my_image); // Chargement de l'image de la surface vers la texture
    SDL_FreeSurface(my_image);                                     // la SDL_Surface ne sert que comme élément transitoire 
    if (my_texture == NULL){
        printf("Erreur  %s\n", SDL_GetError());
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDL_Quit();
        exit(EXIT_FAILURE);
    }

    return my_texture;
  }

void pollEvent(SDL_Renderer * renderer,SDL_Texture* texture,SDL_Rect avancer[8][3], int* posX, int* posY) {
    SDL_Event event;
    int nb_iteration=5000;
    int running=1;
    int i=0;
    int indice=0;
    SDL_Rect sourceRect ={16*4,24*10,16,24};
    while(i<nb_iteration && running){
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                running = 0;
            } else if (event.type == SDL_KEYDOWN) {
                switch (event.key.keysym.sym) {
                    case SDLK_UP:
                        *posY -= 10;
                        indice = (indice +1) %3;
                         sourceRect =avancer[H][indice];
                        break;
                    case SDLK_DOWN:
                        *posY += 10;
                        indice = (indice +1) %3;
                        sourceRect =avancer[B][indice];
                        break;
                    case SDLK_LEFT:
                        *posX -= 10;
                        indice = (indice +1) %3;
                        sourceRect =avancer[G][indice];
                        break;
                    case SDLK_RIGHT:
                        *posX += 10;
                        indice = (indice +1) %3;
                        sourceRect =avancer[D][indice];
                        break;
                }
            }
            
        }

        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_RenderClear(renderer);
        SDL_Rect destRect = { (*posX), (*posY),80, 120 };
        SDL_RenderCopy(renderer, texture, &sourceRect, &destRect);
        SDL_RenderPresent(renderer);
    }
}

int main(int argc, char* argv[]) {
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
        return 1;
    }

    SDL_DisplayMode dim;
    SDL_GetCurrentDisplayMode(0, &dim);

    int milieux = dim.w / 2;
    int milieuy = dim.h / 2;
    int taillefenx = dim.w * 0.8;
    int taillefeny = dim.h * 0.8;

    SDL_Window* window = SDL_CreateWindow("SDL Tutorial",
                                          SDL_WINDOWPOS_UNDEFINED,
                                          SDL_WINDOWPOS_UNDEFINED,
                                          taillefenx, taillefeny,
                                          SDL_WINDOW_SHOWN);
    if (!window) {
        printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
        SDL_Quit();
        return 1;
    }

    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (!renderer) {
        printf("Renderer could not be created! SDL_Error: %s\n", SDL_GetError());
        SDL_DestroyWindow(window);
        SDL_Quit();
        return 1;
    }

    int imgFlags = IMG_INIT_PNG;
    if (!(IMG_Init(imgFlags) & imgFlags)) {
        printf("SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError());
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDL_Quit();
        return 1;
    }

    SDL_Texture* texture = loadTexture("spritecharac.png", window,renderer);
    if (!texture) {
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        IMG_Quit();
        SDL_Quit();
        return 1;
    }
    SDL_Rect avancer[8][3];
    initrect(avancer);

    int posX = milieux, posY = milieuy;


    pollEvent(renderer,texture,avancer,&posX,&posY);
    

    SDL_DestroyTexture(texture);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    IMG_Quit();
    SDL_Quit();

    return 0;
}
