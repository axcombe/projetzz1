#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <math.h>


enum direction {
    N = -1,
	H = 0,
	B = 4,
	G = 6,
    D = 2,
    HD=1,
    HG=7,
    BD=3,
    BG=5,
};

typedef struct{
    int v;
    int h;
}entrees;

typedef struct{
    float x;
    float y;
    int etat;
    int etatprec;
    float vitessed;
    float vitessea;
    int frame; 
    int tc;
}donnees;

void initrect(SDL_Rect avancer[8][3]){
    for (int i=0;i<8;i++){
        SDL_Rect rect = {16*i, 24*9,16, 24 };
        SDL_Rect rect1 = {16*i, 24*10,16, 24 };
        SDL_Rect rect2 = {16*i, 24*11,16, 24 };
        avancer[i][0] = rect ;
        avancer[i][1] = rect1 ;
        avancer[i][2] = rect2 ;
    }
}

void initdonnees(donnees * donnees,int milieux,int milieuy){
    donnees->x=milieux-8;
    donnees->y=milieuy-12;
    donnees->etat=N;
    donnees->etatprec=N;
    donnees->vitessed=160./1000;
    donnees->vitessea=900/3;
    donnees->frame=0; 
    donnees->tc=0;
}

void initentrees(entrees * entrees){
    entrees->h=0;
    entrees->v=0;
}

SDL_Texture* loadTexture(char  *  file_image_name, SDL_Window *window, SDL_Renderer *renderer ){
    SDL_Surface *my_image = NULL;           // Variable de passage
    SDL_Texture* my_texture = NULL;         // La texture

    my_image = IMG_Load(file_image_name);   // Chargement de l'image dans la surface
                                            // image=SDL_LoadBMP(file_image_name); fonction standard de la SDL, 
                                            // uniquement possible si l'image est au format bmp */
    if (my_image == NULL) {
        printf("Erreur  %s\n", SDL_GetError());
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDL_Quit();
        exit(EXIT_FAILURE);
    }
    my_texture = SDL_CreateTextureFromSurface(renderer, my_image); // Chargement de l'image de la surface vers la texture
    SDL_FreeSurface(my_image);                                     // la SDL_Surface ne sert que comme élément transitoire 
    if (my_texture == NULL){
        printf("Erreur  %s\n", SDL_GetError());
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDL_Quit();
        exit(EXIT_FAILURE);
    }

    return my_texture;
}

void pollEvent(entrees * input,int * running) {
    SDL_Event event;

    while (SDL_PollEvent(&event)) {
        if (event.type == SDL_QUIT) {
            *running = 0;
        } else if (event.type == SDL_KEYDOWN && event.key.repeat == 0) {
            switch (event.key.keysym.sym) {
                case SDLK_UP:
                    input->v = -1;
                    break;
                case SDLK_DOWN:
                    input->v = +1;
                    break;
                case SDLK_LEFT:
                    input->h = -1;
                    break;
                case SDLK_RIGHT:
                    input->h = +1;
                    break;
            }
        } else if (event.type == SDL_KEYUP && event.key.repeat == 0) {
            switch (event.key.keysym.sym) {
                case SDLK_UP:
                    input->v = 0;
                    break;
                case SDLK_DOWN:
                    input->v = 0;
                    break;
                case SDLK_LEFT:
                    input->h = 0;
                    break;
                case SDLK_RIGHT:
                    input->h = 0;
                    break;
            }
        }

    }
}

void normalisation(float *x, float *y) {
    float norme = sqrt((*x) * (*x) + (*y) * (*y));
    if (norme != 0) {
        *x = *x / norme;
        *y = *y / norme;
    }
    
}

void maj(entrees * input,donnees * donnees,int deltat){
    float dy=0;
    float dx=0;
    dx= input->h * donnees->vitessed * deltat;
    dy= input->v * donnees->vitessed * deltat;
    donnees->etatprec = donnees->etat;
    switch(input->v){
        case -1:
            switch(input->h){
                case 0:
                    donnees->etat=H;
                    break;
                case -1:
                    donnees->etat=HG;
                    dx*=1/sqrt(2);
                    dy*=1/sqrt(2);
                    break;
                case 1:
                    donnees->etat=HD;
                    dx*=1/sqrt(2);
                    dy*=1/sqrt(2);
                    break;
            }
            break;
        case 1:
            switch(input->h){
                case 0:
                    donnees->etat=B;
                    break;
                case -1:
                    donnees->etat=BG;
                    dx*=1/sqrt(2);
                    dy*=1/sqrt(2);
                    break;
                case 1:
                    donnees->etat=BD;
                    dx*=1/sqrt(2);
                    dy*=1/sqrt(2);
                    break;
            }
            break;
        case 0:
            switch(input->h){
                case 0:
                    donnees->etat=N;
                    break;
                case -1:
                    donnees->etat=G;
                    break;
                case 1:
                    donnees->etat=D;
                    break;
            }
            break;
    }
    donnees->x += dx ;
    donnees->y += dy ;
    donnees->tc+=deltat;
    if (donnees->tc>donnees->vitessea){
        donnees->frame = (donnees->frame +1) %3;
        donnees->tc-=donnees->vitessea;
    }

}

void dessin(SDL_Renderer* renderer,SDL_Texture * texture,SDL_Texture * fond,SDL_Rect*sourceRectfond,SDL_Rect*destRectfond,donnees * donnees,SDL_Rect avancer[8][3]){
    SDL_Rect sourceRect;
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);
    SDL_Rect destRect = { donnees->x/1, donnees->y/1 ,80, 120 };
    if (donnees->etat!=-1){
        sourceRect = avancer[donnees->etat][donnees->frame];
    }
    else{
        sourceRect = avancer[B][1];
    }
    SDL_RenderCopy(renderer, fond, sourceRectfond, destRectfond);
    SDL_RenderCopy(renderer, texture, &sourceRect, &destRect);
    SDL_RenderPresent(renderer);
    
}

int main() {
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
        return 1;
    }


    SDL_DisplayMode dim;
    SDL_GetCurrentDisplayMode(0, &dim);
    //SDL_EnableKeyRepeat(0, 0);

    int milieux = dim.w / 2;
    int milieuy = dim.h / 2;
    int taillefenx = dim.w * 0.8;
    int taillefeny = dim.h * 0.8;
    

    SDL_Window* window = SDL_CreateWindow("SDL Tutorial",
                                          SDL_WINDOWPOS_CENTERED,
                                          SDL_WINDOWPOS_CENTERED,
                                          taillefenx, taillefeny,
                                          SDL_WINDOW_SHOWN);
    if (!window) {
        printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
        SDL_Quit();
        return 1;
    }

    SDL_Rect destRectfond={0, 0,taillefenx, taillefeny};
    SDL_Rect sourceRectfond={0, 0,736, 736};

    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (!renderer) {
        printf("Renderer could not be created! SDL_Error: %s\n", SDL_GetError());
        SDL_DestroyWindow(window);
        SDL_Quit();
        return 1;
    }

    int imgFlags = IMG_INIT_PNG;
    if (!(IMG_Init(imgFlags) & imgFlags)) {
        printf("SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError());
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDL_Quit();
        return 1;
    }
    SDL_Texture* fond=loadTexture("fond.jpg",window,renderer);
    SDL_Texture* texture = loadTexture("spritecharac.png", window,renderer);
    if (!texture) {
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        IMG_Quit();
        SDL_Quit();
        return 1;
    }
    SDL_Rect avancer[8][3];
    initrect(avancer);
    entrees entre;
    initentrees(&entre);
    donnees donne;
    initdonnees(&donne,milieux,milieuy);

    int running=1;
    int t1=SDL_GetTicks();
    int t2=0;
    int deltat=0;
    while (running){
        t2 = SDL_GetTicks();
        deltat = t2 - t1;
        t1 = t2;
        pollEvent(&entre,&running);
        maj(&entre,&donne,deltat);
        dessin(renderer,texture,fond,&sourceRectfond,&destRectfond,&donne,avancer);
        SDL_Delay(50);
    }
    

    SDL_DestroyTexture(texture);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    IMG_Quit();
    SDL_Quit();

    return 0;
}
