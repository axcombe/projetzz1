#include <stdio.h>
#include <SDL2/SDL.h>
#include <stdlib.h>
#include <time.h>

int nbrect=20;

void HsvToRgb(int h,int s,int v,int* r, int* g, int* b)
{
    int region;
    int remainder;
    int p;
    int q;
    int t;

    if (s == 0)
    {
        *r = v;
        *g = v;
        *b = v;
    }
    else
    {
        region = h / 43;
        remainder = (h - (region * 43)) * 6; 
        
        p = (v * (255 - s)) >> 8;
        q = (v * (255 - ((s * remainder) >> 8))) >> 8;
        t = (v * (255 - ((s * (255 - remainder)) >> 8))) >> 8;
        
        switch (region)
        {
            case 0:
                *r = v; *g = t; *b = p;
                break;
            case 1:
                *r = q; *g = v; *b = p;
                break;
            case 2:
                *r = p; *g = v; *b = t;
                break;
            case 3:
                *r = p; *g = q; *b = v;
                break;
            case 4:
                *r = t; *g = p; *b = v;
                break;
            default:
                *r = v; *g = p; *b = q;
                break;
        }
    }
}

void initcouleurHSV(int * H){
    H[nbrect-1]= rand()%361;
    for (int i=nbrect-2;i>=0;i--){
        H[i]=(H[i+1]+2) %361;   
    }
}

void recuperercouleur(int * H,SDL_Color * couleur){
    for (int i=0;i<nbrect-1;i++){
        int r,g,b;
        HsvToRgb(H[i],150,255,&r,&g, &b);
        couleur[i].r=r;
        couleur[i].g=g;
        couleur[i].b=b;
    }
    int r2,g2,b2;
    HsvToRgb(H[nbrect-1],255,255,&r2,&g2, &b2);
    couleur[nbrect-1].r=r2;
    couleur[nbrect-1].g=g2;
    couleur[nbrect-1].b=b2;
    
}

void initrectangle(SDL_Rect * rectangle ,int * dirx, int* diry,int milieux,int milieuy){
    for (int i=0;i<nbrect-1;i++){
        rectangle[i].w = 0;  
        rectangle[i].h = 0;    
        rectangle[i].x = 0;                                             
        rectangle[i].y = 0;
    }
    rectangle[nbrect-1].w = 200;  
    rectangle[nbrect-1].h = 150;    
    rectangle[nbrect-1].x = milieux-rectangle[nbrect-1].w/2;                                             
    rectangle[nbrect-1].y = milieuy-rectangle[nbrect-1].h/2;                                                 
    (*dirx)= (rand() % 11) -5;
    (*diry)= (rand() % 11) -5;
}


void dessin(SDL_Renderer * renderer,SDL_Rect rectangle,SDL_Color couleur){
    SDL_SetRenderDrawColor(renderer, couleur.r, couleur.g, couleur.b, 255);                   
    SDL_RenderFillRect(renderer,&rectangle);
    
}

void prochaincoo(SDL_Rect * rect,int * dirx,int * diry,int taillefenx,int taillefeny,SDL_Color * couleur,int* H){
    for (int i=0;i<nbrect-1;i++){
        rect[i]=rect[i+1];
    }
        rect[nbrect-1].x += (*dirx);
        rect[nbrect-1].y += (*diry);
    if (rect[nbrect-1].x <= 0 || rect[nbrect-1].x >= taillefenx-200){
        (*dirx)=-(*dirx);
        initcouleurHSV(H);
        recuperercouleur(H,couleur);
    }
    if (rect[nbrect-1].y <= 0 || rect[nbrect-1].y >= taillefeny-150){
        (*diry)=-(*diry);
        initcouleurHSV(H);
        recuperercouleur(H,couleur);
    }

}

void pollEvent(SDL_Renderer * renderer,SDL_Rect * rectangle,int * dirx, int* diry,SDL_Color* couleur,int taillefenx,int taillefeny,int * H) {
  SDL_Event event;
  int nb_iteration=5000;
  int running=1;
  int i=0;
  while(i<nb_iteration && running){
    while (SDL_PollEvent(&event)){
      if (event.type == SDL_QUIT){
        running =0;
        break;
      }
    }
    SDL_SetRenderDrawColor(renderer,0,0,0,255);
    SDL_RenderClear(renderer);

    for (int i=0;i<nbrect;i++){
        if (rectangle[i].h!=0)
        dessin(renderer,rectangle[i],couleur[i]);
    }
    prochaincoo(rectangle,dirx,diry,taillefenx,taillefeny,couleur,H);

    SDL_RenderPresent(renderer);
    SDL_Delay(20);
    i++;
  }
}


int main(){
    if(SDL_Init(SDL_INIT_VIDEO)!=0){
        fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError()); 
        return EXIT_FAILURE; 
    }
    srand(time(NULL));
    SDL_Window* window = NULL;
    SDL_Renderer* renderer = NULL;

    SDL_DisplayMode dim;
    
    SDL_GetCurrentDisplayMode(0,&dim);
    
    int taillefenx= dim.w*0.8;
    int taillefeny= dim.h*0.8;
    int milieuX=taillefenx/2;
    int milieuY=taillefeny/2;
   

    window = SDL_CreateWindow("DVD", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,taillefenx, taillefeny, SDL_WINDOW_RESIZABLE);  
    if (window == 0){
      fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError()); 
      return EXIT_FAILURE; 
    }

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED ); 
    if (renderer == 0) {
     fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError()); 
     return EXIT_FAILURE; 
    }
    
    SDL_Rect rectangle[nbrect];
    int dirx,diry;
    initrectangle(rectangle,&dirx,&diry,milieuX,milieuY);
    SDL_Color couleur[nbrect];
    int H[nbrect];
    initcouleurHSV(H);
    recuperercouleur(H,couleur);
    
    pollEvent(renderer,rectangle,&dirx,&diry,couleur,taillefenx,taillefeny,H);


    SDL_Quit();
    return 0;
}
